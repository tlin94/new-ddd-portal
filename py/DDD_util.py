#!/usr/bin/python
import numpy as np
import sqlite3
import csv
import json
from collections import namedtuple

PATH_DB = "/Users/tien-julin/Sites/DDD-portal/db/"
DataBase = "micro.db"


conn = sqlite3.connect(PATH_DB + DataBase)
c = conn.cursor()

def namedtuple_factory(cursor,row):
	#Usage:con.row_factory = named_factory
	fields = [col[0] for col in cursor.description]
	Row = namedtuple("Row",fields)
	return Row(*row)


def removeAllData():
	
	conn = sqlite3.connect(PATH_DB + DataBase)
	c = conn.cursor()
	
	c.execute('DELETE FROM Configurations')
	c.execute('DELETE FROM Experiments')
	c.execute('DELETE FROM Volume_Info')
	c.execute('DELETE FROM Material')
	c.execute('DELETE FROM Layer_Property')
	c.execute('DELETE FROM Simulation_Control')
	c.execute('DELETE FROM Numerical_Procedure')
	c.execute('DELETE FROM Simulation_Options')
	c.execute('DELETE FROM Outputs')
	c.execute('DELETE FROM Elastic_Stiffness_Matrix')
	c.execute('DELETE FROM Matrix_1by3')
	c.execute('DELETE FROM Defect')
	c.execute('DELETE FROM Slip_System')
	c.execute('DELETE FROM Defect_Node')	
	c.execute('DELETE FROM Parameter_Description')	
	conn.commit()
	conn.close()
	
	
def insertDummyData_csv():
	
	conn = sqlite3.connect(PATH_DB + DataBase)
	c = conn.cursor()
	
	#c.execute('BEGIN TRANSACTION')
	csvfile = open(PATH_DB + 'dummydata/data_experiments.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Experiments VALUES (?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_configurations.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Configurations VALUES (?,?,?,?,?,?,?,?)', t )
	csvfile.close()
	 
	csvfile = open(PATH_DB + 'dummydata/data_material.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Material VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', t )
	csvfile.close() 
	
	csvfile = open(PATH_DB + 'dummydata/data_slip.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Slip VALUES (?,?,?,?,?,?,?,?,?,?)', t )
	csvfile.close() 
	 
	csvfile = open(PATH_DB + 'dummydata/data_layer_property.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Layer_Property VALUES (?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_volume_info.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Volume_Info VALUES (?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_numerical_procedure.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Numerical_Procedure VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_simulation_control.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Simulation_Control VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_simulation_options.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	for t in creader:
	    c.execute('INSERT INTO Simulation_Options VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_outputs.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Outputs VALUES (?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_elastic_stiffness_matrix.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Elastic_Stiffness_Matrix VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_matrix_1by3.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Matrix_1by3 VALUES (?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_defect.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Defect VALUES (?,?,?,?,?,?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_slip_systems.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Slip_System VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_defect_node.csv', 'rb')
	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Defect_Node VALUES (?,?,?,?,?,?)', t )
	csvfile.close()
	
	csvfile = open(PATH_DB + 'dummydata/data_parameter.csv', 'rb')
	creader = csv.reader(csvfile, delimiter='$', quotechar='|')
	creader.next()
	for t in creader:
	    c.execute('INSERT INTO Parameter_Description VALUES (?,?,?,?)', t )
	csvfile.close()
	
#  	csvfile = open(PATH_DB + 'dummydata/data_reports.csv', 'rb')
#  	creader = csv.reader(csvfile, delimiter=',', quotechar='|')
#  	for t in creader:
#  	    c.execute('INSERT INTO Reports VALUES (?,?,?,?,?,?,?)', t )
#  	csvfile.close()
	
	#c.execute('END TRANSACTION')
	conn.commit()
	conn.close()
	
#--------------------------------------------------------------------------------------
# Function: readInputFile
# @note: Notice that all the scientific expression in Fortran should be in lower case. Ex. 2.5d-2
#---------------------------------------------------------------------------------------
def readInputFile():
	
	# use json format
	d = {}
	with open("material_input.txt","r") as ifile:
		ifile.readline();ifile.readline();ifile.readline();
		d["NewSimulation"] = ifile.readline().split()[1]
		#-----------Volume Info------------------#
		ifile.readline();ifile.readline();ifile.readline();
		temp = ifile.readline().split()
		d["Volume_Size_x"] = temp[1]; 
		d["Volume_Size_y"] =temp[2]
		d["Num_Layer"] = ifile.readline().split()[1]
		d["iInterface"] = ifile.readline().split()[1]
		
		#-----------Layer Property------------------#
		ifile.readline();ifile.readline();ifile.readline();
		o1 = []
		for i in range(int(d["Num_Layer"])):
			d1={}		
			d1["Layer_Type"] = ifile.readline().split()[1]
			d1["Thickness"] = ifile.readline().split()[1]
			ifile.readline()
			temp = ifile.readline().split()
			d1["Euler_Angles_x"] = temp[0];d1["Euler_Angles_y"] = temp[1];d1["Euler_Angles_z"] = temp[2];
			d1["Mu"] = (ifile.readline().split()[1]).replace('d','e')
			d1["Nu"] = (ifile.readline().split()[1]).replace('d','e')
			ifile.readline()
			temp = ifile.readline().split()
			d1["E_01"] = temp[0]; d1["E_02"] = temp[1]; d1["E_03"] = temp[2]; d1["E_04"] = temp[3]; d1["E_05"] = temp[4]; d1["E_06"] = temp[5];
			temp = ifile.readline().split()
			d1["E_07"] = temp[0]; d1["E_08"] = temp[1]; d1["E_09"] = temp[2]; d1["E_10"] = temp[3]; d1["E_11"] = temp[4]; d1["E_12"] = temp[5];
			temp = ifile.readline().split()
			d1["E_13"] = temp[0]; d1["E_14"] = temp[1]; d1["E_15"] = temp[2]; d1["E_16"] = temp[3]; d1["E_17"] = temp[4]; d1["E_18"] = temp[5];
			temp = ifile.readline().split()
			d1["E_19"] = temp[0]; d1["E_20"] = temp[1]; d1["E_21"] = temp[2]; d1["E_22"] = temp[3]; d1["E_23"] = temp[4]; d1["E_24"] = temp[5];
			temp = ifile.readline().split()
			d1["E_25"] = temp[0]; d1["E_26"] = temp[1]; d1["E_27"] = temp[2]; d1["E_28"] = temp[3]; d1["E_29"] = temp[4]; d1["E_30"] = temp[5];
			temp = ifile.readline().split()
			d1["E_31"] = temp[0]; d1["E_32"] = temp[1]; d1["E_33"] = temp[2]; d1["E_34"] = temp[3]; d1["E_35"] = temp[4]; d1["E_36"] = temp[5];
			ifile.readline()
			d1["Crystal_Type"] = ifile.readline().split()[1]
			d1["Lattice_Param"] = (ifile.readline().split()[1]).replace('d','e')
			temp = ifile.readline().split()
			d1["Lattice_Ratios_x"] = temp[1]; d1["Lattice_Ratios_y"] = temp[2]; d1["Lattice_Ratios_z"] = temp[3]
			temp = ifile.readline().split()
			d1["Lattice_Angles_x"] = temp[1]; d1["Lattice_Angles_y"] = temp[2]; d1["Lattice_Angles_z"] = temp[3]
			ifile.readline()
			d1["nSlip"] = ifile.readline().split()[1]
			d1["Basal"]= ifile.readline().split()[1]
			d1["Prismatic"]= ifile.readline().split()[1]
			d1["Pyramidal"]= ifile.readline().split()[1]
			d1["Mobilityns"]= (ifile.readline().split()[1]).replace('d','e')
			d1["Mobilitys"]= (ifile.readline().split()[1]).replace('d','e')	
			d1["Anisotropy"]= ifile.readline().split()[1]
			o1.append(d1)
		d["Layers"] = o1
		
		#-----------Simulation Control------------------#
		ifile.readline();ifile.readline();ifile.readline();
		d["Load"] = ifile.readline().split()[1]
		ifile.readline()
		temp = ifile.readline().split()
		d["Strain_01"] = temp[0].replace('d','e');d["Strain_02"]=temp[1].replace('d','e');d["Strain_03"]=temp[2].replace('d','e')
		temp = ifile.readline().split()
		d["Strain_04"] = temp[0].replace('d','e');d["Strain_05"] = temp[1].replace('d','e');d["Strain_06"] = temp[2].replace('d','e')
		temp = ifile.readline().split()
		d["Strain_07"] = temp[0].replace('d','e'); d["Strain_08"] = temp[1].replace('d','e'); d["Strain_09"] = temp[2].replace('d','e')
		ifile.readline()
		temp = ifile.readline().split()
		d["Stress_01"] = temp[0].replace('d','e'); d["Stress_02"] = temp[1].replace('d','e'); d["Stress_03"] = temp[2].replace('d','e')
		temp = ifile.readline().split()
		d["Stress_04"] = temp[0].replace('d','e'); d["Stress_05"] = temp[1].replace('d','e'); d["Stress_06"] = temp[2].replace('d','e')
		temp = ifile.readline().split()
		d["Stress_07"] = temp[0].replace('d','e'); d["Stress_08"] = temp[1].replace('d','e'); d["Stress_09"] = temp[2].replace('d','e')
		ifile.readline()
		temp = ifile.readline().split()
		d["IAS_01"] = temp[0].replace('d','e'); d["IAS_02"] = temp[1].replace('d','e'); d["IAS_03"] = temp[2].replace('d','e')
		temp = ifile.readline().split()
		d["IAS_04"] = temp[0].replace('d','e'); d["IAS_05"] = temp[1].replace('d','e'); d["IAS_06"] = temp[2].replace('d','e')
		temp = ifile.readline().split()
		d["IAS_07"] = temp[0].replace('d','e'); d["IAS_08"] = temp[1].replace('d','e'); d["IAS_09"] = temp[2].replace('d','e')		
		d["iRelaxtion"] = ifile.readline().split()[1]
		d["iRelaxStep"] = ifile.readline().split()[1]

		#-----------Numerical Procedure------------------#
		ifile.readline();ifile.readline();ifile.readline();
		d["ILoop_Time"] = ifile.readline().split()[1]
		temp = ifile.readline().split()
		d["DTIME_MAX"] = temp[1].replace('d','e')
		d["DTIME_MIN"] = temp[2].replace('d','e')
		d["MAX_QUAD"] = ifile.readline().split()[1]
		d["NPOINT_I"] = ifile.readline().split()[1]
		d["MAX_node"] = ifile.readline().split()[1]
		
		
		d["MAX_Neighbor"] = ifile.readline().split()[1]
		d["dpMaxAveLength"] = (ifile.readline().split()[1]).replace('d','e')
		temp = ifile.readline().split()
		d["nbox_x"] = temp[1]; d["nbox_y"] = temp[2]; d["nbox_z"] = temp[3]		
		d["reebox"] = ifile.readline().split()[1]
		d["dcrit"] = (ifile.readline().split()[1]).replace('d','e')
		d["iArr"] = ifile.readline().split()[1]
		d["iFEM"] = ifile.readline().split()[1]
		temp = ifile.readline().split()
		d["nElmtFe_x"] = temp[1]; d["nElmtFe_y"] = temp[2]; d["nElmtFe_z"] = temp[3]
		
		#-----------Defect Properties------------------#
		ifile.readline();ifile.readline();ifile.readline();
		ifile.readline()
		ifile.readline()
		ifile.readline()
		ifile.readline()
		
		#-----------Simulation Option------------------#
		ifile.readline();ifile.readline();ifile.readline();
		d["CheckNeiBur"] = ifile.readline().split()[1]
		d["LogInteraction"] = ifile.readline().split()[1]
		d["iCrossSlip"] = ifile.readline().split()[1]
		d["FullyPeriodic"] = ifile.readline().split()[1]
		d["FiniteBox"] = ifile.readline().split()[1]
		d["ioutFe"] = ifile.readline().split()[1]
		d["nBoxFe"] = ifile.readline().split()[1]
		d["iSplineFe"] = ifile.readline().split()[1]
		temp = ifile.readline().split()
		d["DispNodes_x"] = temp[1]; d["DispNodes_y"] = temp[2]; d["DispNodes_z"] = temp[3]
		d["iXRD"] = ifile.readline().split()[1]
				
		#-----------Outputs------------------#
		ifile.readline();ifile.readline();ifile.readline();
		d["iOutFreq"] = ifile.readline().split()[1]
		d["iPastFreq"] = ifile.readline().split()[1]
		d["iDebugMode"] = ifile.readline().split()[1]
		
	with open("interaction_geom_input.txt","r") as ifile:
		#-----------Defects ------------------#
		ifile.readline(); ifile.readline()
		o1=[] 
		while (ifile.readline().split()[0]=="frs"):
			d1={}
			d1["LayerPos"] = ifile.readline().split()[0]
			temp = ifile.readline().split()
			d1["Form_Type"] = temp[0]
			d1["nNode"] = temp[1]
			temp = ifile.readline().split()
			d1["MillerIdx_01"] = temp[0]; d1["MillerIdx_02"] = temp[1]; d1["MillerIdx_03"] = temp[2]; d1["MillerIdx_04"] = temp[3]
			temp = ifile.readline().split()
			d1["vBurger_01"] = temp[0]; d1["vBurger_02"] = temp[1]; d1["vBurger_03"] = temp[2]; d1["vBurger_04"] = temp[3]
			temp = ifile.readline().split()
			d1["CenterPos_01"] = temp[0]; d1["CenterPos_02"] = temp[1]; d1["CenterPos_03"] = temp[2] 	
			d1["LoopRad"] = temp[3]; d1["CenterLoop_01"] = temp[4]; d1["CenterLoop_02"] = temp[5]
			#-----------Defects Nodes------------------#
			isEnd = False
			o2 = []
			while (not isEnd):
				d2={}
				temp = ifile.readline().split()
				if len(temp)>0:
					#print temp
					d2["NodePos"] = temp[0]; d2["Coordinate_01"] = temp[1]; d2["Coordinate_02"] = temp[2]
					o2.append(d2)
				else:
					isEnd = True
					
			d1["Nodes"] = o2
			o1.append(d1)
						
		d["Defects"] = o1
		
	json_data = json.dumps(d)
	#print json_data
	return json_data


def writeInputFile(exp):
	
	#!============== Define row_factory ================!#
	conn = sqlite3.connect(PATH_DB + DataBase)
	conn.row_factory = namedtuple_factory
	c = conn.cursor()
	
	# !====================== GET ID for all tables =============================!#
	# @note: input can be simulation name or simulation ID
	if isinstance(exp,int):
		Exp_ID = exp
	elif isinstance(exp,str):
		c.execute("SELECT Exp_ID FROM Experiments WHERE Experiment_Name= '%s'" %exp)
		Exp_ID = c.fetchone()[0]
	
	c.execute('SELECT Config_ID FROM Experiments WHERE Exp_ID = %d' %Exp_ID)	
	Config_ID = c.fetchone()[0]
	
	c.execute('SELECT VoIn_ID, SimCo_ID, NuPro_ID, SimOp_ID, Out_ID FROM Configurations WHERE Config_ID = %d' %Config_ID)	
	data_id = c.fetchone()
	#print data_id
	VoIn_ID = data_id[0]; SimCo_ID = data_id[1]
	NuPro_ID = data_id[2]; SimOp_ID = data_id[3]; 
	Out_ID = data_id[4];
	#print "%d,%d,%d,%d,%d" %(VoIn_ID,SimCo_ID,NuPro_ID,SimOp_ID,Out_ID )
	
	#!====================== GET DATA BY QUERY =============================!#
	#---------------- Volume Info -------------------#
	c.execute('SELECT * FROM Volume_Info WHERE VoIn_ID = %d' %VoIn_ID)
	data_volume_info = c.fetchone()
	#print data_volume_info	
	
	#---------------- Layer Property & Material & Slip-------------------#
	c.execute('SELECT * FROM Layer_Property WHERE Config_ID = %d' %Config_ID)
	data_layer_property = c.fetchall()
	#print data_layer_property
	
	data_material=[];data_euler_angles=[]
	for record in data_layer_property:
		c.execute('SELECT * FROM Material WHERE Mat_ID = %d' %record.Mat_ID)
		data_material.append(c.fetchone())
		c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.Euler_Angles )
		data_euler_angles.append(c.fetchone())
	#print data_material
	#print data_euler_angles
	
	data_elastic_stiffness_mtx=[];data_lattice_ratios=[];data_lattice_angles=[];data_slip=[]
	for record in data_material:
		c.execute('SELECT * FROM Elastic_Stiffness_Matrix WHERE ElSt_MTX_ID = %d' %record.ElSt_MTX_ID)
		data_elastic_stiffness_mtx.append(c.fetchone())
		c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.Lattice_Ratios )
		data_lattice_ratios.append(c.fetchone())
		c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.Lattice_Angles )
		data_lattice_angles.append(c.fetchone())
		c.execute('SELECT * FROM Slip WHERE Mat_ID = %d' %record.Mat_ID)
		data_slip.append(c.fetchall())	
	#print data_elastic_stiffness_mtx
	#print data_lattice_ratios
	#print data_lattice_angles
		
		
	#----------------- Simulation Control --------------------#
	c.execute('SELECT * FROM Simulation_Control WHERE SimCo_ID = %d' %SimCo_ID)
	data_simulation_control = c.fetchone()

	#print data_simulation_control
	
	#----------------- Numerical Procedure --------------------#
	c.execute('SELECT * FROM Numerical_Procedure WHERE NuPro_ID = %d' %NuPro_ID)
	data_numerical_procedure = c.fetchone()
	c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %data_numerical_procedure.nbox)
	data_nbox = c.fetchone()
	c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %data_numerical_procedure.nElmtFe)
	data_nElmtFe = c.fetchone()	
	#print data_numerical_procedure	
	
	#----------------- Simulation Option --------------------#
	c.execute('SELECT * FROM Simulation_Options WHERE SimOp_ID = %d' %SimOp_ID)
	data_simulation_options = c.fetchone()
	c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %data_simulation_options.DispNodes)
	data_dispNodes = c.fetchone()
	#print data_simulation_options
	
	#----------------- Outputs --------------------#
	c.execute('SELECT * FROM Outputs WHERE Out_ID = %d' %Out_ID)
	data_outputs = c.fetchone()
	
	#----------------- Defect --------------------#
	c.execute('SELECT * FROM Defect WHERE Config_ID = %d' %Config_ID)
	data_defect = c.fetchall()
	data_milleridx=[];data_vburger=[];data_centerpos=[]
	for record in data_defect:
		c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.CenterPos)
		data_centerpos.append(c.fetchone())
	
	#print data_defect
	
	#----------------- Defect Node & Slip System--------------------#
	data_slip_system=[]; data_defect_node=[]
	for record in data_defect:
		c.execute('SELECT * FROM Slip_System WHERE Slsys_ID = %d' %record.Slsys_ID)
		data_slip_system.append(c.fetchone())
		c.execute('SELECT * FROM Defect_Node WHERE Def_ID = %d' %record.Def_ID)
		data_defect_node.append(c.fetchall())
	#print data_defect_node
	#print data_slip_system
	
	
	
	#--------------------Special materials (user define crystal stype) -----------#
	_crystal=["FCC","BCC","HCP","HCP1","HCP2"]
	_crystalToNum={"Default":0,"FCC":1,"BCC":2,"HCP":3,"HCP1":4,"HCP2":5} # 1 FCC; 2 BCC; 3 HCP; 4 HCP1; 5 HCP2
	
	data_s_material=[];
 	for record in data_material:
 		if (record.Crystal_Type not in _crystal):
 			data_s_material.append(record)
 	
 	if(len(data_s_material)>0):
 		data_s_latticeRatios=[];data_s_latticeAngles=[]
	 	data_numMode=[];data_Mode=[];data_ModeInfo=[];data_ModeMiller=[];data_ModeBurger=[];structure_type=""
	 	
	 	for record in data_s_material:
	 		c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.Lattice_Ratios )
			data_s_latticeRatios.append(c.fetchone())
			c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.Lattice_Angles )
			data_s_latticeAngles.append(c.fetchone())
	 		
	 		c.execute("SELECT * FROM Slip_System WHERE Crystal = '%s'" %record.Crystal_Type)
	 		slipSys = c.fetchall()
	 		numMode=0;arrMode=[];dictMiller={};dictBurger={}; arrInfo=[]
	 		
	 		for s in slipSys:
	 			mode=str(s.mode)
	 			miller=[s.M_01,s.M_02,s.M_03,s.M_04]
	 			burger=[s.B_01,s.B_02,s.B_03,s.B_04]
	 			structure_type=s.Structure
	 			
	 			if ( mode not in arrMode):
	 				arrMode.append(mode)
	 				dictMiller[mode]=[]
	 				dictBurger[mode]=[]
	 				numMode=numMode+1	
	 				
	 			dictMiller[mode].append(miller)
	 			dictBurger[mode].append(burger)
	 			
	 		data_numMode.append(numMode)
	 		data_Mode.append(arrMode)
	 		data_ModeMiller.append(dictMiller)
	 		data_ModeBurger.append(dictBurger)
			
			for m in arrMode:
				c.execute("SELECT * FROM Slip WHERE Mat_ID=%d AND mode='%s'" %(record.Mat_ID,m))
				arrInfo.append(c.fetchone())		
			
			data_ModeInfo.append(arrInfo)
		
# 		print data_Mode	
# 		print data_ModeInfo 
# 		print data_ModeMiller
# 		print data_ModeBurger
	
	conn.commit()
	conn.close()		
	
	#!====================== WRITE MATERIAL INPUT FILE =============================!#
 	
 	with open("material_input.txt","w") as ofile:
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* New simulation or Restart previous simulation\n")
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("%-22s %-22s\n" %("newsimulation:",1)) ###  TBD  ###
 		
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* Simulated Volume information\n")
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("%-22s %-22s\n" %("volumeType:",1))
 		ofile.write("%-22s %-22s\n" %("volume:", str(data_volume_info.Volume_Size_x)+'  '+str(data_volume_info.Volume_Size_y))) 
 		ofile.write("%-22s %-22s\n" %("numlayers:",data_volume_info.Num_Layer))
 		ofile.write("%-22s %-22s\n" %("volumeBC:",str('1 1 1')))
 		ofile.write("%-22s %-22s\n" %("iInterface:",data_volume_info.iInterface))
 		
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* Layers/Materials/Crystals properties\n")
 		ofile.write("----------------------------------------------------------\n")
 		for i in range(len(data_layer_property)):
 			ofile.write("%-22s %-22s\n" %("LAYER:",data_material[i].Material_Type))
 			ofile.write("%-22s %-22s\n" %("thickness:",data_layer_property[i].Thickness))
 			ofile.write("EulerAngles:\n")
 			mtx = data_euler_angles[i]
 			ofile.write(" %s\n" %(str(mtx.E_01)+'  '+str(mtx.E_02)+'  '+str(mtx.E_03)))
 			ofile.write(("%-22s" %"Mu:") + (" %-22.2e\n" %data_material[i].Mu).replace('e','d'))
 			ofile.write("%-22s %-22s\n" %("Nu:",data_material[i].Nu))
 			ofile.write("*Elastic stiffness of single crystal (MPa)\n")
 			mtx = data_elastic_stiffness_mtx[i]
 			ofile.write("%-9.1e %-9.1e %-9.1e %-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e %-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e %-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e %-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e %-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e %-9.1e %-9.1e %-9.1e\n" \
 			%(mtx.E_01,mtx.E_02,mtx.E_03,mtx.E_04,mtx.E_05,mtx.E_06,mtx.E_07,mtx.E_08,mtx.E_09,mtx.E_10,mtx.E_11,mtx.E_12, \
 			  mtx.E_13,mtx.E_14,mtx.E_15,mtx.E_16,mtx.E_17,mtx.E_18,mtx.E_19,mtx.E_20,mtx.E_21,mtx.E_22,mtx.E_23,mtx.E_24, \
 			  mtx.E_25,mtx.E_26,mtx.E_27,mtx.E_28,mtx.E_29,mtx.E_30,mtx.E_31,mtx.E_32,mtx.E_33,mtx.E_34,mtx.E_35,mtx.E_36))
 			ofile.write("*Lattice information (1 FCC; 2 BCC; 3 HCP; 4 HCP1; 5 HCP2)\n")
 			if data_material[i].Crystal_Type in _crystal:
 				ofile.write("%-22s %-22s\n" %("crystaltype:",_crystalToNum[data_material[i].Crystal_Type]))
 			else:
 				ofile.write("%-22s %-22s\n" %("crystaltype:","0"))
 			ofile.write(("%-22s" %"Latticeparameter: ") + (" %-22.2e\n" %data_material[i].Lattice_Param).replace('e','d'))
 			mtx = data_lattice_ratios[i]
 			ofile.write("%-22s %-22s\n" %("Latticeratios:",str(mtx.E_01)+'  '+str(mtx.E_02)+'  '+str(mtx.E_03))) 
 			mtx = data_lattice_angles[i]
 			ofile.write("%-22s %-22s\n" %("Latticeangles:",str(mtx.E_01)+'  '+str(mtx.E_02)+'  '+str(mtx.E_03))) 
 			ofile.write("*Lattice friction stresses (in MPa)\n")
 			ofile.write("%-22s %-22s\n" %("nslip: ", data_material[i].nSlip))
 			for j in range(len(data_slip[i])):
 				ofile.write("%-22s %-22s\n" %(data_slip[i][j].Slip_Name, str(data_slip[i][j].Slip_x) +' '+str(data_slip[i][j].Slip_y)+' '+str(data_slip[i][j].Slip_z)))
 			ofile.write(("%-22s" %"MOBILITYns:") + (" %-22.1e\n" %data_material[i].Mobilityns).replace('e','d'))
 			ofile.write(("%-22s" %"MOBILITYs:") + (" %-22.1e\n" %data_material[i].Mobilitys).replace('e','d')) 
 			ofile.write("%-22s %-22s\n" %("anisotropy:", data_material[i].Anisotropy))
 			
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* Simulation control / B.C. Load= 1 Strain rate imposed, 2 stress rate imposed\n")
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("%-22s %-22s\n" %("Load:",data_simulation_control.Load))
 		if data_simulation_control.Load == 1:
 			ofile.write("*Applied Strain rate\n")
 			ofile.write(("%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n" \
 					%(data_simulation_control.AF_01,data_simulation_control.AF_02,data_simulation_control.AF_03,
					data_simulation_control.AF_04,data_simulation_control.AF_05,data_simulation_control.AF_06,
					data_simulation_control.AF_07,data_simulation_control.AF_08,data_simulation_control.AF_09
					)).replace('e','d'))
 			ofile.write("*Applied Stress rate (in Pa)\n")
 			ofile.write(("%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n" \
 					%(0,0,0,0,0,0,0,0,0)).replace('e','d'))				
 		elif data_simulation_control.Load == 2:
 			ofile.write("*Applied Strain rate\n")
 			ofile.write(("%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n" \
 					%(0,0,0,0,0,0,0,0,0)).replace('e','d'))
 			ofile.write("*Applied Stress rate (in Pa)\n")
 			ofile.write(("%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n" \
 					%(data_simulation_control.AF_01,data_simulation_control.AF_02,data_simulation_control.AF_03,
					data_simulation_control.AF_04,data_simulation_control.AF_05,data_simulation_control.AF_06,
					data_simulation_control.AF_07,data_simulation_control.AF_08,data_simulation_control.AF_09
					)).replace('e','d'))
 		ofile.write(" *Initial Applied Stress (in Pa)\n")
 		ofile.write(("%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n%-9.1e %-9.1e %-9.1e\n" \
 					%(data_simulation_control.IAS_01,data_simulation_control.IAS_02,data_simulation_control.IAS_03,
					data_simulation_control.IAS_04,data_simulation_control.IAS_05,data_simulation_control.IAS_06,
					data_simulation_control.IAS_07,data_simulation_control.IAS_08,data_simulation_control.IAS_09
					)).replace('e','d'))
 		ofile.write("%-22s %-22s\n" %("iRelaxation:",data_simulation_control.iRelaxtion))
 		ofile.write("%-22s %-22s\n" %("iRelaxSteps:",data_simulation_control.iRelaxStep))
 	
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* Numerical procedur\n")
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("%-22s %-22s\n" %("ILOOP_TIME:",data_numerical_procedure.ILoop_Time))
 		ofile.write(("%-22s" %"DTIME:") + (" %.1e %.1e\n" %(data_numerical_procedure.DTIME_MAX,data_numerical_procedure.DTIME_MIN)).replace('e','d'))
 		ofile.write("%-22s %-22s\n" %("MAX_QUAD:",data_numerical_procedure.MAX_QUAD))
 		ofile.write("%-22s %-22s\n" %("NPOINT_I:",data_numerical_procedure.NPOINT_I))
 		ofile.write("%-22s %-22s\n" %("MAX_NODE:",data_numerical_procedure.MAX_node))
 		ofile.write("%-22s %-22s\n" %("MAX_NEIGHBOR:",data_numerical_procedure.MAX_Neighbor))
 		ofile.write(("%-22s" %"dpMaxAveLength:") + (" %-22.1e\n" %data_numerical_procedure.dpMaxAveLength).replace('e','d'))
 		mtx = data_nbox
 		ofile.write("%-22s %-22s\n" %("nbox:",str(int(mtx.E_01))+'  '+str(int(mtx.E_02))+'  '+str(int(mtx.E_03)))) 
 		ofile.write("%-22s %-22s\n" %("reebox:",data_numerical_procedure.reebox))
 		ofile.write("%-22s %-22s\n" %("dcrit:",data_numerical_procedure.dcrit))
 		ofile.write("%-22s %-22s\n" %("iArr:",data_numerical_procedure.iArr))
 		ofile.write("%-22s %-22s\n" %("iFEM:",data_numerical_procedure.iFEM))
 		mtx = data_nElmtFe
 		ofile.write("%-22s %-22s\n" %("nElmtFe:",str(int(mtx.E_01))+'  '+str(int(mtx.E_02))+'  '+str(int(mtx.E_03))))
 		
 		############      TBD      ###########
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* Defect Properies\n")
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("%-22s %-22s\n" %("damage:","0"))
 		ofile.write("%-22s %-22s\n" %("ndef:","2"))
 		ofile.write("24.28 20.18 15.14 0. 0. 0.\n")
 		ofile.write("-9.08 -0.66 -2.81 0. 0. 0.\n")
 		############      TBD      ###########
 		
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* Simulation options\n")
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("%-22s %-22s\n" %("CheckNeiBur:",data_simulation_options.CheckNeiBur))
 		ofile.write("%-22s %-22s\n" %("logInteraction:",data_simulation_options.LogInteraction))
 		ofile.write("%-22s %-22s\n" %("iCrossSlip:",data_simulation_options.iCrossSlip))
 		ofile.write("%-22s %-22s\n" %("iInertial:",data_simulation_options.iInertial))
 		ofile.write("%-22s %-22s\n" %("fullyperiodic:",data_simulation_options.FullyPeriodic))
 		ofile.write("%-22s %-22s\n" %("ioutFe:",data_simulation_options.ioutFe))
 		ofile.write("%-22s %-22s\n" %("nboxFe:",data_simulation_options.nBoxFe))
 		ofile.write("%-22s %-22s\n" %("iSplineFe:",data_simulation_options.iSplineFe))
 		mtx = data_dispNodes
 		ofile.write("%-22s %-22s\n" %("DispNodes:",str(int(mtx.E_01))+'  '+str(int(mtx.E_02))+'  '+str(int(mtx.E_03)))) 
 		ofile.write("%-22s %-22s\n" %("iXRD:",data_simulation_options.iXRD))
 
 
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* Outputs\n")
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("%-22s %-22s\n" %("iOutFreq:",data_outputs.iOutFreq))
 		ofile.write("%-22s %-22s\n" %("iPastFreq:",data_outputs.iPastFreq))
 		ofile.write("%-22s %-22s\n" %("iDebugMode:",data_outputs.iDebugMode))
 		
 		###############  TBD  #######################
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("* Latent coefficients (0: none, 1: Glissile, 2: Hirth, 3: Lomer, 4: Collinear)\n")
 		ofile.write("----------------------------------------------------------\n")
 		ofile.write("%-22s %-22s\n" %("iLatentCoef:", "0"))
 	
 	
# 	#!====================== WRITE INTERACTION_GEOM INPUT FILE =============================!#
     
 	with open("interaction_geom_input.txt","w") as ofile:
 		ofile.write("DEFECT  PLANE  NODES M_1 M_2 M_3 B_1 B_2 B_3 O_x O_y O_z\n\n")
 		for i in range(len(data_defect)):
 			ofile.write("frs\n")
 			ofile.write("%-22s\n" %(data_defect[i].LayerPos))
 			ofile.write("%18s %18s \n" %(data_defect[i].Form_Type,data_defect[i].nNode))
 			ofile.write("%22.16f %22.16f %22.16f %22.16f\n" %(data_slip_system[i].M_01,data_slip_system[i].M_02,data_slip_system[i].M_03,data_slip_system[i].M_04))
 			ofile.write("%22.16f %22.16f %22.16f %22.16f\n" %(data_slip_system[i].B_01,data_slip_system[i].B_02,data_slip_system[i].B_03,data_slip_system[i].B_04))
 			mtx = data_centerpos[i]
 			ofile.write("%22.13f %22.13f %22.13f"%(mtx.E_01,mtx.E_02,mtx.E_03))
 			ofile.write("%15.7f %15.7f %15.7f\n" %(data_defect[i].LoopRad,data_defect[i].CenterLoop_01,data_defect[i].CenterLoop_02))			
 			data = data_defect_node[i]
 			for j in range(len(data)):			
 				ofile.write("%-4s %-21.14f %-21.14f\n" %(j+1,data[j].Coordinate_01,data[j].Coordinate_02))
 			ofile.write("\n")
 		
 		ofile.write("ENDDEFECT") 
	
 	#!====================== WRITE INTERACTION_GEOM INPUT FILE =============================!#
	#data_numMode=[];data_Mode=[];data_ModeInfo=[];data_ModeMiller=[];data_ModeBurger=[];structure_type=""
 	if(len(data_s_material)>0):
 		for i,material in enumerate(data_s_material):
 			fileName="crystal" + material.Crystal_Type +".sx"
	 		with open(fileName,"w") as ofile:
	 			ofile.write("%-22s %-22s\n" %("*Material:",material.Material_Type))
	 			ofile.write("%-22s %-42s\n" %(structure_type,"crysym"))
	 			mtx1=data_s_latticeRatios[i];mtx2=data_s_latticeAngles[i]
	 			ofile.write("%-22s %-22s\n" %(str(mtx1.E_01)+'  '+str(mtx1.E_02)+'  '+str(mtx1.E_03),str(mtx2.E_01)+'  '+str(mtx2.E_02)+'  '+str(mtx2.E_03)) )
 				ofile.write("SLIP AND TWINNING MODES\n")
 				ofile.write("%-22s %-22s\n" %(data_numMode[i],"nmodesx"))
 				ofile.write("%-22s %-22s\n" %(data_numMode[i],"nmodes"))
 				text=""
 				for mode in data_Mode[i]:
 					text=text+str(mode)+" "
 				ofile.write("%-22s %-22s\n" %(text,"mode(i)"))
 				
 				for j,info in enumerate(data_ModeInfo[i]):
 					ofile.write("%-22s\n" %info.Slip_Name)
 					mode = str(info.mode)
 					order = j+1
 					numOfSys = len(data_ModeMiller[i][mode])
 					text1="  %d %d 1 0" %(order,numOfSys)
 					ofile.write("%-22s %-22s\n" %(text1,"modex,nsmx,iopsysx,itwtypex"))
 					for m,b in zip(data_ModeMiller[i][mode],data_ModeBurger[i][mode]):
 						miller=str(m[0])+" "+str(m[1])+" "+str(m[2])+" "+str(m[3])
       	 	 			burger=str(b[0])+" "+str(b[1])+" "+str(b[2])+" "+str(b[3])
       	 	 			text2=miller+"   "+burger
       	 	 			ofile.write("%-22s\n" %text2)

 	 	 		ofile.write("SLIP PARAMETERS\n")
		 	 	for j,info in enumerate(data_ModeInfo[i]):
 			 	 	ofile.write("%s -----------------------------\n" %info.Slip_Name)
 		 			ofile.write("%-22s %-22s\n" %(info.magBurger,"!BURG (m)"))
 		 			ofile.write("%-22s %-22s\n" %(info.Slip_x,"!Friction stress (MPa)"))
 		 			ofile.write("%-22s %-22s\n" %(str(info.Slip_y)+" "+str(info.Slip_z),"!Mobilities edge/screw (Pa.s)"))
 	
 	
#======================== Function Test ===================================#
#removeAllData()
#insertDummyData_csv()
writeInputFile("test")
#readInputFile()
