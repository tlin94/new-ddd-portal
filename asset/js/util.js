//Manage the template of html files
function require_template(templateName) {
    var template = $('#template_' + templateName);
    if (template.length === 0) {
        var tmpl_dir = './template';
        var tmpl_url = tmpl_dir + '/' + templateName + '.html';
        var tmpl_string = '';

        $.ajax({
            url: tmpl_url,
            method: 'GET',
            async: false,
            contentType: 'text',
            success: function (data) {
                tmpl_string = data;
            }
        });

        $('head').append(tmpl_string);
    }
}
//Refresh after navigating to home page
$(document).on('click', 'a[href="#home"]', function (event) {
    event.preventDefault();
    //myApp.app_router.navigate('/home',true);
    window.location.replace('#home');
    //Backbone.history.navigate("#home");
});

