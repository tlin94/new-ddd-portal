require_template('home');
myApp.HomeView = Backbone.View.extend({
    events: {
		 'click #simBtn':'simulation',
		 'click #materialMgrBtn.btn-home':'loadMaterialMgr',
		 'click #controlMgrBtn.btn-home':'loadControlMgr',
		 'click #okMaterialBtn.btn-home':'createMaterial',
      	 'click #okControlBtn.btn-home':'createControl',
      	 'click #deleteMaterialBtn.btn-home':'delMaterial',
	     'click #deleteControlBtn.btn-home':'delControl',
	     'click #modifyMaterialBtn.btn-home':'modifyMaterial',
	     'click #modifyControlBtn.btn-home':'modifyControl',
	     'click #saveMaterialBtn.btn-home':'saveMaterial',
	     'click #saveControlBtn.btn-home':'saveControl',
	     "change #Load": "selectLoad",
      	 'click #okResetDBBtn':'resetDB',
      	 'click .addslip':'addSlip',
      	 'click .rmslip':'rmSlip', 
    },

    initialize: function(options) {
      
      homeTemplate = _.template($('#home').html());
      this.router = options.router;
      
      return this.render();
      
      
    },

    render: function(eventName) {
	  
      //Render the page template
      $(this.el).html(homeTemplate());
   
	  //initMaterialMgrList and initControlMgrList are defined in config.js
      initMaterialMgrList();
	  initControlMgrList();
          
      return this;
    },
	
	simulation:function(){
		this.router.navigate('/simulation', {trigger: true});
	},
	
	loadMaterialMgr:function(e){
		initMaterialMgrList();
		var nSlip = $("#matBody2").find("input[name='nSlip']").val();
		while(nSlip >1){
			nSlip = $("#matBody2").find("input[name='nSlip']").val();
			$("#matBody2").find(".rmslip").click();
		}
		
		$("#matBody2").find("input,select").not("input[readonly='readonly']").val("");
		$(".matMgrMode1").click();
		$("#materialDialog").modal("show");
	},
	loadControlMgr:function(){
		initControlMgrList();
		$("#ctrlBody2").find("input,select").not("input[readonly='readonly']").val("");
		$(".ctrlMgrMode1").click();
		$("#controlDialog").modal("show");
	},
	//**************************************************************
	// Function: createMaterial
	// @note: create a new material
	//**************************************************************
   	createMaterial:function(e){
   		
   		var json_data = $('#materialDialog .modal-body').children( "div" ).not('.slip-content').
   						find('input,select').serializeFormJSON();
   		
		var o1 = [];
		var slips = $('#materialDialog #matBody2 .slip-content');
		var name = "Slips";
		$.each(slips, function(i,slip) {
			var d1={};
			var a1 = $(slip).find("select,input").serializeArray();
			$.each(a1, function() {
				d1[this.name] = this.value || '';
			});
			o1.push(d1);
		});
		json_data[name]	= o1;	
		
		if (json_data["Material_Type"]!=undefined &&json_data["Material_Type"]!=''){
			var data = JSON.stringify(json_data);
   			console.log(json_data);
	   		$.ajax({
				async: false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=CreateMaterial&data='+data,
				success : function(response) {
					alert("Create a new material");
					//$('#materialDialog').modal('hide');
					//window.location.reload();
				}
		 	});
		}
		else{
			alert("Please enter the valid material name");
		}
		
		initMaterialMgrList();	
   		$('.matMgrMode1').click();
		
   	},
   	
   	//**************************************************************
	// Function: deleteMaterial
	// @note: delete existing material
	//**************************************************************
   	delMaterial:function(e){
   		
   		var matName = $(e.target).parents(".modal-dialog").find(".list-material .list-group-item.active").text();
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=DeleteMaterial&matName=' + matName,
			success : function(response) {
				alert("Delete material");
				//$('#materialDialog').modal('hide');
				//window.location.reload();
			}
		}); 
		
   		initMaterialMgrList();	
   		$('.matMgrMode1').click();

   		
   	},
   	
   	modifyMaterial:function(e){
   		
   		//switch buttons
   		$(e.target).parents(".modal-footer").find("#modifyMaterialBtn").hide();
   		$(e.target).parents(".modal-footer").find("#saveMaterialBtn").show();
   		
   		var self = this;
   		var matName = $(e.target).parents(".modal-dialog").find(".list-material .list-group-item.active").text();
   		var mat2Template = _.template($('#material2').html());
   		var slip3Template = _.template($('#slip3').html());
   		
   		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetMaterial&Material_Name='+matName,
			success : function(response) {
				self.material = response;
				
			}
		}); 
		//show contents
		var material = JSON.parse(self.material);
    	var container = $(".material-preview");
    	$(container).html('').append(mat2Template(material[0]));
    	$(container).find("#Anisotropy").val(material[0]["Anisotropy"]);
    	$(container).find("#Crystal_Type").val(material[0]["Crystal_Type"]);
    	var div = $(".material-preview .slip-contents");
    	$.each(material[0]['Slips'], function(i, data) {
			$(div).append(slip3Template(data));
		});
		$(container).find("input,select").css("width","80%");
		
		
   		
   		
   	},
   	
   	saveMaterial:function(e){
   		
   		//switch buttons
   		$(e.target).parents(".modal-footer").find("#modifyMaterialBtn").show();
   		$(e.target).parents(".modal-footer").find("#saveMaterialBtn").hide();
   		
   		//save new change
   		var json_data = $('#materialDialog .material-preview').children( "div" ).not('.slip-content').
   						find('input,select').serializeFormJSON();
   		
		var o1 = [];
		var slips = $('#materialDialog .material-preview .slip-content');
		var name = "Slips";
		$.each(slips, function(i,slip) {
			var d1={};
			var a1 = $(slip).find("select,input").serializeArray();
			$.each(a1, function() {
				d1[this.name] = this.value || '';
			});
			o1.push(d1);
		});
		json_data[name]	= o1;
   		
   		if (json_data["Material_Type"]!=undefined &&json_data["Material_Type"]!=''){
   			var data = JSON.stringify(json_data);
   			console.log(json_data);
	   		$.ajax({
				async: false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=SaveMaterial&data='+data,
				success : function(response) {
					
				}
		 	});
   		}
   		else{
			alert("Please enter the valid material name");
		}
   		
   		//back to preview 
   		initMaterialMgrList();
  		
   	},
   	
   	//**************************************************************
	// Function: createControl
	// @note: create a new control
	//**************************************************************
   	createControl:function(){
   		var json_data = $('#controlDialog .modal-body').find('input,select').serializeFormJSON();
   		if (json_data["Control_Name"]!=undefined &&json_data["Control_Name"]!=''){
   		var data = JSON.stringify(json_data);
	   		console.log(json_data);
			$.ajax({
				async: false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=CreateControl&data='+data,
				success : function(response) {
					alert("Create a new control");
					//$('#controlDialog').modal('hide');
					//window.location.reload();
				}
		 	});
		}
		else{
			alert("Please enter the valid control name");
		}
		
   		initControlMgrList();
   		$('.ctrlMgrMode1').click();
   	},
   	
   	//**************************************************************
	// Function: deleteControl
	// @note: delete existing simulation control
	//**************************************************************
   	delControl:function(e){
   		
   		var ctrlName = $(e.target).parents(".modal-dialog").find(".list-control .list-group-item.active").text();
		
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=DeleteControl&ctrlName=' + ctrlName,
			success : function(response) {
				alert("Delete control");
				//$('#controlDialog').modal('hide');
				//window.location.reload();
			}
		}); 
		
   		initControlMgrList();
   		$('.ctrlMgrMode1').click();
   	},
   	
   	modifyControl:function(e){
   		
   		//switch buttons
   		$(e.target).parents(".modal-footer").find("#modifyControlBtn").hide();
   		$(e.target).parents(".modal-footer").find("#saveControlBtn").show();
   		
   		var self = this;
   		var ctrlName = $(e.target).parents(".modal-dialog").find(".list-control .list-group-item.active").text();
   		var ctrl2Template = _.template($('#simctrl2').html());
		
   		
   		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSimControl&Control_Name=' + ctrlName,
			success : function(response) {
				self.simctrls = response;
			}
		});
		
		//show contents
    	var simctrls = JSON.parse(self.simctrls);
    	var container = $(".control-preview");
    	var load = simctrls[0]["Load"];
    	$(container).html('').append(ctrl2Template(simctrls[0]));
  		$(container).find("input,select").css("width","80%");
  	
		if (load == "1") {
			$(e.target).parents('.modal-body').find('.applied_force label').html("Applied Strain Rate(s<sup>-1</sup>)");
		} 
		else if (load == "2") {
			$(e.target).parents('.modal-body').find('.applied_force label').html("Applied Stress Rate(MPa)");
		}
   		
   	},
   	
   	saveControl:function(e){
   		//switch buttons
   		$(e.target).parents(".modal-footer").find("#modifyControlBtn").show();
   		$(e.target).parents(".modal-footer").find("#saveControlBtn").hide();
   		
   		//save new changes
   		var json_data = $('#controlDialog .control-preview').find('input,select').serializeFormJSON();
   		console.log(json_data);
   		if (json_data["Control_Name"]!=undefined &&json_data["Control_Name"]!=''){
   			var data = JSON.stringify(json_data);
	   		$.ajax({
				async : false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=SaveSimControl&data=' + data,
				success : function(response) {
				}
			});
		}
		else{
			alert("Please enter the valid control name");
		}
		
   		//back to preview 
   		initControlMgrList();
   		
   		
   	},
   	
   	//**************************************************************
	// Function: selectLoad
	// @note: select load type (stress/strain) 
	//**************************************************************
   	selectLoad:function(e){
   		var load = $(e.target).val();	
		if (load == "1") {
			$(e.target).parents('.modal-body').find('.applied_force label').html("Applied Strain Rate(s<sup>-1</sup>)");
		} else if (load == "2") {
			$(e.target).parents('.modal-body').find('.applied_force label').html("Applied Stress Rate(MPa)");
		}
   	},
   	
   	//**************************************************************
	// Function: resetDB
	// @note: reset the database (only keep default simulations)
	//**************************************************************
   	resetDB:function(){
   		
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=ResetDatabase',
			success : function(response) {
				$('#resetDBDialog').modal('hide');
				window.location.reload();
			}
		}); 

   	},
   	
   	//**************************************************************
	// Function: addSlip
	// @note: add a new Slip
	//**************************************************************
   	addSlip:function(e){
   		var numSlip = parseInt($(e.target).parents('.modal-body').find('#nSlip').val());
   		//console.log(numSlip);
   		numSlip++;
   		$('.modal-body #nSlip').val(numSlip);
   		var numSlip = $('.modal-body #nSlip').val();
   		var div = $(e.target).parents('.modal-body').find('.slip-content').last();		
   		$(div).clone().insertAfter($(div)).find("input,select").val('');
   		
   	},
   	
   	//**************************************************************
	// Function: rmSlip
	// @note: remove the last Slip
	//**************************************************************
   	rmSlip:function(e){
   		var numSlip = parseInt($(e.target).parents('.modal-body').find('#nSlip').val());
   		if(numSlip>1){
   			numSlip--;
   			$('.modal-body #nSlip').val(numSlip);
   			$(e.target).parents('.modal-body').find('.slip-content').last().remove();
   		}

   	},
   	
   	
	   
 });
 

