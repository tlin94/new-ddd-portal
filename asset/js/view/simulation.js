require_template('simulation');
myApp.SimView = Backbone.View.extend({
    events: {
      'click a[href="#reports-board"]':'changeToReport',
      'click a[href="#simulations-board"]':'changeToSim',
      'click #openReportBtn':'openReport',
      'click #delReportBtn':'delReport',
      'click #importSimBtn':'importSim',
      'click #editSimBtn':'editSim',
      'click #delSimBtn':'delSim',
      'click #okNewSimBtn':'okNewSim',
      'click #refreshBtn':'refresh',
    },

    initialize: function(options) {
      
      homeTemplate = _.template($('#simulation').html());
      simTemplate = _.template($('#listSimulation').html());
      reportTemplate= _.template($('#listReport').html());
      this.router = options.router;
      
      var self = this;
      //get simulations
      $.ajax({
		async : false,
		type : "POST",
		url : "action.php",
		data : 'FunctionName=GetAllSimulations',
		success : function(response) {
			self.simulations = response;
		}
	  });
	  //get reports
	  $.ajax({
		async : false,
		type : "POST",
		url : "action.php",
		data : 'FunctionName=GetAllReports',
		success : function(response) {
			self.reports = response;
		}
	  });
      
      return this.render();
      
      
    },

    render: function(eventName) {
		
      //Render the page template
      $(this.el).html(homeTemplate());
      
      //Display all simulations
      if (this.simulations.replace(/(\r\n|\n|\r|\t)/gm, "")!=''){
			var simulations = JSON.parse(this.simulations);
			//console.log(simulations);
			var table = $(this.el).find("#simulations-table");
			var content = $("<tbody></tbody>");
			simulations.forEach(function(sim) {
				content.append(simTemplate(sim));
			});
			$(table).append(content);
			$(table).DataTable(); 
      }
      //Display all reports
      if (this.reports.replace(/(\r\n|\n|\r|\t)/gm, "")!=''){
      		console.log(this.reports);
			var reports = JSON.parse(this.reports);
			
			var table = $(this.el).find("#reports-table");
			var content = $("<tbody></tbody>");
			reports.forEach(function(report) {
				content.append(reportTemplate(report));
			});
			$(table).append(content);
			$(table).DataTable(); 
      }
      
      
      //Make each experiment/report record clickable
      $("body").on("click","#simulations-table tr, #reports-table tr", function(){	
		$("#simulations-table tr.selected, #reports-table tr.selected").removeClass("selected");
		$(this).addClass("selected");
	  });
	  
          
      return this;
    },
    
    //***************************
    //Change Button Group (Simulation/Report)
    //***************************
    changeToReport: function(){
    	$(".btn-simulation").hide();
    	$(".btn-report").show();
    },
    
    changeToSim: function(){
    	$(".btn-report").hide();
    	$(".btn-simulation").show();
    },
    
	//**************************************************************
	// Function: openReport 
	// @note: open a new window for the selected report
	//**************************************************************
	openReport:function(){
		var reportName = $("#reports-table tr.selected").find("td:first").text();
		if(reportName!="" && reportName != undefined){
			alert(reportName);
			this.router.navigate('report/'+reportName, {trigger: true});
			window.location.reload();
		}
		else alert("Please choose a report");
	},
	
	//**************************************************************
	// Function: delReport 
	// @note: delete a selected simulation report
	//**************************************************************
	delReport: function(){		
		var reportName = $("#reports-table tr.selected").find("td:first").text();
		
		if (reportName != "" && reportName != undefined) {
			$.ajax({
				type : "POST",
				url : "action.php",
				data : 'FunctionName=DeleteReport&Report_Name=' + reportName,
				success : function(response) {
				}
			});
			alert(reportName);
			window.location.reload();
		} else
	    alert("No report selected");
		//wlocation.href = "http://localhost/DDD-portal/#reports-board"; //????
	},
	
	//*****************************************
	// Function: importConfiguration 
	// @note: Import the existing input file to configure the simulation
	//*****************************************
	importSim:function (){
	
	},
	
	//*****************************************
	// Function: editSim
	// @note: Direct to the configuration form
	//*****************************************
	editSim:function() {
		var simName = $("#simulations-table tr.selected").find("td:first").text();
		if(simName!="" && simName != undefined){
			this.router.navigate('/config/'+simName,true);
			window.location.reload();
		}
	
	},
	
	//**************************************************************
	// Function: delSimulation
	// @note: delete a selected simulation report 
	//**************************************************************
	delSim:function (){
		var simName = $("#simulations-table tr.selected").find("td:first").text();
		
		if (simName!="" && simName!=undefined)
		{
			$.ajax({
				async:false,
				type : "POST",
				url : "action.php",
				data : 'FunctionName=DeleteExperiment&Experiment_Name=' + simName,
				success : function(response) {
				}
			});
			alert(simName);
			window.location.reload();
						    
		}
		else alert("No simulation selected");
		
	},
	
	//*****************************************
	// Function: okNewExperiment 
	// @note: Verify the simulation name and then create a new simulation
	//*****************************************
	okNewSim:function(){
		var simName = $("#Simulation_Name").val();
		// when modal is hidden, change page to config
		var self =this;
	 	$('#newSimDialog').on('hidden.bs.modal', function (e) {
  			self.router.navigate('/config/'+simName, true);
  			window.location.reload();
	 	});
	 	
		if (simName!="" && simName !=undefined) {	
			$.ajax({
				async : false,
				type : "POST",
				url : "action.php",
				data : 'FunctionName=CreateSimulation&Experiment_Name=' + simName,
				success : function(response) {
					if (response.replace(/(\r\n|\n|\r|\t)/gm, "") == "failure") {
						//alert("This simulation name has been used");
						var content = "<div class='alert alert-warning alert-dismissable'>\
						   <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\
						   <strong>Warning!</strong> Please choose another simulation name\
							</div>";
						$("#newSimDialog .modal-body .alert").remove();
						$("#newSimDialog .modal-body").append(content);
					}
					else{
						console.log("success!");
						$("#newSimDialog").modal('hide');					
					}		
				}
			});
			
		}
		else {
			//alert("Please enter a valid name");
			var content = "<div class='alert alert-warning alert-dismissable'>\
						   <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\
						   <strong>Warning!</strong> Please enter a valid name.\
							</div>";
			$("#newSimDialog .modal-body .alert").remove();
			$("#newSimDialog .modal-body").append(content);
		}
		
	},
	
	refresh:function(){
		window.location.reload();
	},
	   
 });
 



