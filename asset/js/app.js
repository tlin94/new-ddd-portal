var myApp = (function($) {
 
  var AppRouter = Backbone.Router.extend({
    routes: {
      "": "home",
      "home":"home",
      "simulation": "sim",
      "config/:name": "config",
      "report/:name": "report"
    },

    // entry point for the app  
    home: function(){
    	
    	this.view = new myApp.HomeView({
    		router:this,
      		el:$('#main'),
      			
    	});
    	
    },
    
    sim: function(){
    	
    	this.view = new myApp.SimView({
    		router:this,
      		el:$('#main'),
      			
    	});
    },
    
    config: function(simName){
      	//console.log(simName);
    	this.view = new myApp.ConfigView({
    		router:this,
    		el:$('#main'),
    		simName : simName,
    	});
    },
    
    report: function(reportName){
    	//console.log(reportName);
    	this.view = new myApp.ReportView({
      		el:$('#main'),
      		reportName : reportName,
      		router:this,  	
      });
    }
    
    
    
  });

  var initialize = function() {

    var app_router = new AppRouter();
    Backbone.history.start({});
    // reload the page if the link of <a> is external
   	$(document).on('click', 'a:not([data-bypass])', function (evt) {
   		var href = $(this).attr('href');
   		if(href!=undefined){
	   		if(href.indexOf("#") == -1){
			    var protocol = this.protocol + '//';
			    if (href.slice(protocol.length) !== protocol) {
			      evt.preventDefault();
			      app_router.navigate(href, true);
			      window.location.reload();
	    		}
	   		}
	   }
		
  	});
    
  };

  return {
    initialize: initialize
  };

}(jQuery));

// When the DOM is ready
$(document).ready(function() {
  myApp.initialize();
});

