/*=========================================

Section: Database Settings/Cleanupc

===========================================*/ 

--Clear all tables
DROP Table IF EXISTS Configurations;
DROP Table IF EXISTS Experiments;
DROP Table IF EXISTS Volume_Info;
DROP Table IF EXISTS Material;
DROP Table IF EXISTS Slip;
DROP Table IF EXISTS Slip_System;
DROP Table IF EXISTS Layer_Property;
DROP Table IF EXISTS Simulation_Control;
DROP Table IF EXISTS Numerical_Procedure;
DROP Table IF EXISTS Simulation_Options;
DROP Table IF EXISTS Outputs;
DROP Table IF EXISTS Elastic_Stiffness_Matrix;
DROP Table IF EXISTS Matrix_1by3;
DROP Table IF EXISTS Matrix_1by4;
DROP Table IF EXISTS Defect;
DROP Table IF EXISTS Defect_Node;
DROP Table IF EXISTS Reports;
DROP Table IF EXISTS Parameter_Description;

--turn on foreign keys
PRAGMA foreign_keys = On ; 


/*=========================================

Section: CREATE TABLES

===========================================*/ 


CREATE Table Parameter_Description(
Param_ID integer Primary Key AUTOINCREMENT,
Field_Name varchar(255),
Title varchar(1000),
Description varchar(1000)
);

--create table Experiment
CREATE Table Experiments ( -- 6 cols
Exp_ID integer Primary Key AUTOINCREMENT,
Config_ID int,
Experiment_Name varchar(100),
Status varchar(10) Default "pending",
FilePath varchar(150),
LastModified datetime Default CURRENT_TIMESTAMP  ,
Created datetime Default CURRENT_TIMESTAMP,
Foreign Key (Config_ID) References Configurations(Config_ID)
);

--create table Configuration
CREATE Table Configurations ( --8 cols
Config_ID integer Primary Key AUTOINCREMENT,
VoIn_ID int,
SimCo_ID int,
NuPro_ID int,
SimOp_ID int,
Out_ID int,
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP,
Foreign Key (VoIn_ID) References Volume_Info(VoIn_ID),
Foreign Key (SimCo_ID) References Simulation_Control(SimCo_ID),
Foreign Key (NuPro_ID) References Numerical_Procedure(NuPro_ID),
Foreign Key (SimOp_ID) References Simulation_Options(SimOp_ID),
Foreign Key (Out_ID) References Outputs(Out_ID)
);


-- create table Volume_Info
CREATE Table Volume_Info ( --8 cols
VoIn_ID integer Primary Key AUTOINCREMENT,
Volume_Size_x int, 
Volume_Size_y int,
Num_Layer int,
iInterface int,
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP 
);

CREATE Table Material (-- 15 cols
Mat_ID integer Primary Key AUTOINCREMENT,
Material_Type varchar(30),
Mu real, 
Nu real,
ElSt_MTX_ID int,  --Elastic_Stiffness_Matrix
Crystal_Type varchar(20),
Lattice_Param real,
Lattice_Ratios int, --MTX_1by3
Lattice_Angles int, --MTX_1by3
nSlip int,
Mobilityns real,
Mobilitys real,
Anisotropy int,
Material_UserDefined int,
LastModified datetime Default CURRENT_TIMESTAMP,
Created datetime Default CURRENT_TIMESTAMP
);


CREATE Table Slip ( --8 cols
Slip_ID integer Primary Key AUTOINCREMENT,
Mat_ID int,
mode int,
Slip_Name varchar(30), 
Slip_x real, Slip_y real, Slip_z real,
magBurger real,
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP,
Foreign Key (Mat_ID) References Material(Mat_ID) 
);

CREATE Table Layer_Property ( --7 cols 
LaPro_ID integer Primary Key AUTOINCREMENT,
Config_ID int,
Mat_ID int,
Thickness int,  
Euler_Angles int, --MTX_1by3
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP,
Foreign Key (Mat_ID) References Material(Mat_ID),
Foreign Key (Config_ID) References Configurations(Config_ID)
);

CREATE Table Simulation_Control ( -- 25 cols
SimCo_ID integer Primary Key AUTOINCREMENT,
Control_Name varchar(100),
Load int,
iRelaxtion int,
iRelaxStep int,
AF_01 real,AF_02 real,AF_03 real,
AF_04 real,AF_05 real,AF_06 real,
AF_07 real,AF_08 real,AF_09 real,
IAS_01 real,IAS_02 real,IAS_03 real,
IAS_04 real,IAS_05 real,IAS_06 real,
IAS_07 real,IAS_08 real,IAS_09 real,
Control_UserDefined int,
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP 

);


CREATE Table Numerical_Procedure ( -- 18 cols
NuPro_ID integer Primary Key AUTOINCREMENT,
ILoop_Time int,
DTIME_MAX real,
DTIME_MIN real,
MAX_QUAD int,
NPOINT_I int,
MAX_node int,
MAX_Neighbor int,
dpMaxAveLength real,
nbox int, --MTX_1by3
reebox int,
dcrit real,
iArr int,
iFEM int,
nElmtFe int, --MTX_1by3
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP 
);

CREATE Table Simulation_Options ( --13 cols
SimOp_ID integer Primary Key AUTOINCREMENT,
CheckNeiBur int,
LogInteraction int,
iCrossSlip int,
FullyPeriodic int,
iInertial int,
ioutFe int,
nBoxFe int,
iSplineFe int,
DispNodes int, --MTX_1by3
iXRD int,
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP 

);

CREATE Table Outputs ( -- 6 cols
Out_ID integer Primary Key AUTOINCREMENT,
iOutFreq int,
iPastFreq int,
iDebugMode int,
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP 

);

CREATE Table Elastic_Stiffness_Matrix( -- 39 cols
ElSt_MTX_ID integer Primary Key AUTOINCREMENT,
E_01 real,E_02 real,E_03 real,E_04 real,E_05 real,E_06 real,
E_07 real,E_08 real,E_09 real,E_10 real,E_11 real,E_12 real,
E_13 real,E_14 real,E_15 real,E_16 real,E_17 real,E_18 real,
E_19 real,E_20 real,E_21 real,E_22 real,E_23 real,E_24 real,
E_25 real,E_26 real,E_27 real,E_28 real,E_29 real,E_30 real,
E_31 real,E_32 real,E_33 real,E_34 real,E_35 real,E_36 real,
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP 
);

CREATE Table Matrix_1by3 ( -- 6 cols
MTX_1by3_ID integer Primary Key AUTOINCREMENT,
E_01 real, E_02 real, E_03 real,
LastModified datetime Default CURRENT_TIMESTAMP ,
Created datetime Default CURRENT_TIMESTAMP 
);

CREATE Table Defect ( --12 cols
Def_ID integer Primary Key AUTOINCREMENT,
Config_ID int,
Slsys_ID int,
LayerPos int,
Form_Type int,
nNode int,
CenterPos int, --MTX_1by3
LoopRad real, CenterLoop_01 real, CenterLoop_02 real,
LastModified datetime Default CURRENT_TIMESTAMP,
Created datetime Default CURRENT_TIMESTAMP,
Foreign Key (Config_ID) References Configurations(Config_ID),
Foreign Key (Slsys_ID) References Slip_System(Slsys_ID)
);

CREATE Table Slip_System( --13 cols
Slsys_ID integer Primary Key AUTOINCREMENT,
Crystal varchar(20),
System_Name varchar(50),
Structure varchar(20),
mode int,
nInd int,
M_01 real, M_02 real, M_03 real, M_04 real, 
B_01 real, B_02 real, B_03 real, B_04 real, 
LastModified datetime Default CURRENT_TIMESTAMP,
Created datetime Default CURRENT_TIMESTAMP
);

CREATE Table Defect_Node( -- 7 cols
DefNo_ID integer Primary Key AUTOINCREMENT,
Def_ID int,
Coordinate_01 real, 
Coordinate_02 real,
LastModified datetime Default CURRENT_TIMESTAMP,
Created datetime Default CURRENT_TIMESTAMP,
Foreign Key (Def_ID) References Defect(Def_ID)
);


CREATE TABLE Reports( --7 cols
Report_ID integer Primary Key AUTOINCREMENT,
Exp_ID int,
Report_Name varchar(100),
Experiment_Name varchar(100),
FilePath varchar(150),
LastModified datetime Default CURRENT_TIMESTAMP,
Created datetime Default CURRENT_TIMESTAMP,
Foreign Key (Exp_ID) References Experiments(Exp_ID)
);

/*=========================================

Section: CREATE TRIGGERS

===========================================*/ 


--create triggers to update lastmodified

CREATE Trigger update_lastmodified_exp After UPDATE on Experiments
	Begin
		UPDATE Experiments SET LastModified = datetime('now') WHERE Exp_ID = New.Exp_ID;
	End;
	
CREATE Trigger update_lastmodified_config After UPDATE on Configurations
	Begin
		UPDATE Configurations SET LastModified = datetime('now') WHERE Config_ID = New.Config_ID;
	End;

CREATE Trigger update_lastmodified_voin After UPDATE on Volume_Info
	Begin
		UPDATE Volume_Info SET LastModified = datetime('now') WHERE VoIn_ID = New.VoIn_ID;
	End;

CREATE Trigger update_lastmodified_lapro After UPDATE on Layer_Property
	Begin
		UPDATE Layer_Property SET LastModified = datetime('now') WHERE LaPro_ID = New.LaPro_ID;
	End;

--create triggers to delete the values from a chosen experiment

Create Trigger delete_exp After DELETE on Experiments
	Begin
		DELETE FROM Configurations WHERE Config_ID = Old.Config_ID;
		DELETE FROM Reports WHERE Exp_ID = Old.Exp_ID;
	End;

Create Trigger delete_config After DELETE on Configurations
	Begin
		DELETE FROM Volume_Info WHERE VoIn_ID = Old.VoIn_ID;
		DELETE FROM Numerical_Procedure WHERE NuPro_ID = Old.NuPro_ID;
		DELETE FROM Simulation_Options WHERE SimOp_ID = Old.SimOp_ID;
		DELETE FROM Outputs WHERE Out_ID = Old.Out_ID;	
	End;
	
Create Trigger delete_layerproerty After DELETE on Configurations
	Begin
		DELETE FROM Layer_Property WHERE Config_ID = Old.Config_ID;
	End;
	
Create Trigger delete_material After DELETE on Layer_Property
	Begin
		DELETE FROM Material WHERE Mat_ID = Old.Mat_ID AND Material_UserDefined = 1;
	End;

Create Trigger delete_slip After DELETE on Material
	Begin
		DELETE FROM Slip WHERE Mat_ID = Old.Mat_ID;
	End;

Create Trigger delete_defect After DELETE on Configurations
	Begin
		DELETE FROM Defect WHERE Config_ID = Old.Config_ID;
	End;

Create Trigger delete_defectnode After DELETE on Defect
	Begin
		DELETE FROM Defect_Node WHERE  Def_ID = Old.Def_ID;
	End;

Create Trigger delete_mtx1 After DELETE on Layer_Property
	Begin
		DELETE FROM Matrix_1by3 WHERE MTX_1by3_ID = Old.Euler_Angles ;
	End;

Create Trigger delete_mtx3 After DELETE on Numerical_Procedure
	Begin
		DELETE FROM Matrix_1by3 WHERE MTX_1by3_ID = Old.nbox OR MTX_1by3_ID = Old.nElmtFe ;
	End;

Create Trigger delete_mtx4 After DELETE on Simulation_Options
	Begin
		DELETE FROM Matrix_1by3 WHERE MTX_1by3_ID = Old.DispNodes;
	End;
	
Create Trigger delete_mtx5 After DELETE on Defect
	Begin
		DELETE FROM Matrix_1by3 WHERE MTX_1by3_ID = Old.CenterPos;
	End;

Create Trigger delete_mtx6 After DELETE on Material
	Begin
		DELETE FROM Elastic_Stiffness_Matrix WHERE ElSt_MTX_ID = Old.ElSt_MTX_ID;
		DELETE FROM Matrix_1by3 WHERE MTX_1by3_ID = Old.Lattice_Ratios OR MTX_1by3_ID = Old.Lattice_Angles;
	End;


