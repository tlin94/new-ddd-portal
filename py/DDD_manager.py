#!/usr/bin/python
import SimpleHTTPServer
import SocketServer
import sys, shutil ,subprocess, os, tempfile
import xml.etree.ElementTree as ET
import numpy as np
import base64
import time
import sqlite3
import json
import DDD_util
from multiprocessing import Process

PATH_NEW_DDD = "/Users/tien-julin/Documents/workspace/7-NoPlane2/code"
PATH_DB = "/Users/tien-julin/Sites/DDD-portal/db/"
PATH_DefaultDB = "/Users/tien-julin/Sites/DDD-portal/db/defaultDB/" #
PATH_DOWNLOAD = "/Users/tien-julin/Sites/DDD-portal/temp/"
DataBase = "micro.db"

class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
	def do_GET(self):
   		if self.path == '/':
   	  		self.send_response(200)
   			self.send_header('Content-type', 'text/html')
   			self.end_headers()
   			return  						
   		elif self.path == "/index.html":
   			SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)
   			'''self.send_error(404, "requested path not available")'''
   			return
   		elif self.path.endswith('.txt'):
   			# format: /DDD-portal/download/{simulation folder}/{file}.txt
			self.send_response(200)
   			self.send_header('Content-type', 'text/plain')
   			self.end_headers()
   			simFolder = self.path.split('/')[3]
   			txtfile = self.path.split('/')[4]
   			try:
   				f = open(PATH_NEW_DDD + "/Temp/"+simFolder+"/"+txtfile,"r")
			   	self.wfile.write(f.read())
				f.close()
			finally:
				if f is not None:
					f.close()
		elif self.path.endswith('.zip'):
   			# format: /DDD-portal/download/{simulation folder}/{file}.zip
			self.send_response(200)
   			self.send_header('Content-type', 'application/zip')
   			self.end_headers()
   			simFolder = self.path.split('/')[3]
   			zipfile = self.path.split('/')[4]
   			try:
   				f = open(PATH_NEW_DDD + "/Temp/"+simFolder+"/"+zipfile,"r")
			   	self.wfile.write(f.read())
				f.close()
			finally:
				if f is not None:
					f.close()	
		elif self.path.endswith('.dat'):
   			# format: /DDD-portal/download/{simulation folder}/{file}.dat
			self.send_response(200)
   			self.send_header('Content-type', 'application/zip')
   			self.end_headers()
   			simFolder = self.path.split('/')[3]
   			zipfile = self.path.split('/')[4]
   			try:
   				f = open(PATH_NEW_DDD + "/Temp/"+simFolder+"/"+zipfile,"r")
			   	self.wfile.write(f.read())
				f.close()
			finally:
				if f is not None:
					f.close()
			
			
	def do_POST(self) :					
		if self.path == "/receiveXML.html" :						
			# get XML message
			length = int(self.headers.getheader('content-length')) 
			request = self.rfile.read(length)
			print "############################## XML Content #####################################"
			print request
			root = ET.fromstring(request)
			# run Simulation
			if root[0].text == 'RunSimulation' :
				SimFunction =  MySimFunction ()
				SimFunction.runSimulation(root[1].text, root[2].text) 
				self.wfile.write("finished")
				return
			elif root[0].text == 'GetStatsData' :
				OpFunction = MyOpFunction()
				json_data = OpFunction.getStatistics(root[1].text, root[2].text, root[3].text,root[4].text) 
				self.wfile.write(json_data)
				return
			elif root[0].text == 'CreateSimulation':
				OpFunction = MyOpFunction()
				response = OpFunction.createSimulation(root[1].text)
				self.wfile.write(response)	
				return
			elif root[0].text == 'CreateSlipSystem':
				OpFunction = MyOpFunction()
				decoded_data = base64.b64decode(root[1].text)
				response = OpFunction.createSlipSys(decoded_data)
				self.wfile.write(response)	
				return
			elif root[0].text == 'CreateCrystal':
				OpFunction = MyOpFunction()
				decoded_data = base64.b64decode(root[1].text)
				response = OpFunction.createCrystal(decoded_data)
				self.wfile.write(response)	
				return
			elif root[0].text == 'CreateControl':
				OpFunction = MyOpFunction()
				decoded_data = base64.b64decode(root[1].text)
				response = OpFunction.createControl(decoded_data)
				self.wfile.write(response)	
				return
			elif root[0].text == 'CreateMaterial':
				OpFunction = MyOpFunction()
				decoded_data = base64.b64decode(root[1].text)
				response = OpFunction.createMaterial(decoded_data)
				self.wfile.write(response)	
				return
			elif root[0].text == 'ExportConfiguration':
				OpFunction = MyOpFunction()
				json_data = OpFunction.exportConfiguration(root[1].text)
				self.wfile.write(json_data)	
				return
			elif root[0].text == 'SaveConfiguration':
				OpFunction = MyOpFunction()
				OpFunction.saveConfiguration(root[1].text,root[2].text,root[3].text)
				return	
			elif root[0].text == 'SaveSlipSystem':
				OpFunction = MyOpFunction()
				decoded_data = base64.b64decode(root[1].text)
				response = OpFunction.saveSlipSys(decoded_data)
				self.wfile.write(response)	
				return
			elif root[0].text == 'SaveSimControl':
				OpFunction = MyOpFunction()
				decoded_data = base64.b64decode(root[1].text)
				response = OpFunction.saveControl(decoded_data)
				self.wfile.write(response)	
				return
			elif root[0].text == 'SaveMaterial':
				OpFunction = MyOpFunction()
				decoded_data = base64.b64decode(root[1].text)
				response = OpFunction.saveMaterial(decoded_data)
				self.wfile.write(response)	
				return
			elif root[0].text == 'GetAllSimulations':
				OpFunction = MyOpFunction()
				json_data = OpFunction.getAllSimulations()
				self.wfile.write(json_data)
				return
			elif root[0].text == 'GetAllReports':
				OpFunction = MyOpFunction()
				json_data = OpFunction.getAllReports()
				self.wfile.write(json_data)
				return
			elif root[0].text == 'DeleteExperiment':
				OpFunction = MyOpFunction()
				response =OpFunction.deleteExperiment(root[1].text)
				self.wfile.write(response)						
				return
			elif root[0].text == 'DeleteReport' :
				OpFunction = MyOpFunction()
				response =OpFunction.deleteReport(root[1].text)
				self.wfile.write(response)
				return
			elif root[0].text == 'DeleteSlipSystem' :
				OpFunction = MyOpFunction()
				response =OpFunction.deleteSlipSys(root[1].text,root[2].text)
				self.wfile.write(response)
				return
			elif root[0].text == 'DeleteCrystal' :
				OpFunction = MyOpFunction()
				response =OpFunction.deleteCrystal(root[1].text)
				self.wfile.write(response)
				return
			elif root[0].text == 'DeleteMaterial' :
				OpFunction = MyOpFunction()
				response =OpFunction.deleteMaterial(root[1].text)
				self.wfile.write(response)
				return
			elif root[0].text == 'DeleteControl' :
				OpFunction = MyOpFunction()
				response =OpFunction.deleteControl(root[1].text)
				self.wfile.write(response)
				return	
			elif root[0].text == 'LoadExperiment':
				OpFunction = MyOpFunction()
				json_data = OpFunction.getConfiguration(root[1].text)
				self.wfile.write(json_data)
				return
			elif root[0].text == 'GetReportPath':
				OpFunction = MyOpFunction()
				response= OpFunction.getReportPath(root[1].text)
				self.wfile.write(response)
				return
			elif root[0].text == 'GetParamDescription':
				OpFunction = MyOpFunction()
				json_data= OpFunction.getParamDescription()
				self.wfile.write(json_data)
				return
			elif root[0].text == 'GetMaterial':
				OpFunction = MyOpFunction()
				json_data= OpFunction.getMaterial(root[1].text)
				self.wfile.write(json_data)
				return
			elif root[0].text == 'GetSimControl':
				OpFunction = MyOpFunction()
				json_data= OpFunction.getSimControl(root[1].text)
				self.wfile.write(json_data)
				return
			elif root[0].text == 'GetCrystal':
				OpFunction = MyOpFunction()
				json_data= OpFunction.getCrystal(root[1].text)
				self.wfile.write(json_data)
				return
			elif root[0].text == 'GetSlipSystem':
				OpFunction = MyOpFunction()
				if root[1].text!=None:
					slipSysName = base64.b64decode(root[1].text)
					json_data= OpFunction.getSlipSys(slipSysName,root[2].text)
				else:
					json_data= OpFunction.getSlipSys(root[1].text,root[2].text)
				self.wfile.write(json_data)
				return
			elif root[0].text == 'ResetDatabase':
				OpFunction = MyOpFunction()
				response = OpFunction.resetDatabase()
				self.wfile.write(response)
				return
											     
					
class MySimFunction :
	def runSimulation(self, name, nProcessors):
		print "Operation:----------------Run Simulation with %s processors--------------------" %nProcessors 
		
		#create a folder for the simulation results and add copy run.sh
		tempDir = PATH_NEW_DDD + '/Temp'
		if not os.path.isdir(tempDir):
			os.mkdir(tempDir)	
			
		simDir = tempfile.mkdtemp('','Sim_', tempDir)
		shutil.copy2(PATH_NEW_DDD + '/run.sh',simDir)
		cwDir = os.getcwd();
		
		# generate a new report in database
		func = MyOpFunction ()
		func.createReport(name,simDir)
		# change status to "submitted"
		self.changeStatusToSubmitted(name)
		
		# create two input files: 1.material_input_new.txt, 2.interaction_geom_input.txt
		os.chdir(simDir)	
		DDD_util.writeInputFile(name)
		
		# add the path of micro to the env variable "PATH" 
		os.environ["PATH"] += os.pathsep  + PATH_NEW_DDD
		proc = subprocess.Popen(simDir + "/run.sh %s >> console.txt" %nProcessors, shell=True, env=os.environ, stderr=subprocess.STDOUT, stdout=subprocess.PIPE) # non-blocking
		os.chdir(cwDir)
		print proc.pid
		print "###################################################\n\n"
		return
	
	
	def changeStatusToSubmitted(self,name):
		print "Operation:----------------Submit Configuration--------------------" 
		#!============== Open database & Define row_factory ================!#
		conn = sqlite3.connect(PATH_DB + DataBase)
		conn.row_factory = DDD_util.namedtuple_factory
		c = conn.cursor()
		
		c.execute("UPDATE Experiments SET Status='%s' WHERE Experiment_Name='%s'" %("submitted",name))
		conn.commit()
		conn.close()
		print "The status of Simulation %s is changed to submitted" %name
		print "###################################################\n\n"	
		return 
	
	
	def detectDDDSimulation(self):
		while True:
			print "Operation:----------------Detect Compeletion of Submitted Simulations--------------------" 
			#!============== Open database & Define row_factory ================!#
			conn = sqlite3.connect(PATH_DB + DataBase)
			conn.row_factory = DDD_util.namedtuple_factory
			c = conn.cursor()
			
			# clean non-used material/control (user defined)
			c.execute("DELETE FROM Material WHERE Mat_ID = \
			(SELECT Mat_ID FROM (SELECT Material.Mat_ID, Layer_Property.LaPro_ID FROM Material LEFT JOIN Layer_Property ON Material.Mat_ID = Layer_Property.Mat_ID WHERE Material_UserDefined =1) WHERE LaPro_ID IS NULL)");
			c.execute("DELETE FROM Simulation_Control WHERE SimCo_ID = \
			(SELECT SimCo_ID FROM (SELECT Simulation_Control.SimCo_ID, Configurations.Config_ID FROM Simulation_Control LEFT JOIN Configurations ON Simulation_Control.SimCo_ID = Configurations.SimCo_ID WHERE Control_UserDefined =1) WHERE Config_ID IS NULL)");
			
			# check simulation status / zip necessary folders 
			c.execute("SELECT Exp_ID FROM Experiments WHERE Status='submitted'")
			simulations = c.fetchall()
			for sim in simulations:
				c.execute("SELECT Experiment_Name, FilePath FROM Reports WHERE Exp_ID=%d" %sim[0])
				temp = c.fetchone(); simDir = temp[1]; name = temp[0]
				
				logfile = simDir +"/log.txt"
				if os.path.isfile(logfile):
					with open(logfile,'rb') as f:
						lines = f.readlines()
					print int(lines[0])
					if (int(lines[0])!=0):
						c.execute("UPDATE Experiments SET Status='%s' WHERE Experiment_Name='%s'" %("crashed",name))
					else:				
						file = simDir + "/FINISH.txt"
						segsFolder = "loops/segs"
						fieldsFolder = "StressLoops"
						if os.path.isfile(file):
							#print "true"
							cwDir = os.getcwd();
							os.chdir(simDir)
							subprocess.call("zip -r segs "+segsFolder, shell=True)
							subprocess.call("zip -r internalfields "+fieldsFolder, shell=True)
							os.chdir(cwDir)
							c.execute("UPDATE Experiments SET Status='%s' WHERE Experiment_Name='%s'" %("completed",name))
					
					
			conn.commit()
			conn.close()
			time.sleep(20)
			
		
	def stopSim(self):
		print "Stop Simualtion"
		
		
				
class MyOpFunction :
	
	def getReportPath(self,name):
		print "Operation:----------------Get Report Path--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("SELECT FilePath FROM Reports WHERE Report_Name='%s'" %name)
		filepath = c.fetchone()[0]
		conn.close()
		print filepath
		print "##########################################################################\n\n\n"
		return filepath
	
	def getStatistics(self,path,filename,xaxis,yaxis):
		print "Operation:----------------Get Statistics--------------------"
		file = path+"/"+filename
		range1 = lambda start, end:range(start, end+1)
		data=[];stats=[]
		with open(file,'r') as ifile:
			lines = ifile.readlines()
		for i in range1(1,len(lines)-1):
			 data.append(lines[i].split())	 
		data = np.array(data)
		stats.append(data[:,int(xaxis)-1]); stats.append(data[:,int(yaxis)-1]);

		d= {};o1=[]
		for x, y in zip(stats[0],stats[1]):
			d1={}
			d1["x"] = x; d1["y"] =y 
			o1.append(d1)
		d["Points"]=o1
		json_data = json.dumps(d)
		
		print json_data
		print "##########################################################################\n\n\n"
		return json_data
	
	def getParamDescription(self):
		print "Operation:----------------Get Parameter Description--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("SELECT * FROM Parameter_Description")
		parameters = c.fetchall();
		
		d={}
		for p in parameters:
			d[p[1]]={"title":p[2],"description":p[3]}
		
		
		conn.close()
		json_data = json.dumps(d)
		print json_data
		print "##########################################################################\n\n\n"
		return json_data
	
	def getSimControl(self,ctrlName):
		print "Operation:----------------Get Simulation Control--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		conn.row_factory = DDD_util.namedtuple_factory
		c = conn.cursor()
		
		if ctrlName==None:
			c.execute("SELECT * FROM Simulation_Control WHERE Control_UserDefined = 0")	
		else:
			c.execute("SELECT * FROM Simulation_Control WHERE Control_Name ='%s' AND Control_UserDefined = 0" %ctrlName)
		
		data= []
		cols = [x[0] for x in c.description] 
		for row in c.fetchall():
			d = {}
 			for col, val in zip(cols, row):
 				d[col] = val
		 	
		 	data.append(d)
		
		conn.close()
		json_data = json.dumps(data)
		print json_data
		print "##########################################################################\n\n\n"
		return json_data	
		
	def getMaterial(self,matName):
		print "Operation:----------------Get Material--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		conn.row_factory = DDD_util.namedtuple_factory
		c = conn.cursor()
		
		if matName==None:
			c.execute("SELECT * FROM Material WHERE Material_UserDefined = 0")	
		else:
			c.execute("SELECT * FROM Material WHERE Material_Type ='%s' AND Material_UserDefined = 0" %matName)	
		data= []
		#cols = [x[0] for x in c.description] 
		for row in c.fetchall():
 			c.execute('SELECT * FROM Elastic_Stiffness_Matrix WHERE ElSt_MTX_ID = %d' %row.ElSt_MTX_ID)
 			data_elmtx =  c.fetchone()
 			c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %row.Lattice_Ratios )
			data_lattice_ratios = c.fetchone()
			c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %row.Lattice_Angles )
			data_lattice_angles = c.fetchone()
			c.execute('SELECT * FROM Slip WHERE Mat_ID= %d' %row.Mat_ID )
			data_slip = c.fetchall()
 			
 			d = {}
 			cols = row._fields
 			for col, val in zip(cols, row):
 				d[col] = val
 			
			o1=[]
			for i in range(len(data_slip)):	
				d1={}
				cols = data_slip[i]._fields; vals=list(data_slip[i])
				for col, val in zip(cols,vals):
					d1[col] = val
				o1.append(d1)
			d["Slips"] = o1
			 
			mtx = data_elmtx
 			d["E_01"] = mtx.E_01; d["E_02"] = mtx.E_02; d["E_03"] = mtx.E_03; d["E_04"] = mtx.E_04; d["E_05"] = mtx.E_05; d["E_06"] = mtx.E_06;
			d["E_07"] = mtx.E_07; d["E_08"] = mtx.E_08; d["E_09"] = mtx.E_09; d["E_10"] = mtx.E_10; d["E_11"] = mtx.E_11; d["E_12"] = mtx.E_12;
			d["E_13"] = mtx.E_13; d["E_14"] = mtx.E_14; d["E_15"] = mtx.E_15; d["E_16"] = mtx.E_16; d["E_17"] = mtx.E_17; d["E_18"] = mtx.E_18;
			d["E_19"] = mtx.E_19; d["E_20"] = mtx.E_20; d["E_21"] = mtx.E_21; d["E_22"] = mtx.E_22; d["E_23"] = mtx.E_23; d["E_24"] = mtx.E_24;
			d["E_25"] = mtx.E_25; d["E_26"] = mtx.E_26; d["E_27"] = mtx.E_27; d["E_28"] = mtx.E_28; d["E_29"] = mtx.E_29; d["E_30"] = mtx.E_30;
			d["E_31"] = mtx.E_31; d["E_32"] = mtx.E_32; d["E_33"] = mtx.E_33; d["E_34"] = mtx.E_34; d["E_35"] = mtx.E_35; d["E_36"] = mtx.E_36;
 			mtx = data_lattice_ratios
 			d["Lattice_Ratios_x"] = mtx.E_01; d["Lattice_Ratios_y"] = mtx.E_02; d["Lattice_Ratios_z"] = mtx.E_03	
 			mtx = data_lattice_angles
 			d["Lattice_Angles_x"] = mtx.E_01; d["Lattice_Angles_y"] = mtx.E_02; d["Lattice_Angles_z"] = mtx.E_03
		 	
 			data.append(d)
		
		conn.close()
		json_data = json.dumps(data)
		print json_data
		print "##########################################################################\n\n\n"
		return json_data
	
	def getCrystal(self,crystalName):
		print "Operation:----------------Get Crystal--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		conn.row_factory = DDD_util.namedtuple_factory
		c = conn.cursor()
		
		c.execute("SELECT * FROM Slip_System WHERE Crystal = '%s'" %crystalName)
		data={}
		slipSys = c.fetchall()
		numMode=0;arrMode=[]
		for i in range(len(slipSys)):
			mode=slipSys[i].mode
			if ( mode not in arrMode):
				arrMode.append(mode)
				numMode=numMode+1
		
		data["nMode"]=numMode;
		conn.close()
		json_data = json.dumps(data)
		print json_data
		print "##########################################################################\n\n\n"
		return json_data
	
	def getSlipSys(self,slipSysName,condition):
		print "Operation:----------------Get Slip System--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		conn.row_factory = DDD_util.namedtuple_factory
		c = conn.cursor()
		
		if condition==None:
			if slipSysName == None: #get all slip systems
				c.execute("SELECT * FROM Slip_System")	
				data= []
				#cols = [x[0] for x in c.description] 
				for row in c.fetchall():
					d = {}
		 			cols = row._fields
		 			for col, val in zip(cols, row):
		 				d[col] = val
				 	
				 	data.append(d)
				 		
				conn.close()
				json_data = json.dumps(data)
				#print json_data
				print "##########################################################################\n\n\n"
				return json_data	
			else: #get one desired system by name
				print slipSysName
				c.execute("SELECT * FROM Slip_System WHERE System_Name='%s'" %slipSysName)	
				data= []
				#cols = [x[0] for x in c.description] 
				for row in c.fetchall():
					d = {}
		 			cols = row._fields
		 			for col, val in zip(cols, row):
		 				d[col] = val
				 	
				 	data.append(d)
				 		
				conn.close()
				json_data = json.dumps(data)
				print json_data
				print "##########################################################################\n\n\n"
				return json_data
				
		else: #get one desired system by condition(milleridx, burgeridx)
			d={}
			temp = json.loads(condition)
			m = [float(temp["M_01"]),float(temp["M_02"]),float(temp["M_03"]),float(temp["M_04"])]
			b = [float(temp["B_01"]),float(temp["B_02"]),float(temp["B_03"]),float(temp["B_04"])]
			crystal = temp["Crystal"]
			
			mm = m; bb =b
			mb = (crystal,)+tuple(mm+bb)
			c.execute("SELECT System_Name FROM Slip_System WHERE Crystal=? AND M_01=? AND M_02=? AND M_03=? AND M_04=? AND B_01=? AND B_02=? AND B_03=? AND B_04=?",(mb))
			slip_systems = c.fetchall()
			if len(slip_systems)==1:
				d["result"] = "success"
				d["System_Name"] = slip_systems[0][0]
				json_data = json.dumps(d)
				conn.close()
				print json_data
				print "##########################################################################\n\n\n"
				return json_data
			
			mm = [e*(-1) for e in m]; bb =b
			mb = (crystal,)+tuple(mm+bb)
			c.execute("SELECT System_Name FROM Slip_System WHERE Crystal=? AND M_01=? AND M_02=? AND M_03=? AND M_04=? AND B_01=? AND B_02=? AND B_03=? AND B_04=?",(mb))
			slip_systems = c.fetchall()
			if len(slip_systems)==1:
				d["result"] = "success"
				d["System_Name"] = slip_systems[0][0]
				json_data = json.dumps(d)
				conn.close()
				print json_data
				print "##########################################################################\n\n\n"
				return json_data
			
			mm = [e*(-1) for e in m]; bb=[e*(-1) for e in b]
			mb = (crystal,)+tuple(mm+bb)
			c.execute("SELECT System_Name FROM Slip_System WHERE Crystal=? AND M_01=? AND M_02=? AND M_03=? AND M_04=? AND B_01=? AND B_02=? AND B_03=? AND B_04=?",(mb))
			slip_systems = c.fetchall()
			if len(slip_systems)==1:
				d["result"] = "success"
				d["System_Name"] = slip_systems[0][0]
				json_data = json.dumps(d)
				conn.close()
				print json_data
				print "##########################################################################\n\n\n"
				return json_data
			
			mm = m; bb =[e*(-1) for e in b]
			mb = (crystal,)+tuple(mm+bb)
			c.execute("SELECT System_Name FROM Slip_System WHERE Crystal=? AND M_01=? AND M_02=? AND M_03=? AND M_04=? AND B_01=? AND B_02=? AND B_03=? AND B_04=?",(mb))
			slip_systems = c.fetchall()
			if len(slip_systems)==1:
				d["result"] = "success"
				d["System_Name"] = slip_systems[0][0]
				json_data = json.dumps(d)
				conn.close()
				print json_data
				print "##########################################################################\n\n\n"
				return json_data

			#No corresponding results found
			d["result"] = "failure"
			json_data = json.dumps(d)
			conn.close()
			#print json_data
			print "##########################################################################\n\n\n"
			return json_data
		
	
	def getConfiguration(self,name):
		print "Operation:----------------Edit Experiment--------------------" 
		#!============== Open database & Define row_factory ================!#
		conn = sqlite3.connect(PATH_DB + DataBase)
		conn.row_factory = DDD_util.namedtuple_factory
		c = conn.cursor()
		
		#!================ Get ID for all tables ===========================!#
		c.execute("SELECT Exp_ID,Status FROM Experiments WHERE Experiment_Name='%s'" %name)
		data = c.fetchone()
		Exp_ID = data[0]; status =  data[1]
		c.execute('SELECT Config_ID FROM Experiments WHERE Exp_ID = %d' %Exp_ID)	
		Config_ID = c.fetchone()[0]
		c.execute('SELECT VoIn_ID, SimCo_ID, NuPro_ID, SimOp_ID, Out_ID FROM Configurations WHERE Config_ID = %d' %Config_ID)	
		data_id = c.fetchone()
		VoIn_ID = data_id[0]; SimCo_ID = data_id[1]; NuPro_ID = data_id[2]; SimOp_ID = data_id[3];Out_ID = data_id[4];
		
		#!===================== Get Data By Query ===========================!#
		#------- Volume Info ---------#
		c.execute('SELECT * FROM Volume_Info WHERE VoIn_ID = %d' %VoIn_ID)
		data_volume_info = c.fetchone()
		#------- Layer Property ---------#
		c.execute('SELECT * FROM Layer_Property WHERE Config_ID = %d' %Config_ID)
		data_layer_property = c.fetchall()
		data_euler_angles=[]; data_material = []
		for record in data_layer_property:
			c.execute('SELECT * FROM Material WHERE Mat_ID = %d' %record.Mat_ID)
			data_material.append(c.fetchone())
			c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.Euler_Angles )
			data_euler_angles.append(c.fetchone())
		#------- Material & Slip ---------#	
		data_elastic_stiffness_mtx=[];data_lattice_ratios=[];data_lattice_angles=[];data_slip=[]
		for record in data_material:
			c.execute('SELECT * FROM Elastic_Stiffness_Matrix WHERE ElSt_MTX_ID = %d' %record.ElSt_MTX_ID)
			data_elastic_stiffness_mtx.append(c.fetchone())
			c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.Lattice_Ratios )
			data_lattice_ratios.append(c.fetchone())
			c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.Lattice_Angles )
			data_lattice_angles.append(c.fetchone())
			c.execute('SELECT * FROM Slip WHERE Mat_ID = %d' %record.Mat_ID)
			data_slip.append(c.fetchall())
		
		#------- Simulation Control ---------#
		c.execute('SELECT * FROM Simulation_Control WHERE SimCo_ID = %d' %SimCo_ID)
		data_simulation_control = c.fetchone()
		
		data_initial_applied_stress = c.fetchone()
		#------- Numerical Procedure ---------#
		c.execute('SELECT * FROM Numerical_Procedure WHERE NuPro_ID = %d' %NuPro_ID)
		data_numerical_procedure = c.fetchone()
		c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %data_numerical_procedure.nbox)
		data_nbox = c.fetchone()
		c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %data_numerical_procedure.nElmtFe)
		data_nElmtFe = c.fetchone()	
		#------- Simulation Options ---------#
		c.execute('SELECT * FROM Simulation_Options WHERE SimOp_ID = %d' %SimOp_ID)
		data_simulation_options = c.fetchone()
		c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %data_simulation_options.DispNodes)
		data_dispNodes = c.fetchone()
		#------- Outputs ---------#
		c.execute('SELECT * FROM Outputs WHERE Out_ID = %d' %Out_ID)
		data_outputs = c.fetchone()
		#------- Defect ---------#
		c.execute('SELECT * FROM Defect WHERE Config_ID = %d' %Config_ID)
		data_defect = c.fetchall()
		data_slip_sys=[];data_centerpos=[]
		for record in data_defect:
			c.execute('SELECT * FROM Slip_System WHERE Slsys_ID= %d' %record.Slsys_ID)
			data_slip_sys.append(c.fetchone())
			c.execute('SELECT * FROM Matrix_1by3 WHERE MTX_1by3_ID= %d' %record.CenterPos)
			data_centerpos.append(c.fetchone())
		#------- Defect Node ---------#	
		data_defect_node=[]
		for record in data_defect:
			c.execute('SELECT * FROM Defect_Node WHERE Def_ID = %d' %record.Def_ID)
			data_defect_node.append(c.fetchall())		
		#print data_defect_node
		conn.close()
		
		#!======================= Create json data ========================!#
		d= {}
		d["status"] = status
		#-------- Volume Info ---------#
		cols = data_volume_info._fields; vals = list(data_volume_info) 
		for col, val in zip(cols,vals):
			d[col] = val
		
		#-------- Simulation Control ---------#
		cols = data_simulation_control._fields; vals = list(data_simulation_control) 
		for col, val in zip(cols,vals):
			d[col] = val
		
		#-------- Numerical Procedure ---------#
		cols = data_numerical_procedure._fields; vals = list(data_numerical_procedure) 
		for col, val in zip(cols,vals):
			d[col] = val
		d["nbox_x"] = data_nbox.E_01; d["nbox_y"] = data_nbox.E_02; d["nbox_z"] = data_nbox.E_03
		d["nElmtFe_x"] = data_nElmtFe.E_01; d["nElmtFe_y"] = data_nElmtFe.E_02; d["nElmtFe_z"] = data_nElmtFe.E_03
		
		#-------- Simulation Options ---------#
		cols = data_simulation_options._fields; vals = list(data_simulation_options) 
		for col, val in zip(cols,vals):
			d[col] = val
		d["DispNodes_x"] = data_dispNodes.E_01; d["DispNodes_y"] = data_dispNodes.E_02; d["DispNodes_z"] = data_dispNodes.E_03
		
		#-------- Outputs ---------#
		cols = data_outputs._fields; vals = list(data_outputs) 
		for col, val in zip(cols,vals):
			d[col] = val
			
		#-------- Layer Property & Material & Slip---------#
		o1= []
		for i in range(len(data_layer_property)):
			d1= {}
			#layer property
			cols = data_layer_property[i]._fields; vals = list(data_layer_property[i])
			for col, val in zip(cols,vals):
				d1[col] = val
			#materila
			cols = data_material[i]._fields; vals = list(data_material[i])
			for col, val in zip(cols,vals):
				d1[col] = val
			#slip
			o2=[]
			for j in range(len(data_slip[i])):	
				d2={}
				cols = data_slip[i][j]._fields; vals=list(data_slip[i][j])
				for col, val in zip(cols,vals):
					d2[col] = val
				o2.append(d2)
			d1["Slips"] = o2
			
			mtx = data_elastic_stiffness_mtx[i]
			d1["E_01"] = mtx.E_01; d1["E_02"] = mtx.E_02; d1["E_03"] = mtx.E_03; d1["E_04"] = mtx.E_04; d1["E_05"] = mtx.E_05; d1["E_06"] = mtx.E_06;
			d1["E_07"] = mtx.E_07; d1["E_08"] = mtx.E_08; d1["E_09"] = mtx.E_09; d1["E_10"] = mtx.E_10; d1["E_11"] = mtx.E_11; d1["E_12"] = mtx.E_12;
			d1["E_13"] = mtx.E_13; d1["E_14"] = mtx.E_14; d1["E_15"] = mtx.E_15; d1["E_16"] = mtx.E_16; d1["E_17"] = mtx.E_17; d1["E_18"] = mtx.E_18;
			d1["E_19"] = mtx.E_19; d1["E_20"] = mtx.E_20; d1["E_21"] = mtx.E_21; d1["E_22"] = mtx.E_22; d1["E_23"] = mtx.E_23; d1["E_24"] = mtx.E_24;
			d1["E_25"] = mtx.E_25; d1["E_26"] = mtx.E_26; d1["E_27"] = mtx.E_27; d1["E_28"] = mtx.E_28; d1["E_29"] = mtx.E_29; d1["E_30"] = mtx.E_30;
			d1["E_31"] = mtx.E_31; d1["E_32"] = mtx.E_32; d1["E_33"] = mtx.E_33; d1["E_34"] = mtx.E_34; d1["E_35"] = mtx.E_35; d1["E_36"] = mtx.E_36;
			mtx = data_lattice_ratios[i]
			d1["Lattice_Ratios_x"] = mtx.E_01; d1["Lattice_Ratios_y"] = mtx.E_02; d1["Lattice_Ratios_z"] = mtx.E_03
			mtx = data_lattice_angles[i]
			d1["Lattice_Angles_x"] = mtx.E_01; d1["Lattice_Angles_y"] = mtx.E_02; d1["Lattice_Angles_z"] = mtx.E_03
			mtx = data_euler_angles[i]
			d1["Euler_Angles_x"] = mtx.E_01; d1["Euler_Angles_y"] = mtx.E_02; d1["Euler_Angles_z"] = mtx.E_03
			o1.append(d1)
		#print o1
		d["Layers"] = o1
		
		#-------- Defect & Slip System & Defect Node  ---------#
		o1 =[] 
		for i in range(len(data_defect)):
			d1 ={}
			cols = data_defect[i]._fields; vals = list(data_defect[i])
			for col, val in zip(cols,vals):
				d1[col] = val
			
			cols = data_slip_sys[i]._fields; vals = list(data_slip_sys[i])
			for col, val in zip(cols,vals):
				d1[col] = val
					
			o2=[]
			for j in range(len(data_defect_node[i])):
				d2={}
				cols = data_defect_node[i][j]._fields; vals=list(data_defect_node[i][j])
				for col, val in zip(cols,vals):
					d2[col] = val
				o2.append(d2)
			d1["Nodes"] = o2

 			mtx = data_centerpos[i]
 			d1["CenterPos_01"] = mtx.E_01; d1["CenterPos_02"] = mtx.E_02; d1["CenterPos_03"] = mtx.E_03 			
			o1.append(d1)
			
		d["Defects"] = o1	
	
		conn.close()
		json_data = json.dumps(d)
		print json_data
		print "##########################################################################\n\n\n"
		return json_data	
	
	def getAllReports(self):
		print "Operation:----------------Display Reports--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		# Only display the completed simulations' reports
		c.execute("SELECT Report_Name,Experiment_Name, FilePath, LastModified, Created FROM Reports INNER JOIN\
		(SELECT Experiment_Name AS name FROM Experiments WHERE Status = 'completed') AS EXP ON Experiment_Name = EXP.name")
		# create json data
		data= []
		cols = [x[0] for x in c.description] 
		for row in c.fetchall():
			d = {}
			for col, val in zip(cols, row):
				d[col] = val
			data.append(d)
		
		conn.close()
		json_data = json.dumps(data)
		print json_data
		print "##########################################################################\n\n\n"
		return json_data
		
	def getAllSimulations(self):
		print "Operation:----------------Display Experiments--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("SELECT * FROM Experiments ORDER BY Created ASC")
		# create json data
		data= []
		cols = [x[0] for x in c.description] 
		for row in c.fetchall():
			d = {}
			for col, val in zip(cols, row):
				d[col] = val
			data.append(d)
		
		conn.close()
		json_data = json.dumps(data)
		print json_data
		print "##########################################################################\n\n\n"
		return json_data
		
	def saveConfiguration(self,name,json_data,mode):
		print "Operation:------------------Save Data with mode "+mode+"--------------------"
		
		# decode the data
		data = json.loads(base64.b64decode(json_data))		
			
		if mode == '0' or mode == '1' or mode =='2':
			self.updateNumOfLayers(name,data)
		if mode == '0' or mode == '2':
			self.updateNumOfDefects(name,data)
			
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
			
		c.execute("SELECT Config_ID FROM Experiments WHERE Experiment_Name='%s'" %name)
 		Config_ID = c.fetchone()[0]
 		c.execute("SELECT VoIn_ID, SimCo_ID, NuPro_ID, SimOp_ID, Out_ID FROM Configurations WHERE Config_ID = %s" %Config_ID)
 		data_id = c.fetchone()
 		VoIn_ID = data_id[0]; SimCo_ID = data_id[1]
 		NuPro_ID = data_id[2]; SimOp_ID = data_id[3]; 
 		Out_ID = data_id[4]; 
				
		#-------- Volume_& Layer Property (step1) ---------#
		if mode == '0' or mode =='1' or mode=='2':
			data_volume_info = (data["Volume_Size_x"],data["Volume_Size_y"],data["Num_Layer"],data["iInterface"])
 			c.execute("UPDATE Volume_Info SET Volume_Size_x =?, Volume_Size_y=?, Num_Layer=?, iInterface=?WHERE VoIn_ID=?", data_volume_info+(VoIn_ID,))
		 	
		 	c.execute("SELECT LaPro_ID, Euler_Angles FROM Layer_Property WHERE Config_ID = %s" %Config_ID)
			records = c.fetchall()
			
 			for i in range(len(records)): 
 				
			 	LaPro_ID = records[i][0];Euler_Angles =records[i][1]
			 	oldMat_ID = data["Layers"][i]["Mat_ID"]
				data_euler_angles = (data["Layers"][i]["Euler_Angles_x"],data["Layers"][i]["Euler_Angles_y"],data["Layers"][i]["Euler_Angles_z"])
				c.execute("UPDATE Matrix_1by3 SET E_01=?, E_02=?, E_03=? WHERE MTX_1by3_ID=?",data_euler_angles+(Euler_Angles,))
				
				c.execute("SELECT Mat_ID FROM Material WHERE Material_Type = '%s' AND Material_UserDefined = 0" %data["Layers"][i]["Material_Type"])			
				materials = c.fetchall()
				
				if(len(materials)==0): #-----Material is user defined
					
					if oldMat_ID != "":
						c.execute("SELECT * FROM Material WHERE Mat_ID = %s AND Material_UserDefined = 1" %oldMat_ID)
					
					if(len(c.fetchall())==0 or oldMat_ID == ""): #-------Create New Material
						
						tempData = data["Layers"][i]
						
						data_lattice_ratios = (tempData["Lattice_Ratios_x"],tempData["Lattice_Ratios_y"],tempData["Lattice_Ratios_z"])
						c.execute("INSERT INTO Matrix_1by3 (E_01,E_02,E_03) VALUES(?,?,?)",(data_lattice_ratios))
						c.execute("SELECT last_insert_rowid()")
						Lattice_Ratios = c.fetchone()[0]
						
						data_lattice_angles = (tempData["Lattice_Angles_x"],tempData["Lattice_Angles_y"],tempData["Lattice_Angles_z"])
						c.execute("INSERT INTO Matrix_1by3 (E_01,E_02,E_03) VALUES(?,?,?)",(data_lattice_angles))
						c.execute("SELECT last_insert_rowid()")
						Lattice_Angles = c.fetchone()[0]
				 		
						data_elst_mtx = (tempData["E_01"],tempData["E_02"],tempData["E_03"],tempData["E_04"],tempData["E_05"],tempData["E_06"],
				 							tempData["E_07"],tempData["E_08"],tempData["E_09"],tempData["E_10"],tempData["E_11"],tempData["E_12"],
				 							tempData["E_13"],tempData["E_14"],tempData["E_15"],tempData["E_16"],tempData["E_17"],tempData["E_18"],
				 							tempData["E_19"],tempData["E_20"],tempData["E_21"],tempData["E_22"],tempData["E_23"],tempData["E_24"],
				 							tempData["E_25"],tempData["E_26"],tempData["E_27"],tempData["E_28"],tempData["E_29"],tempData["E_30"],
				 							tempData["E_31"],tempData["E_32"],tempData["E_33"],tempData["E_34"],tempData["E_35"],tempData["E_36"])		
						c.execute("INSERT INTO Elastic_Stiffness_Matrix (E_01,E_02,E_03,E_04,E_05,E_06,E_07,E_08,E_09,E_10,E_11,E_12,\
									E_13,E_14,E_15,E_16,E_17,E_18,E_19,E_20,E_21,E_22,E_23,E_24,\
									E_25,E_26,E_27,E_28,E_29,E_30,E_31,E_32,E_33,E_34,E_35,E_36)\
									VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(data_elst_mtx))
						c.execute("SELECT last_insert_rowid()")
						ElSt_MTX_ID = c.fetchone()[0];
						
						data_material=(tempData["Material_Type"],tempData["Mu"],tempData["Nu"],ElSt_MTX_ID,tempData["Crystal_Type"],tempData["Lattice_Param"],Lattice_Ratios,Lattice_Angles,
										tempData["nSlip"],tempData["Mobilityns"],tempData["Mobilitys"],tempData["Anisotropy"],tempData["Material_UserDefined"])	
						c.execute("INSERT INTO Material (Material_Type,Mu,Nu,ElSt_MTX_ID,Crystal_Type,Lattice_Param,Lattice_Ratios,Lattice_Angles,nSlip,\
									Mobilityns,Mobilitys,Anisotropy,Material_UserDefined) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",(data_material))
						c.execute("SELECT last_insert_rowid()")
				 		newMat_ID = c.fetchone()[0]
				 			
				 		for j in range(len(tempData["Slips"])):
				 			data_slip =(newMat_ID, tempData["Slips"][j]["mode"],tempData["Slips"][j]["Slip_Name"],tempData["Slips"][j]["Slip_x"],tempData["Slips"][j]["Slip_y"],
										tempData["Slips"][j]["Slip_z"],tempData["Slips"][j]["magBurger"])
				 			c.execute("INSERT INTO Slip (Mat_ID,mode,Slip_Name,Slip_x,Slip_y,Slip_z,magBurger) VALUES (?,?,?,?,?,?,?)",(data_slip))
						
						
						c.execute("UPDATE Layer_Property SET Mat_ID=?,Thickness=? WHERE LaPro_ID=?",(newMat_ID,data["Layers"][i]["Thickness"],LaPro_ID))
					
					else: #-------Material ID already exists (old material)
						
						tempData = data["Layers"][i]
						
						c.execute("SELECT Slip_ID FROM Slip WHERE Mat_ID=%s" %oldMat_ID)
						data_slips_id = c.fetchall()
						curNum = len(data_slips_id)
						tarNum = len(tempData["Slips"])
						
						while (tarNum > curNum):
				
							c.execute("INSERT INTO Slip (Mat_ID) VALUES (%s)" %oldMat_ID)
							curNum = curNum + 1
						
						while (tarNum < curNum):
							curNum = curNum - 1		
							c.execute("DELETE FROM Slip WHERE Slip_ID =%s" %data_slips_id[curNum][0])
						
						c.execute("SELECT ElSt_MTX_ID, Lattice_Ratios, Lattice_Angles FROM Material WHERE Mat_ID= %s" %oldMat_ID)
						data_id = c.fetchone();
						ElSt_MTX_ID = data_id[0]; Lattice_Ratios = data_id[1]; Lattice_Angles = data_id[2]
						
						data_lattice_ratios = (tempData["Lattice_Ratios_x"],tempData["Lattice_Ratios_y"],tempData["Lattice_Ratios_z"])
						data_lattice_angles = (tempData["Lattice_Angles_x"],tempData["Lattice_Angles_y"],tempData["Lattice_Angles_z"])
						data_elst_mtx = (tempData["E_01"],tempData["E_02"],tempData["E_03"],tempData["E_04"],tempData["E_05"],tempData["E_06"],
				 							tempData["E_07"],tempData["E_08"],tempData["E_09"],tempData["E_10"],tempData["E_11"],tempData["E_12"],
				 							tempData["E_13"],tempData["E_14"],tempData["E_15"],tempData["E_16"],tempData["E_17"],tempData["E_18"],
				 							tempData["E_19"],tempData["E_20"],tempData["E_21"],tempData["E_22"],tempData["E_23"],tempData["E_24"],
				 							tempData["E_25"],tempData["E_26"],tempData["E_27"],tempData["E_28"],tempData["E_29"],tempData["E_30"],
				 							tempData["E_31"],tempData["E_32"],tempData["E_33"],tempData["E_34"],tempData["E_35"],tempData["E_36"])				
						c.execute("UPDATE Matrix_1by3 SET E_01=?,E_02=?,E_03=? WHERE MTX_1by3_ID=?",data_lattice_ratios+(Lattice_Ratios,))
						c.execute("UPDATE Matrix_1by3 SET E_01=?,E_02=?,E_03=? WHERE MTX_1by3_ID=?",data_lattice_angles+(Lattice_Angles,))
						c.execute("UPDATE Elastic_Stiffness_Matrix SET E_01=?,E_02=?,E_03=?,E_04=?,E_05=?,E_06=?,E_07=?,E_08=?,E_09=?,E_10=?,E_11=?,E_12=?,\
									E_13=?,E_14=?,E_15=?,E_16=?,E_17=?,E_18=?,E_19=?,E_20=?,E_21=?,E_22=?,E_23=?,E_24=?,\
									E_25=?,E_26=?,E_27=?,E_28=?,E_29=?,E_30=?,E_31=?,E_32=?,E_33=?,E_34=?,E_35=?,E_36=? WHERE ElSt_MTX_ID=? ",data_elst_mtx+(ElSt_MTX_ID,))
							
						data_material=(tempData["Material_Type"],tempData["Mu"],tempData["Nu"],ElSt_MTX_ID,tempData["Crystal_Type"],tempData["Lattice_Param"],Lattice_Ratios,Lattice_Angles,
										tempData["nSlip"],tempData["Mobilityns"],tempData["Mobilitys"],tempData["Anisotropy"])	
						c.execute("UPDATE Material SET Material_Type=?,Mu=?,Nu=?,ElSt_MTX_ID=?,Crystal_Type=?,Lattice_Param=?,Lattice_Ratios=?,Lattice_Angles=?,nSlip=?,\
									Mobilityns=?,Mobilitys=?,Anisotropy=? WHERE Mat_ID=?",data_material+(oldMat_ID,))
						c.execute("SELECT Slip_ID FROM Slip WHERE Mat_ID = %s" %oldMat_ID)
				 		record_slips = c.fetchall()
						
				 		for j in range(len(record_slips)):
				 			Slip_ID = record_slips[j][0]
				 			data_slip =(tempData["Slips"][j]["mode"],tempData["Slips"][j]["Slip_Name"],tempData["Slips"][j]["Slip_x"],tempData["Slips"][j]["Slip_y"],
										tempData["Slips"][j]["Slip_z"],tempData["Slips"][j]["magBurger"])
				 			c.execute("UPDATE Slip SET mode=?,Slip_Name=?,Slip_x=?,Slip_y=?,Slip_z=?,magBurger=? WHERE Slip_ID=?",data_slip+(Slip_ID,))
						
						c.execute("UPDATE Layer_Property SET Mat_ID=?,Thickness=? WHERE LaPro_ID=?",(oldMat_ID,data["Layers"][i]["Thickness"],LaPro_ID))
				
				else: #-----Material is from database
					newMat_ID = materials[0][0]
					c.execute("UPDATE Layer_Property SET Mat_ID=?,Thickness=? WHERE LaPro_ID=?",(newMat_ID,data["Layers"][i]["Thickness"],LaPro_ID))
		
		
		#-------- Defects & Defect Nodes(step2) ---------#
		if mode == '0' or mode =='2':
			c.execute("SELECT Def_ID, CenterPos FROM Defect WHERE Config_ID = %s" %Config_ID)
 			records = c.fetchall()
 		 	for i in range(len(records)): 
			 	Def_ID = records[i][0]; CenterPos = records[i][1]
			 	data_centerpos = (data["Defects"][i]["CenterPos_01"],data["Defects"][i]["CenterPos_02"],data["Defects"][i]["CenterPos_03"])
	
				c.execute("SELECT Slsys_ID FROM Slip_System WHERE System_Name = '%s'" %data["Defects"][i]["System_Name"])		
				newSlsys_ID = c.fetchone()[0]
				data_defect = (newSlsys_ID, data["Defects"][i]["LayerPos"],data["Defects"][i]["Form_Type"],data["Defects"][i]["nNode"],
 						data["Defects"][i]["LoopRad"],data["Defects"][i]["CenterLoop_01"],data["Defects"][i]["CenterLoop_02"])
				c.execute("UPDATE Matrix_1by3 SET E_01=?, E_02=?, E_03=? WHERE MTX_1by3_ID=?",data_centerpos+(CenterPos,))	
				c.execute("UPDATE Defect SET Slsys_ID=?, LayerPos=?, Form_Type=?, nNode=?, LoopRad=?, CenterLoop_01=?, CenterLoop_02=? WHERE Def_ID=?",data_defect+(Def_ID,))
							
 				c.execute("SELECT DefNo_ID FROM Defect_Node WHERE Def_ID = %s" %Def_ID)
 			 	records1 =  c.fetchall()
 			  	for j in range(len(records1)):
 			  		DefNo_ID = records1[j][0]
 				 	data_defect_node = (data["Defects"][i]["Nodes"][j]["Coordinate_01"],data["Defects"][i]["Nodes"][j]["Coordinate_02"])
 				 	c.execute("UPDATE Defect_Node SET Coordinate_01=?, Coordinate_02=? WHERE DefNo_ID=?", data_defect_node+(DefNo_ID,))
			
			
		#-------- Simulation Control (step3)---------#
		if mode == '0' or mode =='3':		
			oldSimCo_ID = data["SimCo_ID"]
			data_numerical_procedure = (data["ILoop_Time"],data["DTIME_MAX"],data["DTIME_MIN"])
 	 		c.execute("UPDATE Numerical_Procedure SET ILoop_Time=?, DTIME_MAX=?, DTIME_MIN=? WHERE NuPro_ID=?", data_numerical_procedure+(NuPro_ID,))
			
			c.execute("SELECT SimCo_ID FROM Simulation_Control WHERE Control_Name = '%s' AND Control_UserDefined = 0" %data["Control_Name"])		
	 		controls = c.fetchall()
	 		if(len(controls)==0): #-----Control is user defined
	 			if oldSimCo_ID != "":
						c.execute("SELECT * FROM Simulation_Control WHERE SimCo_ID = %s AND Control_UserDefined = 1" %oldSimCo_ID)
					
				if(len(c.fetchall())==0 or oldSimCo_ID == ""): #-------Create New Material
		 			data_control = (data["Control_Name"],data["Load"],data["iRelaxtion"],data["iRelaxStep"],
					data["AF_01"],data["AF_02"],data["AF_03"],data["AF_04"],data["AF_05"],data["AF_06"],data["AF_07"],data["AF_08"],data["AF_09"],
					data["IAS_01"],data["IAS_02"],data["IAS_03"],data["IAS_04"],data["IAS_05"],data["IAS_06"],data["IAS_07"],data["IAS_08"],data["IAS_09"],data["Control_UserDefined"])
				 	c.execute("INSERT INTO Simulation_Control (Control_Name,Load,iRelaxtion,iRelaxStep,\
						AF_01,AF_02,AF_03,AF_04,AF_05,AF_06,AF_07,AF_08,AF_09,\
						IAS_01,IAS_02,IAS_03,IAS_04,IAS_05,IAS_06,IAS_07,IAS_08,IAS_09,Control_UserDefined)\
						VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(data_control))
				 	c.execute("SELECT last_insert_rowid()")
					newSimCo_ID = c.fetchone()[0]
					c.execute("UPDATE Configurations SET SimCo_ID=? WHERE Config_ID =? ",(newSimCo_ID ,Config_ID))
					
				else: #-------SimCo ID already exists (old control)
					data_ctrl = (data["Control_Name"],data["Load"],data["iRelaxtion"],data["iRelaxStep"],data["AF_01"],data["AF_02"],data["AF_03"],
								data["AF_04"],data["AF_05"],data["AF_06"],data["AF_07"],data["AF_08"],data["AF_09"],data["IAS_01"],data["IAS_02"],data["IAS_03"],
								data["IAS_04"],data["IAS_05"],data["IAS_06"],data["IAS_07"],data["IAS_08"],data["IAS_09"])
					c.execute("UPDATE Simulation_Control SET Control_Name=?,Load=?,iRelaxtion=?,iRelaxStep=?,AF_01=?,AF_02=?,AF_03=?,\
							 AF_04=?,AF_05=?,AF_06=?,AF_07=?,AF_08=?,AF_09=?,IAS_01=?,IAS_02=?,IAS_03=?,IAS_04=?,IAS_05=?,IAS_06=?,\
							 IAS_07=?,IAS_08=?,IAS_09=? WHERE SimCo_ID=?",data_ctrl+(oldSimCo_ID,))
				
	 		else: #-----Control is from database
	 			newSimCo_ID = controls[0][0]
 	 			c.execute("UPDATE Configurations SET SimCo_ID=? WHERE Config_ID =? ",(newSimCo_ID ,Config_ID))
 	 		
 	 		
 	 	
 	 	#-------- Numerical Procedure (step4)---------#
 	 	if mode == '0' or mode =='4':	
 	 	 	c.execute("SELECT nbox, nElmtFe FROM Numerical_Procedure WHERE NuPro_ID =%s" %NuPro_ID)
 		 	data_id = c.fetchone()
 		 	nbox = data_id[0]; nElmtFe = data_id[1]
 		 	data_nbox = (data["nbox_x"],data["nbox_y"],data["nbox_z"])
 		 	data_nElmtFe = (data["nElmtFe_x"],data["nElmtFe_y"],data["nElmtFe_z"])
 		 	data_numerical_procedure = (data["MAX_QUAD"],data["NPOINT_I"], data["MAX_node"],data["MAX_Neighbor"],
										data["dpMaxAveLength"],	 data["reebox"],data["dcrit"],data["iArr"],data["iFEM"],)
 		 	c.execute("UPDATE Matrix_1by3 SET E_01=?, E_02=?, E_03=? WHERE MTX_1by3_ID=?",data_nbox+(nbox,))
 		 	c.execute("UPDATE Matrix_1by3 SET E_01=?, E_02=?, E_03=? WHERE MTX_1by3_ID=?",data_nElmtFe+(nElmtFe,))
 		 	c.execute("UPDATE Numerical_Procedure SET MAX_QUAD=?, NPOINT_I=?, MAX_node=?, MAX_Neighbor=?, \
 				 	dpMaxAveLength=?, reebox=?, dcrit=?, iArr=?, iFEM=? WHERE NuPro_ID=?", data_numerical_procedure+(NuPro_ID,))
 		
 		#-------- Simulation Options & Output (step5) ---------#
 	 	if mode == '0' or mode =='5':	
 	 		c.execute("SELECT DispNodes FROM Simulation_Options WHERE SimOp_ID =%s" %SimOp_ID)
 		 	DispNodes = c.fetchone()[0]
 		 	data_dispnodes = (data["DispNodes_x"],data["DispNodes_y"],data["DispNodes_z"])
 		 	data_simulation_options = (data["CheckNeiBur"],data["LogInteraction"],data["iCrossSlip"],data["FullyPeriodic"],data["iInertial"],
 							data["ioutFe"],data["nBoxFe"],data["iSplineFe"],data["iXRD"])
 		 	c.execute("UPDATE Matrix_1by3 SET E_01=?, E_02=?, E_03=? WHERE MTX_1by3_ID=?",data_dispnodes+(DispNodes,))
 		 	c.execute("UPDATE Simulation_Options SET CheckNeiBur=?, LogInteraction=?, iCrossSlip=?,  FullyPeriodic=?, iInertial=?, ioutFe=?, \
 				 	 nBoxFe=?, iSplineFe=?, iXRD=? WHERE SimOp_ID=?",data_simulation_options+(SimOp_ID,))	 	
 		 	data_outputs = (data["iOutFreq"],data["iPastFreq"],data["iDebugMode"])
 		 	c.execute("UPDATE Outputs SET iOutFreq=?, iPastFreq=?, iDebugMode=? WHERE Out_ID=?",data_outputs+(Out_ID,))


		conn.commit()
		conn.close()
		print "Data updated for Simulation %s" %name 
		print "##########################################################################\n\n\n"
		
	def saveSlipSys(self,json_data):
		print "Operation:----------------Update Slip System--------------------"
		
		data = json.loads(json_data)
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		
		Slsys_ID = data["Slsys_ID"]
		data_slipsys=(data["System_Name"],data["mode"],data["M_01"],data["M_02"],data["M_03"],data["M_04"],data["B_01"],data["B_02"],data["B_03"],data["B_04"])
		
		c.execute("UPDATE Slip_System SET System_Name=?,mode=?,M_01=?,M_02=?,M_03=?,M_04=?,B_01=?,B_02=?,B_03=?,B_04=? \
		WHERE SlSys_ID=?",data_slipsys+(Slsys_ID,))
		
		
		conn.commit()
		conn.close()
		print "Slip System %s updated" %data["System_Name"]
		print "##########################################################################\n\n\n"
		return "success"
	
	def saveMaterial(self,json_data):
		print "Operation:----------------Update Material -------------------"
		
		data = json.loads(json_data)
		Mat_ID = data["Mat_ID"]
		self.updateNumOfSlips(Mat_ID,data)
		
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		
		c.execute("SELECT ElSt_MTX_ID, Lattice_Ratios, Lattice_Angles FROM Material WHERE Mat_ID= %s" %Mat_ID)
		data_id = c.fetchone();
		ElSt_MTX_ID = data_id[0]; Lattice_Ratios = data_id[1]; Lattice_Angles = data_id[2]
		
		data_lattice_ratios = (data["Lattice_Ratios_x"],data["Lattice_Ratios_y"],data["Lattice_Ratios_z"])
		data_lattice_angles = (data["Lattice_Angles_x"],data["Lattice_Angles_y"],data["Lattice_Angles_z"])
		data_elst_mtx = (data["E_01"],data["E_02"],data["E_03"],data["E_04"],data["E_05"],data["E_06"],
 							data["E_07"],data["E_08"],data["E_09"],data["E_10"],data["E_11"],data["E_12"],
 							data["E_13"],data["E_14"],data["E_15"],data["E_16"],data["E_17"],data["E_18"],
 							data["E_19"],data["E_20"],data["E_21"],data["E_22"],data["E_23"],data["E_24"],
 							data["E_25"],data["E_26"],data["E_27"],data["E_28"],data["E_29"],data["E_30"],
 							data["E_31"],data["E_32"],data["E_33"],data["E_34"],data["E_35"],data["E_36"])				
		c.execute("UPDATE Matrix_1by3 SET E_01=?,E_02=?,E_03=? WHERE MTX_1by3_ID=?",data_lattice_ratios+(Lattice_Ratios,))
		c.execute("UPDATE Matrix_1by3 SET E_01=?,E_02=?,E_03=? WHERE MTX_1by3_ID=?",data_lattice_angles+(Lattice_Angles,))
		c.execute("UPDATE Elastic_Stiffness_Matrix SET E_01=?,E_02=?,E_03=?,E_04=?,E_05=?,E_06=?,E_07=?,E_08=?,E_09=?,E_10=?,E_11=?,E_12=?,\
					E_13=?,E_14=?,E_15=?,E_16=?,E_17=?,E_18=?,E_19=?,E_20=?,E_21=?,E_22=?,E_23=?,E_24=?,\
					E_25=?,E_26=?,E_27=?,E_28=?,E_29=?,E_30=?,E_31=?,E_32=?,E_33=?,E_34=?,E_35=?,E_36=? WHERE ElSt_MTX_ID=? ",data_elst_mtx+(ElSt_MTX_ID,))
			
		data_material=(data["Material_Type"],data["Mu"],data["Nu"],ElSt_MTX_ID,data["Crystal_Type"],data["Lattice_Param"],Lattice_Ratios,Lattice_Angles,
						data["nSlip"],data["Mobilityns"],data["Mobilitys"],data["Anisotropy"])	
		c.execute("UPDATE Material SET Material_Type=?,Mu=?,Nu=?,ElSt_MTX_ID=?,Crystal_Type=?,Lattice_Param=?,Lattice_Ratios=?,Lattice_Angles=?,nSlip=?,\
					Mobilityns=?,Mobilitys=?,Anisotropy=? WHERE Mat_ID=?",data_material+(Mat_ID,))
		
		
		c.execute("SELECT Slip_ID FROM Slip WHERE Mat_ID = %s" %Mat_ID)
 		records = c.fetchall()
		
 		for i in range(len(records)):
 			Slip_ID = records[i][0]
 			data_slip =(data["Slips"][i]["mode"],data["Slips"][i]["Slip_Name"],data["Slips"][i]["Slip_x"],data["Slips"][i]["Slip_y"]
						,data["Slips"][i]["Slip_z"],data["Slips"][i]["magBurger"])
 			c.execute("UPDATE Slip SET mode=?,Slip_Name=?,Slip_x=?,Slip_y=?,Slip_z=?,magBurger=? WHERE Slip_ID=?",data_slip+(Slip_ID,))
		
		conn.commit()
		conn.close()
		print "Material %s updated" %data["Material_Type"]
		print "##########################################################################\n\n\n"
		return "success"
	
	def saveControl(self,json_data):
		print "Operation:----------------Update Simulation Control--------------------"

		data = json.loads(json_data)
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		
		SimCo_ID = data["SimCo_ID"]
		data_ctrl = (data["Control_Name"],data["Load"],data["iRelaxtion"],data["iRelaxStep"],data["AF_01"],data["AF_02"],data["AF_03"],
					data["AF_04"],data["AF_05"],data["AF_06"],data["AF_07"],data["AF_08"],data["AF_09"],data["IAS_01"],data["IAS_02"],data["IAS_03"],
					data["IAS_04"],data["IAS_05"],data["IAS_06"],data["IAS_07"],data["IAS_08"],data["IAS_09"])
		c.execute("UPDATE Simulation_Control SET Control_Name=?,Load=?,iRelaxtion=?,iRelaxStep=?,AF_01=?,AF_02=?,AF_03=?,\
				 AF_04=?,AF_05=?,AF_06=?,AF_07=?,AF_08=?,AF_09=?,IAS_01=?,IAS_02=?,IAS_03=?,IAS_04=?,IAS_05=?,IAS_06=?,\
				 IAS_07=?,IAS_08=?,IAS_09=? WHERE SimCo_ID=?",data_ctrl+(SimCo_ID,))
		
		conn.commit()
		conn.close()
		print "Control %s updated" %data["Control_Name"]
		print "##########################################################################\n\n\n"
		return "success"
	
	def createSlipSys(self,json_data):
		print "Operation:----------------Create Crystal--------------------"
		##-----------check if name is used?
		data = json.loads(json_data)
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		
		data_slipsys=(data["Crystal"],data["System_Name"],data["Structure"],data["mode"],data["nInd"],
					data["M_01"],data["M_02"],data["M_03"],data["M_04"],data["B_01"],data["B_02"],data["B_03"],data["B_04"])
		c.execute("INSERT INTO Slip_System (Crystal,System_Name,Structure,mode,nInd,M_01,M_02,M_03,M_04,B_01,B_02,B_03,B_04) \
		VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",(data_slipsys))
		
		conn.commit()
		conn.close()
		print "New Slip System %s created" %data["System_Name"]
		print "##########################################################################\n\n\n"
		return 
	
	def createCrystal(self,json_data):
		print "Operation:----------------Create Crystal--------------------"
		##-----------check if name is used?
		data = json.loads(json_data)
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		#Default values
		M_01=0; M_02=0; M_03=0; M_04=0;
		B_01=0; B_02=0; B_03=0; B_04=0;
		System_Name = "Default"; mode = 1
			
		data_crystal=(data["Crystal"],System_Name,data["Structure"],mode,data["nInd"],M_01,M_02,M_03,M_04,B_01,B_02,B_03,B_04)
		c.execute("INSERT INTO Slip_System (Crystal,System_Name,Structure,mode,nInd,M_01,M_02,M_03,M_04,B_01,B_02,B_03,B_04) \
		VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",(data_crystal))
		conn.commit()
		conn.close()
		print "New Crystal %s created" %data["Crystal"]
		print "##########################################################################\n\n\n"
		return 
		
	def createMaterial(self,json_data):
		print "Operation:----------------Create Material--------------------"
		##-----------check if name is used?
		data = json.loads(json_data)
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		
		data_lattice_ratios = (data["Lattice_Ratios_x"],data["Lattice_Ratios_y"],data["Lattice_Ratios_z"])
		c.execute("INSERT INTO Matrix_1by3 (E_01,E_02,E_03) VALUES(?,?,?)",(data_lattice_ratios))
		c.execute("SELECT last_insert_rowid()")
		Lattice_Ratios = c.fetchone()[0]
		
		data_lattice_angles = (data["Lattice_Angles_x"],data["Lattice_Angles_y"],data["Lattice_Angles_z"])
		c.execute("INSERT INTO Matrix_1by3 (E_01,E_02,E_03) VALUES(?,?,?)",(data_lattice_angles))
		c.execute("SELECT last_insert_rowid()")
		Lattice_Angles = c.fetchone()[0]
 		
		data_elst_mtx = (data["E_01"],data["E_02"],data["E_03"],data["E_04"],data["E_05"],data["E_06"],
 							data["E_07"],data["E_08"],data["E_09"],data["E_10"],data["E_11"],data["E_12"],
 							data["E_13"],data["E_14"],data["E_15"],data["E_16"],data["E_17"],data["E_18"],
 							data["E_19"],data["E_20"],data["E_21"],data["E_22"],data["E_23"],data["E_24"],
 							data["E_25"],data["E_26"],data["E_27"],data["E_28"],data["E_29"],data["E_30"],
 							data["E_31"],data["E_32"],data["E_33"],data["E_34"],data["E_35"],data["E_36"])		
		c.execute("INSERT INTO Elastic_Stiffness_Matrix (E_01,E_02,E_03,E_04,E_05,E_06,E_07,E_08,E_09,E_10,E_11,E_12,\
					E_13,E_14,E_15,E_16,E_17,E_18,E_19,E_20,E_21,E_22,E_23,E_24,\
					E_25,E_26,E_27,E_28,E_29,E_30,E_31,E_32,E_33,E_34,E_35,E_36)\
					VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(data_elst_mtx))
		c.execute("SELECT last_insert_rowid()")
		ElSt_MTX_ID = c.fetchone()[0];
		
		data_material=(data["Material_Type"],data["Mu"],data["Nu"],ElSt_MTX_ID,data["Crystal_Type"],data["Lattice_Param"],Lattice_Ratios,Lattice_Angles,
						data["nSlip"],data["Mobilityns"],data["Mobilitys"],data["Anisotropy"],data["Material_UserDefined"])	
		c.execute("INSERT INTO Material (Material_Type,Mu,Nu,ElSt_MTX_ID,Crystal_Type,Lattice_Param,Lattice_Ratios,Lattice_Angles,nSlip,\
					Mobilityns,Mobilitys,Anisotropy,Material_UserDefined) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",(data_material))
		c.execute("SELECT last_insert_rowid()")
 		Mat_ID = c.fetchone()[0]
 			
 		for i in range(len(data["Slips"])):
 			data_slip =(Mat_ID, data["Slips"][i]["mode"],data["Slips"][i]["Slip_Name"],data["Slips"][i]["Slip_x"],data["Slips"][i]["Slip_y"]
						,data["Slips"][i]["Slip_z"],data["Slips"][i]["magBurger"])
 			c.execute("INSERT INTO Slip (Mat_ID,mode,Slip_Name,Slip_x,Slip_y,Slip_z,magBurger) VALUES (?,?,?,?,?,?,?)",(data_slip))
		
		conn.commit()
		conn.close()
		print "New Material %s created" %data["Material_Type"]
		print "##########################################################################\n\n\n"
		return Mat_ID
		
	def createControl(self,json_data):
		print "Operation:----------------Create Control--------------------"
		##-----------check if name is used?
		data = json.loads(json_data)
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		
		data_control = (data["Control_Name"],data["Load"],data["iRelaxtion"],data["iRelaxStep"],
					data["AF_01"],data["AF_02"],data["AF_03"],data["AF_04"],data["AF_05"],data["AF_06"],data["AF_07"],data["AF_08"],data["AF_09"],
					data["IAS_01"],data["IAS_02"],data["IAS_03"],data["IAS_04"],data["IAS_05"],data["IAS_06"],data["IAS_07"],data["IAS_08"],data["IAS_09"],data["Control_UserDefined"])
		c.execute("INSERT INTO Simulation_Control (Control_Name,Load,iRelaxtion,iRelaxStep,\
					AF_01,AF_02,AF_03,AF_04,AF_05,AF_06,AF_07,AF_08,AF_09,\
					IAS_01,IAS_02,IAS_03,IAS_04,IAS_05,IAS_06,IAS_07,IAS_08,IAS_09,Control_UserDefined)\
					VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(data_control))
		c.execute("SELECT last_insert_rowid()")
		SimCo_ID = c.fetchone()[0]

		conn.commit()
		conn.close()
		print "New Control %s created" %data["Control_Name"]
		print "##########################################################################\n\n\n"
		return SimCo_ID
		
	def createSimulation(self,name):
		print "Operation:----------------Create Simulation--------------------"
		# check if the name has been used
		if self.isSimNameUsed(name):
			return "failure"
		
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		
		# Give default values for new simulation
		Num_Layer = 1; iInterface = 0; nNode = 2; LayerPos=1# default values
		MAX_QUAD =32;NPOINT_I=4;MAX_node=1000;MAX_Neighbor=2000;dpMaxAveLength=1000;
		reebox = 10;dcrit =20;iArr=1;iFEM=0;
		data_nbox = (3,3,3)
		data_nElmtFe =(1,1,1)
		CheckNeiBur=1;LogInteraction=1;iCrossSlip=0;FullyPeriodic=0;iInertial=0;ioutFe=0;nBoxFe=100
		iSplineFe=0;iXRD=0
		data_DispNodes =(3,3,3)
		iOutFreq=10;iPastFreq=100;iDebugMode=2
		LoopRad=0; CenterLoop_01=0;CenterLoop_02=0
		
		#------------ Volume information -----------#
		c.execute("INSERT INTO Volume_Info (Num_Layer,iInterface) VALUES (%s,%s)" %(Num_Layer,iInterface))
		c.execute("SELECT last_insert_rowid()")
		VoIn_ID = c.fetchone()[0]
		
		#------------ Simulation Control -----------#
		SimCo_ID = 0
		
		#------------ Numerical Procedure -----------#
		c.execute("INSERT INTO Matrix_1by3 (E_01,E_02,E_03) VALUES (?,?,?)",data_nbox)
		c.execute("SELECT last_insert_rowid()")
		nbox = c.fetchone()[0]
		c.execute("INSERT INTO Matrix_1by3 (E_01,E_02,E_03) VALUES (?,?,?)",data_nElmtFe)
		c.execute("SELECT last_insert_rowid()")
		nElmtFe = c.fetchone()[0]
		c.execute("INSERT INTO Numerical_Procedure (MAX_QUAD,NPOINT_I,MAX_node,MAX_Neighbor,dpMaxAveLength,nbox,\
		reebox,dcrit,iArr,iFEM, nElmtFe) VALUES (?,?,?,?,?,?,?,?,?,?,?)",(MAX_QUAD,NPOINT_I,MAX_node,MAX_Neighbor,dpMaxAveLength,nbox
		,reebox,dcrit,iArr,iFEM, nElmtFe))
		c.execute("SELECT last_insert_rowid()")
		NuPro_ID = c.fetchone()[0]
		
		#------------ Simulation Options -----------#
		c.execute("INSERT INTO Matrix_1by3 (E_01,E_02,E_03) VALUES (?,?,?)",data_DispNodes)
		c.execute("SELECT last_insert_rowid()")
		DispNodes = c.fetchone()[0]
		c.execute("INSERT INTO Simulation_Options (CheckNeiBur,LogInteraction,iCrossSlip,FullyPeriodic,iInertial,ioutFe,nBoxFe,\
		iSplineFe,DispNodes,iXRD) VALUES (?,?,?,?,?,?,?,?,?,?)",(CheckNeiBur,LogInteraction,iCrossSlip,FullyPeriodic,iInertial,ioutFe,nBoxFe,
		iSplineFe,DispNodes,iXRD))
		c.execute("SELECT last_insert_rowid()")
		SimOp_ID = c.fetchone()[0]
			
		#------------ Outputs -----------#
		c.execute("INSERT INTO Outputs (iOutFreq,iPastFreq,iDebugMode) VALUES (?,?,?)",(iOutFreq,iPastFreq,iDebugMode))
		c.execute("SELECT last_insert_rowid()")
		Out_ID= c.fetchone()[0]
		
		
		#------------ Configuration & Experiment -----------#	
		c.execute("INSERT INTO Configurations(VoIn_ID,SimCo_ID,NuPro_ID,SimOp_ID,Out_ID) VALUES (?,?,?,?,?)",(VoIn_ID,SimCo_ID,NuPro_ID,SimOp_ID,Out_ID))
		c.execute("SELECT last_insert_rowid()")
		Config_ID = c.fetchone()[0]
		
		c.execute("INSERT INTO Experiments (Experiment_Name,Config_ID) VALUES (?,?)",(name,Config_ID))
			
		#------------ Layer Property & Material-----------#
		# @note: one layer, one defect, two defect nodes (default setting)
		ElSt_MTX_ID=0;Lattice_Ratios=0;Lattice_Angles=0
		Mat_ID=0
		c.execute("INSERT INTO Matrix_1by3 (E_01) VALUES (NULL)")
		c.execute("SELECT last_insert_rowid()")
		Euler_Angles = c.fetchone()[0]
		c.execute("INSERT INTO Layer_Property (Config_ID,Mat_ID,Euler_Angles) VALUES (?,?,?)",(Config_ID,Mat_ID,Euler_Angles))
		
		#------------ Defect -----------#
		Slsys_ID = 0
		c.execute("INSERT INTO Matrix_1by3 (E_01) VALUES (NULL)")
		c.execute("SELECT last_insert_rowid()")
		CenterPos = c.fetchone()[0]
		c.execute("INSERT INTO Defect (Config_ID, Slsys_ID, LayerPos, nNode, LoopRad,CenterLoop_01,CenterLoop_02,CenterPos) VALUES (?,?,?,?,?,?,?,?)",
				(Config_ID, Slsys_ID, LayerPos, nNode,LoopRad,CenterLoop_01,CenterLoop_02,CenterPos))
		c.execute("SELECT last_insert_rowid()")
		Def_ID = c.fetchone()[0]	
		
		#------------ Defect Node -----------#
		c.execute("INSERT INTO Defect_Node (Def_ID) VALUES (%s)" %Def_ID)
		c.execute("INSERT INTO Defect_Node (Def_ID) VALUES (%s)" %Def_ID)
	
		conn.commit()
		conn.close()
		
		print "New Simulation %s is created" %name
		print "##########################################################################\n\n\n"
		return "success"
	
	def createReport(self,name,filepath):
		print "Operation:----------------Create Report--------------------" 
		#!============== Open database & Define row_factory ================!#
		conn = sqlite3.connect(PATH_DB + DataBase)
		conn.row_factory = DDD_util.namedtuple_factory
		c = conn.cursor()
		
		c.execute("SELECT Exp_ID FROM Experiments WHERE Experiment_Name='%s'" %name)
		data = c.fetchone()
		Exp_ID = data[0]
		report_name = name+"_r"
		c.execute("INSERT INTO Reports (Exp_ID,Report_Name,Experiment_Name,FilePath) VALUES (?,?,?,?)", (Exp_ID,report_name,name,filepath))
		c.execute("UPDATE Experiments Set FilePath = '%s' WHERE Experiment_Name='%s'" %(filepath,name))
		conn.commit()
		conn.close()
		print "New Report %s is created" %report_name
		print "##########################################################################\n\n\n"
		return "success"
		
	def deleteExperiment(self,name):
		print "Operation:----------------Delete Experiment--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("DELETE FROM Experiments WHERE Experiment_Name ='%s'" %name)
		conn.commit()
		conn.close()
		print "Experiment %s is deleted" %name
		print "##########################################################################\n\n\n"
		return "success"
	
	def deleteReport(self,name):
		print "Operation:----------------Delete Report--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		# remove the simulation folder 
		c.execute("SELECT FilePath FROM Reports WHERE Report_Name='%s'" %name)
		sim_folder = c.fetchone()[0]
		shutil.rmtree(sim_folder)
		# delete the record
		c.execute("DELETE FROM Reports WHERE Report_Name ='%s'" %name)
		conn.commit()
		conn.close()
		print "Report %s is deleted" %name
		print "##########################################################################\n\n\n"
		return "success"
	def deleteSlipSys(self,name,crystal_type):
		print "Operation:----------------Delete Slip System--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("DELETE FROM Slip_System WHERE Crystal ='%s' AND System_Name='%s'" %(crystal_type,name))
		conn.commit()
		conn.close()
		print "Slip System %s is deleted" %name
		print "##########################################################################\n\n\n"
		return "success"
	
	def deleteCrystal(self,name):
		print "Operation:----------------Delete Crystal--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("DELETE FROM Slip_System WHERE Crystal ='%s'" %name)
		conn.commit()
		conn.close()
		print "Crystal %s is deleted" %name
		print "##########################################################################\n\n\n"
		return "success"
	
	def deleteMaterial(self,name):
		print "Operation:----------------Delete Material--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("DELETE FROM Material WHERE Material_Type ='%s'" %name)
		conn.commit()
		conn.close()
		print "Material %s is deleted" %name
		print "##########################################################################\n\n\n"
		return "success"
	
	def deleteControl(self,name):
		print "Operation:----------------Delete Simulation Control--------------------"
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("DELETE FROM Simulation_Control WHERE Control_Name ='%s'" %name)
		conn.commit()
		conn.close()
		print "Control %s is deleted" %name
		print "##########################################################################\n\n\n"
		return "success"
	
	def exportConfiguration(self,name):
		print "Operation:----------------Export Configuration--------------------" 
				
		fileDir = PATH_DOWNLOAD; file1 ='material_input.txt' ; file2 = 'interaction_geom_input.txt';
		cwDir = os.getcwd();

		# create two input files: 1.material_input.txt, 2.interaction_geom_input.txt
		os.chdir(fileDir)
		if os.path.isfile(file1):
			os.remove(file1)
		if os.path.isfile(file2):
			os.remove(file2)		
		DDD_util.writeInputFile(name)
		os.chdir(cwDir)
		
		d={}
		d["input_material"] = fileDir + file1
		d["input_interaction"] = fileDir + file2
		print fileDir
		
		json_data = json.dumps(d)
		print json_data
		print "##########################################################################\n\n\n"
		return json_data
		
	def resetDatabase(self):
		print "Operation:----------------Reset Database--------------------" 
		cwDir = os.getcwd();
		os.remove(PATH_DB + DataBase)
		shutil.copy2(PATH_DefaultDB+"default.db",PATH_DB)
		os.rename(PATH_DB+"default.db", PATH_DB+DataBase)
		
		print "##########################################################################\n\n\n"
		return "success"
	
	def updateNumOfSlips (self,id, data):
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		
		#-------  Add/Remove Slip ----------#
		c.execute("SELECT Slip_ID FROM Slip WHERE Mat_ID=%s" %id)
		data_slips_id = c.fetchall()
		curNum = len(data_slips_id)
		tarNum = len(data["Slips"])
		
		while (tarNum > curNum):

			c.execute("INSERT INTO Slip (Mat_ID) VALUES (%s)" %id)
			curNum = curNum + 1
		
		while (tarNum < curNum):
			curNum = curNum - 1		
			c.execute("DELETE FROM Slip WHERE Slip_ID =%s" %data_slips_id[curNum][0])
		
		conn.commit()	
		conn.close()
		return
	
	def updateNumOfLayers (self,name,data):
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()

		#-------  Add/Remove Layer Property ----------#
		c.execute("SELECT Config_ID FROM Experiments WHERE Experiment_Name='%s'" %name)
		Config_ID = c.fetchone()[0]
		c.execute("SELECT LaPro_ID FROM Layer_Property WHERE Config_ID=%s" %Config_ID)
		data_layers_id = c.fetchall()
		curNum = len(data_layers_id)
		tarNum = len(data["Layers"])

		while (tarNum > curNum):
			Mat_ID=0
			c.execute("INSERT INTO Matrix_1by3 (E_01) VALUES (NULL)")
			c.execute("SELECT last_insert_rowid()")
			Euler_Angles = c.fetchone()[0]
			c.execute("INSERT INTO Layer_Property (Config_ID,Mat_ID,Euler_Angles) VALUES (?,?,?)",(Config_ID,Mat_ID,Euler_Angles))
			curNum = curNum + 1
		
		while (tarNum < curNum):
			curNum = curNum - 1		
			c.execute("DELETE FROM Layer_Property WHERE LaPro_ID =%s" %data_layers_id[curNum][0])
		
		conn.commit()	
		conn.close()
		return 
	
	def updateNumOfDefects(self,name,data):
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
	
		#-------  Add/Remove Defect----------#
		c.execute("SELECT Config_ID FROM Experiments WHERE Experiment_Name='%s'" %name)
		Config_ID = c.fetchone()[0]
		c.execute("SELECT Def_ID FROM Defect WHERE Config_ID=%s" %Config_ID)
		data_defects_id = c.fetchall()
		curNum = len(data_defects_id)
		tarNum = len(data["Defects"])
		
		while (tarNum > curNum):
			Slsys_ID = 0; nNode=2
			c.execute("INSERT INTO Matrix_1by3 (E_01) VALUES (NULL)")
			c.execute("SELECT last_insert_rowid()")
			CenterPos = c.fetchone()[0]
			c.execute("INSERT INTO Defect (Config_ID, Slsys_ID, nNode, CenterPos) VALUES (?,?,?,?)",(Config_ID, Slsys_ID, nNode,CenterPos))
			curNum = curNum + 1
			
		while (tarNum < curNum):
			curNum = curNum - 1
			c.execute("DELETE FROM Defect WHERE Def_ID =%s" %data_defects_id[curNum][0])
			
		
		#-------  Add/Remove Defect Node----------#
		c.execute("SELECT Def_ID FROM Defect WHERE Config_ID=%s" %Config_ID)
		data_defects_id = c.fetchall()
		for i in range(curNum):
			c.execute("SELECT DefNo_ID FROM Defect_Node WHERE Def_ID=%s" %data_defects_id[i][0])
			data_nodes_id = c.fetchall()
			curNum1 = len(data_nodes_id)
		 	tarNum1 = len(data["Defects"][i]["Nodes"])

		 	while (tarNum1 > curNum1):
		 		c.execute("INSERT INTO Defect_Node (Def_ID) VALUES (%s)" %data_defects_id[i][0])
		 		curNum1 = curNum1 + 1
		 	
		 	while (tarNum1 < curNum1):
		 		curNum1 = curNum1 - 1
				c.execute("DELETE FROM Defect_Node WHERE DefNo_ID =%s" %data_nodes_id[curNum1][0])
			
		conn.commit()	
		conn.close()
		return 
	
	def isSimNameUsed(self,name):
		conn = sqlite3.connect(PATH_DB + DataBase)
		c = conn.cursor()
		c.execute("SELECT * FROM Experiments WHERE Experiment_Name= '%s'" %name)
		isUsed = False	
		if (len(c.fetchall()) > 0):
			isUsed = True
				
		conn.close()	
		return isUsed

def runServer():
	Handler = MyRequestHandler
	while True:
		try:
			print("Server listening on port 8080...")
			server = SocketServer.TCPServer(('0.0.0.0', 8080), Handler)
			server.serve_forever()
		except IOError:
			time.sleep(1.0)




if __name__=="__main__":
 	 	
   	# trace simulation status
   	SimFunc=  MySimFunction ()	
   	p1 = Process(target=SimFunc.detectDDDSimulation)
   	p1.start()
   
   	runServer()
   	p1.join()
	


