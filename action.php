<?php 

if(isset($_POST["FunctionName"])){ // tackle different functions requested from jquery
		
		$funcName = $_POST["FunctionName"];
		
		if($funcName =="SaveConfiguration"){
	
			$Experiment_Name = $_POST["Experiment_Name"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Experiment>'.$Experiment_Name.'</Experiment>
				<data>'.base64_encode($_POST["data"]).'</data>
				<mode>'.$_POST["mode"].'</mode>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";	
		}
		else if ($funcName =="CreateSimulation"){
			$Experiment_Name = $_POST["Experiment_Name"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Experiment>'.$Experiment_Name.'</Experiment>
				</request>
				';     
			$url = "http://localhost:8080/receiveXML.html"; 	
		}
		else if ($funcName =="CreateSlipSystem"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<data>'.base64_encode($_POST["data"]).'</data>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";
		}
		else if ($funcName =="DeleteSlipSystem"){
			$Crystal=$_POST["crystalName"];
			$SlipSys=$_POST["slipSysName"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<SlipSys>'.$SlipSys.'</SlipSys>
				<Crystal>'.$Crystal.'</Crystal>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";
		}
		else if ($funcName =="SaveSlipSystem"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<data>'.base64_encode($_POST["data"]).'</data>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";	
			
		}
		else if ($funcName =="CreateCrystal"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<data>'.base64_encode($_POST["data"]).'</data>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";
		}
		else if ($funcName =="DeleteCrystal"){
			$Crystal=$_POST["crystalName"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Crystal>'.$Crystal.'</Crystal>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";
		}
		else if ($funcName =="CreateMaterial"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<data>'.base64_encode($_POST["data"]).'</data>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";	
			
		}
		else if ($funcName =="SaveMaterial"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<data>'.base64_encode($_POST["data"]).'</data>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";	
			
		}
		else if ($funcName =="DeleteMaterial"){
			$Material_Name = $_POST["matName"];	
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Material>'.$Material_Name.'</Material>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";	
			
		}
		else if ($funcName =="CreateControl"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<data>'.base64_encode($_POST["data"]).'</data>
				</request>
				';     
			$url = "http://localhost:8080/receiveXML.html"; 	
				     			
		}
		else if ($funcName =="SaveSimControl"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<data>'.base64_encode($_POST["data"]).'</data>
				</request>
				';     
			$url = "http://localhost:8080/receiveXML.html"; 	
				     			
		}
		else if ($funcName =="DeleteControl"){
			$Control_Name = $_POST["ctrlName"];	
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Control>'.$Control_Name.'</Control>
				</request>
				';      
			$url = "http://localhost:8080/receiveXML.html";	
			
		}
		else if ($funcName =="GetParamDescription"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				</request>
				'; 
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="GetAllSimulations"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				</request>
				'; 
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="GetAllReports"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				</request>
				';     
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="DeleteExperiment"){
			$Experiment_Name = $_POST["Experiment_Name"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Experiment>'.$Experiment_Name.'</Experiment>
				</request>
				'; 
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="LoadExperiment"){
			$Experiment_Name = $_POST["Experiment_Name"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Experiment>'.$Experiment_Name.'</Experiment>
				</request>
				'; 
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="ExportConfiguration"){
			$Simulation_Name = $_POST["Simulation_Name"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Simulation>'.$Simulation_Name.'</Simulation>
				</request>
				'; 
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="GetMaterial"){
			$Material_Name = $_POST["Material_Name"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Material>'.$Material_Name.'</Material>
				</request>
				'; 
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="GetSimControl"){
			$Control_Name = $_POST["Control_Name"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Control>'.$Control_Name.'</Control>
				</request>
				';
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="GetCrystal"){
			$Crystal_Name = $_POST["crystalName"];
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<Crystal>'.$Crystal_Name.'</Crystal>
				</request>
				';
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="GetSlipSystem"){
			$condition = $_POST["data"];
			$slipSystem = $_POST["slipSystem"];
		
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<SlipSystem>'.base64_encode($slipSystem).'</SlipSystem>
				<data>'.$condition.'</data>
				</request>
				';   
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName == "RunSimulation"){
			$Simulation_Name = $_POST["Simulation_Name"];
			$nProcessors = $_POST["nProcessors"];
			$xml = '
				<request>
		        <FunctionName>RunSimulation</FunctionName>
				<Simulation>'.$Simulation_Name.'</Simulation>
				<NumofProcessors>'.$nProcessors.'</NumofProcessors>
				</request>
				'; 
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName == "DeleteReport"){
			$Report_Name = $_POST["Report_Name"];
			$xml = '
				<request>
		        <FunctionName>DeleteReport</FunctionName>
				<Report>'.$Report_Name.'</Report>
				</request>
				';
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName == "GetReportPath"){
			$Report_Name = $_POST["Report_Name"];
			$xml = '
				<request>
		        <FunctionName>GetReportPath</FunctionName>
				<Report>'.$Report_Name.'</Report>
				</request>
				';
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName == "GetStatsData"){
			//$Report_Name = $_POST["Report_Name"];
			$xml = '
				<request>
		        <FunctionName>GetStatsData</FunctionName>
				<FilePath>'.$_POST["FilePath"].'</FilePath>
				<FileName>'.$_POST["FileName"].'</FileName>
				<XAxis>'.$_POST["xAxis"].'</XAxis>
				<YAxis>'.$_POST["yAxis"].'</YAxis>
				</request>
				';
			$url = "http://localhost:8080/receiveXML.html";
			
		}else if ($funcName =="ResetDatabase"){
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				</request>
				';     
			$url = "http://localhost:8080/receiveXML.html"; 	
		}
		
		
		//Send the HTTP request			 
		$ch = curl_init($url);
		//curl_setopt($ch, CURLOPT_MUTE, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// get and echo HTTP response ,json or data
		$output = curl_exec($ch);
		echo $output;
		curl_close($ch);		
       
}

?>

<?php // For the test use
if(0){
		$funcName ="GetSlipSystem";
		$slipSystem = "Pyramidal <c+a> 2nd order P2ca3 1/3[-1-123](11-22)";
		$slipSystem = urldecode("Pyramidal%20%3Cc%2Ba%3E%202nd%20order%20P2ca3%201%2F3%5B-1-123%5D(11-22)");
		
			$xml = '
				<request>
				<FunctionName>'.$funcName.'</FunctionName>
				<SlipSystem>'.$slipSystem.'</SlipSystem>
				<data>'.$condition.'</data>
				</request>
				';   
			$url = "http://localhost:8080/receiveXML.html";
			 
		$ch = curl_init($url);
		//curl_setopt($ch, CURLOPT_MUTE, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// get and echo HTTP response (json)
		$output = curl_exec($ch); 
		echo $output;
	    curl_close($ch);    
}
?>



	
