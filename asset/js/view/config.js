require_template('config');
myApp.ConfigView = Backbone.View.extend({
    events: {
      'click #okSaveAllBtn':'okSaveAll',
      'click #okClearAllBtn':'okClearAll',
      'click #loadAllBtn':'loadAll',
      'click #okLoadAllBtn':'okLoadAll',
      'click #okLoadStepBtn':'okLoadStep',
      'click .save':'saveStep',
      'click .clear':'clearStep',
      'click .load':'loadStep',
      'click .import-defect':'importDislocation',
      'click .import-material':'importMaterial',
      'click .generate-defect':'generateDislocation',
      'click .visualize':'visualizeStructure',
      'click #addLayerBtn':'addLayer',
      'click #rmLayerBtn':'rmLayer',
      'click #addDefectBtn':'addDefect',
      'click #rmDefectBtn':'rmDefect',
      'click .addnode':'addNode',
      'click .rmnode':'rmNode',
      'click .addslip':'addSlip',
      'click .rmslip':'rmSlip',
      "change select[name='Material_UserDefined']": "selectSrcMaterial",
      "change select[name='Control_UserDefined']": "selectSrcControl",
      "change select[name='Material_Type']": "selectMaterial",
      "change select[name='Control_Name']": "selectControl",
      "change #Load": "selectLoad",
      "change #Form_Type": "selectForm",
      "change #LayerPos": "selectLayerPosition",
      "change select[name='Structure']":"selectStructure",
      "change select[name='Crystal_Type']":"selectCrystal",
      'click #addSlipSysBtn':'createSlipSys',
      'click .modifySlipSys':'modifySlipSys',
      'click .saveSlipSys':'saveSlipSys',
      'click .deleteSlipSys':'delSlipSys',
      'click #crystalMgrBtn':'loadCrystalMgr',
      'click #okCrystalBtn':'createCrystal',
      'click #deleteCrystalBtn':'delCrystal',
      'click #materialMgrBtn':'loadMaterialMgr',
	  'click #controlMgrBtn':'loadControlMgr',
      'click #okMaterialBtn':'createMaterial',
      'click #deleteMaterialBtn':'delMaterial',
      'click #okControlBtn':'createControl',
      'click #deleteControlBtn':'delControl',
      'click #modifyMaterialBtn':'modifyMaterial',
	  'click #modifyControlBtn':'modifyControl',
	  'click #saveMaterialBtn':'saveMaterial',
	  'click #saveControlBtn':'saveControl',
      "click input.target-function":"selectTargetFunction",
   
    },

    initialize: function(options) {
      
      // get template
      pageTemplate = _.template($('#config').html());
      layerTemplate = _.template($('#layer').html());
      slipTemplate = _.template($('#slip').html());
      defectTemplate = _.template($('#defect').html());
      nodeTemplate = _.template($('#node').html());
      
      this.simName = options.simName;
      this.router = options.router;
      var self = this; 
      
      //get configuration
      $.ajax({
			async: false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=LoadExperiment&Experiment_Name=' + this.simName,
			success : function(response) {
				//console.log(response);
				self.configuration = response;
				
			}
	 });
	
     //get slip systems
      $.ajax({
			async: false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSlipSystem',
			success : function(response) {
				self.slipsys = response;
			}
	 });
	  
      return this.render();
    },

    render: function(eventName) {

      //------Render main page-------------
      var self = this; 
      var configuration = JSON.parse(this.configuration);
      var status = configuration['status'];
      configuration['simName'] = this.simName;
      configuration['Num_Defect'] = configuration['Defects'].length;
      console.log(configuration);     
      $(this.el).html(pageTemplate(configuration));
      
      //-----Create the configuration wizard--------
      console.log(status);
      if (status == "completed" || status == "submitted" || status =="crashed" ){
      	$("#wizard").steps({
			headerTag : "legend",
			bodyTag : "fieldset",
			transitionEffect : "none",
			enableAllSteps : true,
			enableFinishButton:false,
			labels: {
        		finish: "Submit"
        	}
	 	 });
	 	 $(".save,.load,.clear,.import-defect,.import-material,.random-generate,#saveAllBtn,#loadAllBtn,#clearAllBtn").hide();
      }
      else{
      	$("#wizard").steps({
			headerTag : "legend",
			bodyTag : "fieldset",
			transitionEffect : "none",
			enableAllSteps : true,
			onFinished: function (event, currentIndex) { 
				var mode = $('.target-function:checked').val();
				submitConfiguration(self.simName,mode,self.router);
				
			},
			labels: {
        		finish: "Submit"
        	}
	 	 });
      }
      
	   
	  //--------Render layer list---------
	  var list = $(this.el).find("#list-layers");
	  var content ="";
      $.each(configuration["Layers"],function(i,data){
      	var layer = {};
      	layer = data;
      	layer['LayerID'] = i+1;
      	$(list).append(layerTemplate(layer));
      	// fill select fields
      	var container = $(".layer-content")[i];
      	$(container).find("#Anisotropy").val(data["Anisotropy"]);
      	$(container).find("select.readonly option").attr("disabled","disabled");
      	
      	var isUserDefined = parseInt(data["Material_UserDefined"]);
      	$(container).find("input[name='Material_UserDefined'][value='"+isUserDefined+"']").attr('checked',true);
      	   	
      });  
      $(".layer-content").last().addClass("in");
	  
	  //--------Render slip list----------
	  $.each(configuration["Layers"],function(i,d){
	  	var div = $(".layer-content .slip-contents")[i];
	  	$.each(d['Slips'],function(j,data){
	  		$(div).append(slipTemplate(data));
	  	});
	  });
	  
	  //------Rneder defect list-------
	  var list = $(this.el).find("#list-defects");
	  var content ="";
      $.each(configuration["Defects"],function(i,data){
      	var defect = {};
      	defect = data;
      	defect['DefectID'] = i+1;
      	$(list).append(defectTemplate(defect));     	
      	//fill select fields
      	var container = $(".defect-content")[i];
      	var type = data["Form_Type"];
      	$(container).find("#LayerPos").val(data["LayerPos"]);     	
      	$(container).find("#Form_Type").val(data["Form_Type"]);
      	if(type == '0'){
   			$(container).find('.form-content').show();
   		}
   		else if (type == '1'){
   			$(container).find('.form-content').hide();
   		}
      });   
	  
	  //-------Render node list--------------------
	  $.each(configuration["Defects"],function(i,d){	  	
		var div = $(".defect-content  .node-contents")[i];
		$.each(d['Nodes'], function(j, data) {
			data["NodeID"] = j+1;
			//console.log(data);
			$(div).append(nodeTemplate(data));
		}); 
		
	  });
  	  
  	  //fill the rest of select fields and disable readonly inputs
	  $("#iInterface").val(configuration["iInterface"]);
	  $("#CheckNeiBur").val(configuration["CheckNeiBur"]);
	  $("#iSplineFe").val(configuration["iSplineFe"]);
	  $("#iDebugMode").val(configuration["iDebugMode"]);
	  $("#iRelaxtion").val(configuration["iRelaxtion"]);
	  $("#iFEM").val(configuration["iFEM"]);
	  $("#LogInteraction").val(configuration["LogInteraction"]);
	  $("#iCrossSlip").val(configuration["iCrossSlip"]);
	  $("#FullyPeriodic").val(configuration["FullyPeriodic"]);
	  $("#iInertial").val(configuration["iInertial"]);
	  $("#Load").val(configuration["Load"]);
	  $("select.readonly option").attr("disabled","disabled");
  	  
  	  
  	  //------Initiate simulation list for load preview-------------
  	  initSimulationList();
	  
	  //------Initiate material manager list/ crystal manager list & Give material src/type for each layer---------
	  initCrystalMgrList();
	  initMaterialMgrList();
	  $.each(configuration["Layers"],function(i,data){
	  	var container = $(".layer-content")[i];
      	$(container).find("#Material_UserDefined").val(data["Material_UserDefined"]);
   		if(data["Material_UserDefined"] == '1'){ // if user defined, unlock the fields	
   			var input_field = "<input type='text' name='Material_Type' id='Material_Type' />";
   			$(container).find("#Material_Type").replaceWith(input_field);
   			$(container).find("input").not("#nSlip").removeAttr("readonly");
   			$(container).find("select").removeClass("readonly");
   			$(container).find("select option").attr("disabled",false);
   			$(container).find("#addSlipBtn,#rmSlipBtn").attr("disabled",false);
   		}
   		
      	//put material values/ crystal values
      	$(container).find("#Material_Type").val(data["Material_Type"]);
      	$(container).find("#Crystal_Type").val(data["Crystal_Type"]);
      	
      	$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetCrystal&crystalName='+data["Crystal_Type"],
			success : function(response) {
				self.crystal = response;
			}
		});
		var crystal = JSON.parse(self.crystal);
		var numMode=crystal["nMode"];
		$(container).find(".slip-contents select[name='mode']").html("");
		for (var i=1; i<=numMode;++i){
			$(container).find(".slip-contents select[name='mode']").append("<option value='"+i+"'>"+i+"</option>");
		}
		
		//put mode index for each slip
		$.each($(container).find(".slip-contents select[name='mode']"), function(i) {
			$(this).val(data['Slips'][i]["mode"]);
		});
      	
      	
      	$("select.readonly option").attr("disabled","disabled");
	  });
	  
	  //fill value for slip system     
      updateSlipSysOptions(this.slipsys);
      $.each(configuration["Defects"],function(i,data){
      	var container = $(".defect-content")[i];
      	$(container).find("#System_Name").val(data["System_Name"]);
      });
      $(".defect-content").last().addClass("in");
	  
	  //------Initiate control manager list & Give control src/type for each layer---------
	  initControlMgrList();
	  var load = configuration['Load'];
	  if(load == "1"){
	  	$("fieldset .applied_force label").html("Applied Strain Rate(s<sup>-1</sup>)");
   	  }
   	  else if(load == "2"){
   		$("fieldset .applied_force label").html("Applied Stress Rate(MPa)");
   	  }    
   	  if(configuration["Control_UserDefined"] == '1'){ // if user defined, unlock the fields
   	  		
   	  		var container = $("#wizard-p-2");
   	  		$(container).find("#Control_UserDefined").val(configuration["Control_UserDefined"]);
   			var input_field = "<input type='text' name='Control_Name' id='Control_Name'/>";
   			$(container).find("#Control_Name").replaceWith(input_field);
   			$(container).find("input").removeAttr("readonly");
   			$(container).find("select").removeClass("readonly");
   			$(container).find("select option").attr("disabled",false);
   			
   	  }
   	  $("#Control_Name").val(configuration["Control_Name"]);
	 		 
	  //-------Add parameter description----------
	  addParamDescription();	
  
      return this;
    },
	
	//**************************************************************
	// Function: okSaveAll 
	// @note: Save all configuration parameters (mode:0)
	//**************************************************************
	okSaveAll:function(){
		var mode = 0;	
		var simName = $("#Simulation_Name").text();
		var form = $("#config-form");
		var json_data = $(form).DDDFormToJSON();
		var data=JSON.stringify(json_data);
		console.log(json_data);
	
		$.ajax({
			async:false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=SaveConfiguration&Experiment_Name='+ simName +'&data=' + data +'&mode='+ mode,
			success : function(response) {
				alert("Save all parameters");
				$("#saveAllDialog").modal('hide');
				//window.location.reload();
			}
		});
		
		//$("#saveAllDialog").modal('hide');
		
	},
	
	//**************************************************************
	// Function: okClearAll
	// @note: Clear all configuration parameters
	//**************************************************************
	okClearAll:function(){
		// update the number of layers, defects, and nodes
		var curNumLayers = parseInt($("#Num_Layer").val());
		var curNumDefects = parseInt($("#Num_Defect").val());
		while (1 < curNumLayers){
			$("#rmLayerBtn").click();
			curNumLayers = parseInt($("#Num_Layer").val());
		}
		while (1 < curNumDefects){
			$("#rmDefectBtn").click();
			curNumDefects = parseInt($("#Num_Defect").val());
		}
		$("fieldset .defect-content").each(function(i){
			var curNumNodes = parseInt($(this).find("#nNode").val());
			while (2 < curNumNodes) {
				$(this).find(".rmnode").click();
				curNumNodes = parseInt($(this).find("#nNode").val());
			}
		});
		var form = $("#config-form");
		$(form).find('input,select').each(function(i) {
			$(this).val("");
		});	
		//Set back the default values
		$('#Num_Layer').val(1);
		$('#Num_Defect').val(1);
		$("input[name='nNode']").val(2);
		
		$("#clearAllDialog").modal('hide');
	},
	
	//**************************************************************
	// Function: loadAll 
	// @note: Display the preview of all parameters of simulations
	//**************************************************************
	loadAll:function(){
		$("#loadAllDialog").modal("show");
		$("#okLoadStepBtn").hide();
		$("#okLoadAllBtn").show();
		$("#loadAllDialog .modal-title").text("Load parameters for all steps");
		
	},
	
	//**************************************************************
	// Function: okLoadAll 
	// @note: Load all parameters from a selected simulation
	//**************************************************************
	okLoadAll:function(){
		var simName = $('.list-simulation .list-group-item.active').text();	
 		var self = this;
 		//get configuration
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=LoadExperiment&Experiment_Name=' + simName,
			success : function(response) {
				self.configuration = response;
			}
		}); 
		
		//get slip systems	     
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSlipSystem',
			success : function(response) {
				self.slipsys = response;
			}
		}); 

	  	
	  	//load configuration
	  	var configuration = JSON.parse(self.configuration);
	  	configuration['Num_Defect'] = configuration['Defects'].length;
	  	//console.log(configuration);
			
		var fieldsets = $("fieldset");
		fieldsets.each(function(i) {
			
			if(i==3 || i==4){ //step3,4,5
				$(this).children( "div" ).find("select,input").each(function(j){
					$(this).val(configuration[$(this).attr('id')]);
				});
			}
			else if(i==2){ //step3
				
				
				var load = configuration['Load'];
				if (load == "1") {
					$("fieldset .applied_force label").html("Applied Strain Rate(s<sup>-1</sup>)");
				} else if (load == "2") {
					$("fieldset .applied_force label").html("Applied Stress Rate(MPa)");
				}
				
				if (configuration["Control_UserDefined"] == '1') {// if user defined, unlock the fields
					var container = $("#wizard-p-2");
					$(container).find("#Control_UserDefined").val(configuration["Control_UserDefined"]);
					var input_field = "<input type='text' name='Control_Name' id='Control_Name'/>";
					$(container).find("#Control_Name").replaceWith(input_field);
					$(container).find("input").removeAttr("readonly");
					$(container).find("select").removeClass("readonly");
					$(container).find("select option").attr("disabled", false);
	
				}
				else if (configuration["Control_UserDefined"] == '0') {// if user defined, unlock the fields
					var container = $("#wizard-p-2");
					$(container).find("#Control_UserDefined").val(configuration["Control_UserDefined"]);
					var select_field = "<select id='Control_Name' name='Control_Name'></select>";
		   			$(container).find("#Control_Name").replaceWith(select_field);
		   			initControlMgrList();
		   			$(container).find("input").not("input[name='Control_UserDefined'],#Control_Name,#ILoop_Time,#DTIME_MAX,#DTIME_MIN")
		   			.attr("readonly","readonly");
		   			$(container).find("select").not("#Control_Name").not("#Control_UserDefined").addClass("readonly");
		   			$(container).find("select").not("#Control_Name").not("#Control_UserDefined").find("option").attr("disabled",true);
					
				}
				$("#Control_Name").val(configuration["Control_Name"]); 
				$("#Control_Name").change(); 
				$(this).children("div").find("select,input").each(function(j) {
					$(this).val(configuration[$(this).attr('id')]);
				});
				
			}
			
			else if (i==0){//step1
				$(this).children( "div" ).find("select,input").each(function(j){
					$(this).val(configuration[$(this).attr('id')]);
				});
				
				//--------Render layer list---------
				var list = $("#list-layers").html('');
				var content = "";
				$.each(configuration["Layers"], function(i, data) {
					var layer = {};
					layer = data;
					layer['LayerID'] = i + 1;
					$(list).append(layerTemplate(layer));
					// fill select fields
					var container = $(".layer-content")[i];
					$(container).find("#Anisotropy").val(data["Anisotropy"]);
					$(container).find("select.readonly option").attr("disabled", "disabled");

					var isUserDefined = parseInt(data["Material_UserDefined"]);
					$(container).find("input[name='Material_UserDefined'][value='" + isUserDefined + "']").attr('checked', true);

				});
				$(".layer-content").last().addClass("in");

				//--------Render slip list----------
				$.each(configuration["Layers"], function(i, d) {
					var div = $(".layer-content .slip-contents")[i];
					$.each(d['Slips'], function(j, data) {
						$(div).append(slipTemplate(data));
					});
				}); 
				
				//------Initiate material manager list & Give material src/type for each layer---------  
				initCrystalMgrList();
				initMaterialMgrList();
				$.each(configuration["Layers"], function(i, data) {
					var container = $(".layer-content")[i];
					$(container).find("#Material_UserDefined").val(data["Material_UserDefined"]);
					if (data["Material_UserDefined"] == '1') {// if user defined, unlock the fields
						var input_field = "<input type='text' name='Material_Type' id='Material_Type' />";
						$(container).find("#Material_Type").replaceWith(input_field);
						$(container).find("input").not("#nSlip").removeAttr("readonly");
						$(container).find("select").removeClass("readonly");
						$(container).find("select option").attr("disabled", false);
						$(container).find("#addSlipBtn,#rmSlipBtn").attr("disabled", false);
					}

					//put material values/ crystal values
					$(container).find("#Material_Type").val(data["Material_Type"]);
					$(container).find("#Crystal_Type").val(data["Crystal_Type"]);

					$.ajax({
						async : false,
						type : 'POST',
						url : 'action.php',
						data : 'FunctionName=GetCrystal&crystalName=' + data["Crystal_Type"],
						success : function(response) {
							self.crystal = response;
						}
					});
					var crystal = JSON.parse(self.crystal);
					var numMode = crystal["nMode"];
					$(container).find(".slip-contents select[name='mode']").html("");
					for (var i = 1; i <= numMode; ++i) {
						$(container).find(".slip-contents select[name='mode']").append("<option value='" + i + "'>" + i + "</option>");
					}

					//put mode index for each slip
					$.each($(container).find(".slip-contents select[name='mode']"), function(i) {
						$(this).val(data['Slips'][i]["mode"]);
					});

					$("select.readonly option").attr("disabled", "disabled"); 


				});      			
				
			}
			else if (i==1){//step2

				
				configuration['Num_Defect'] = configuration['Defects'].length;
				$(this).children("div").find("select,input").each(function(j) {
					$(this).val(configuration[$(this).attr('id')]);
				});

				var list = $("#list-defects").html('');
				var content = "";
				$.each(configuration["Defects"], function(i, data) {
					var defect = {};
					defect = data;
					defect['DefectID'] = i + 1;
					$(list).append(defectTemplate(defect));

					//fill select fields
					var container = $(".defect-content")[i];
					var type = data["Form_Type"];
					$(container).find("#LayerPos").val(data["LayerPos"]);
					$(container).find("#Form_Type").val(data["Form_Type"]);
					if (type == '0') {
						$(container).find('.form-content').show();
					} else if (type == '1') {
						$(container).find('.form-content').hide();
					}
				});
				//fill value for slip system
				updateSlipSysOptions(self.slipsys);
				$.each(configuration["Defects"], function(i, data) {
					var container = $(".defect-content")[i];
					$(container).find("#System_Name").val(data["System_Name"]);
				});
				$(".defect-content").last().addClass("in");

				//-------Render node list--------------------
				$.each(configuration["Defects"], function(i, d) {
					var div = $(".defect-content  .node-contents")[i];
					$.each(d['Nodes'], function(j, data) {
						data["NodeID"] = j + 1;
						//console.log(data);
						$(div).append(nodeTemplate(data));
					});

				}); 
 				
			}	
		});
	
    	$("#loadAllDialog").modal("hide");	
	},
	
	//**************************************************************
	// Function: okLoadStep 
	// @note: Load all parameters from current step/section
	//**************************************************************
	okLoadStep:function(){
		
		var simName = $('.list-simulation .list-group-item.active').text();	
 		var self = this;
 		//get configuration
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=LoadExperiment&Experiment_Name=' + simName,
			success : function(response) {
				self.configuration = response;
			}
		}); 
		
		//get slip systems	     
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSlipSystem',
			success : function(response) {
				self.slipsys = response;
			}
		}); 

	  	
	  	//load configuration
	  	var configuration = JSON.parse(self.configuration);
	  	configuration['Num_Defect'] = configuration['Defects'].length;
	  	//console.log(configuration);
		
		var fieldset = $("fieldset.current");
		var s= parseInt($(fieldset).attr("id").substring(9,10)); 
		alert("Load parameters for step"+(s+1));
	
		if ( s == 3 || s == 4) {//step3,4,5
			$(fieldset).children("div").find("select,input").each(function(j) {
				$(this).val(configuration[$(this).attr('id')]);
			});
		} else if (s==2) {//step3
				
			var load = configuration['Load'];
			if (load == "1") {
				$("fieldset .applied_force label").html("Applied Strain Rate(s<sup>-1</sup>)");
			} else if (load == "2") {
				$("fieldset .applied_force label").html("Applied Stress Rate(MPa)");
			}
			
			if (configuration["Control_UserDefined"] == '1') {// if user defined, unlock the fields
				var container = $("#wizard-p-2");
				$(container).find("#Control_UserDefined").val(configuration["Control_UserDefined"]);
				var input_field = "<input type='text' name='Control_Name' id='Control_Name'/>";
				$(container).find("#Control_Name").replaceWith(input_field);
				$(container).find("input").removeAttr("readonly");
				$(container).find("select").removeClass("readonly");
				$(container).find("select option").attr("disabled", false);

			}
			else if (configuration["Control_UserDefined"] == '0') {// if user defined, unlock the fields
				var container = $("#wizard-p-2");
				$(container).find("#Control_UserDefined").val(configuration["Control_UserDefined"]);
				var select_field = "<select id='Control_Name' name='Control_Name'></select>";
	   			$(container).find("#Control_Name").replaceWith(select_field);
	   			initControlMgrList();
	   			$(container).find("input").not("input[name='Control_UserDefined'],#Control_Name,#ILoop_Time,#DTIME_MAX,#DTIME_MIN")
	   			.attr("readonly","readonly");
	   			$(container).find("select").not("#Control_Name").not("#Control_UserDefined").addClass("readonly");
	   			$(container).find("select").not("#Control_Name").not("#Control_UserDefined").find("option").attr("disabled",true);
				
			}
			$("#Control_Name").val(configuration["Control_Name"]); 
			$("#Control_Name").change();
			$(fieldset).children("div").find("select,input").each(function(j) {
				$(this).val(configuration[$(this).attr('id')]);
			});

		} else if (s == 0) {//step1
			
			$(fieldset).children("div").find("select,input").each(function(j) {
				$(this).val(configuration[$(this).attr('id')]);
			});

			//--------Render layer list---------
			var list = $("#list-layers").html('');
			var content = "";
			$.each(configuration["Layers"], function(i, data) {
				var layer = {};
				layer = data;
				layer['LayerID'] = i + 1;
				$(list).append(layerTemplate(layer));
				// fill select fields
				var container = $(".layer-content")[i];
				$(container).find("#Crystal_Type").val(data["Crystal_Type"]);
				$(container).find("#Anisotropy").val(data["Anisotropy"]);
				$(container).find("select.readonly option").attr("disabled", "disabled");

				// var isUserDefined = parseInt(data["Material_UserDefined"]);
				// $(container).find("input[name='Material_UserDefined'][value='" + isUserDefined + "']").attr('checked', true);

			});
			$(".layer-content").last().addClass("in");

			//--------Render slip list----------
			$.each(configuration["Layers"], function(i, d) {
				var div = $(".layer-content .slip-contents")[i];
				$.each(d['Slips'], function(j, data) {
					$(div).append(slipTemplate(data));
				});
			});

			//------Initiate material manager list & Give material src/type for each layer---------
			initMaterialMgrList(); 
			$.each(configuration["Layers"], function(i, data) {
				var container = $(".layer-content")[i];
				$(container).find("#Material_UserDefined").val(data["Material_UserDefined"]);
				if (data["Material_UserDefined"] == '1') {// if user defined, unlock the fields
					var input_field = "<input type='text' name='Material_Type' id='Material_Type' />";
					$(container).find("#Material_Type").replaceWith(input_field);
					$(container).find("input").not("#nSlip").removeAttr("readonly");
					$(container).find("select").removeClass("readonly");
					$(container).find("select option").attr("disabled", false);
					$(container).find("#addSlipBtn,#rmSlipBtn").attr("disabled", false);
				}

				$(container).find("#Material_Type").val(data["Material_Type"]);

			}); 


		} else if (s == 1) {//step2
			
			configuration['Num_Defect'] = configuration['Defects'].length;
			$(fieldset).children("div").find("select,input").each(function(j) {
				$(this).val(configuration[$(this).attr('id')]);
			});
				
			var list = $("#list-defects").html('');
			var content = "";
			$.each(configuration["Defects"], function(i, data) {
				var defect = {};
				defect = data;
				defect['DefectID'] = i + 1;
				$(list).append(defectTemplate(defect));
				
				//fill select fields
				var container = $(".defect-content")[i];
				var type = data["Form_Type"];
				$(container).find("#LayerPos").val(data["LayerPos"]);
				$(container).find("#Form_Type").val(data["Form_Type"]);
				if (type == '0') {
					$(container).find('.form-content').show();
				} else if (type == '1') {
					$(container).find('.form-content').hide();
				}
			});
			//fill value for slip system
			updateSlipSysOptions(this.slipsys);
			$.each(configuration["Defects"], function(i, data) {
				var container = $(".defect-content")[i];
				$(container).find("#System_Name").val(data["System_Name"]);
			});
			$(".defect-content").last().addClass("in");

			//-------Render node list--------------------
			$.each(configuration["Defects"], function(i, d) {
				var div = $(".defect-content  .node-contents")[i];
				$.each(d['Nodes'], function(j, data) {
					data["NodeID"] = j + 1;
					//console.log(data);
					$(div).append(nodeTemplate(data));
				});

			}); 

		}

		 $("#loadAllDialog").modal("hide");	
	},
	
	//**************************************************************
	// Function: saveStep
	// @note: Save configuration parameters for current step/section
	//**************************************************************
	saveStep:function(e){
		var mode = parseInt($("fieldset.current").attr("id").substring(9,10))+1; 	
		var simName = $("#Simulation_Name").text();
		var form = $("#config-form");
		var json_data = $(form).DDDFormToJSON();
		var data= JSON.stringify(json_data);
		console.log(json_data);
	
		$.ajax({
			async:false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=SaveConfiguration&Experiment_Name='+ simName +'&data=' + data +'&mode='+ mode,
			success : function(response) {
				alert("Save parameters for current step");
				// if (mode==1 || mode==3) {
					// //window.location.reload();
				// }
			}
		});
	},
	
	//**************************************************************
	// Function: clearStep
	// @note: Clear onfiguration parameters for current step/section
	//**************************************************************
	clearStep:function(){
		var s= parseInt($("fieldset.current").attr("id").substring(9,10)); 
		alert("Clear parameters for step"+(s+1));
		
		if(s==2||s==3||s==4){
			$("fieldset.current").find('input,select').each(function(i) {
				$(this).val("");
			});
		}
		//Set back the default values
		if(s==0) {
			var curNumLayers = parseInt($("#Num_Layer").val());
			while (1 < curNumLayers){
				$("#rmLayerBtn").click();
				curNumLayers = parseInt($("#Num_Layer").val());
			}
			$("fieldset.current").find('input,select').each(function(i) {
				$(this).val("");
			});
			$('#Num_Layer').val(1);
			
		}
		if(s==1){
			var curNumDefects = parseInt($("#Num_Defect").val());
			while (1 < curNumDefects){
				$("#rmDefectBtn").click();
				curNumDefects = parseInt($("#Num_Defect").val());
			}
			$("fieldset .defect-content").each(function(i){
				var curNumNodes = parseInt($(this).find("#nNode").val());
				while (2 < curNumNodes) {
					$(this).find(".rmnode").click();
					curNumNodes = parseInt($(this).find("#nNode").val());
				}
			});
			$("fieldset.current").find('input,select').each(function(i) {
				$(this).val("");
			});
			
			$('#Num_Defect').val(1);
			$("input[name='nNode']").val(2);
		}
		
	},
	
	//**************************************************************
	// Function: loadStep
	// @note: Load preview of parameters for current step/section
	//**************************************************************
	loadStep:function(){
		var s= parseInt($("fieldset.current").attr("id").substring(9,10)); 
		$("#loadAllDialog").modal("show");
		$(".simulation-preview .nav-tabs li").removeClass("active");
		$(".simulation-preview .tab-content .tab-pane").removeClass("active");
		
		$(".simulation-preview .nav-tabs li").each(function(i){
			if (i==s) $(this).addClass("active");
			//else $(this).hide();
		});
		
		$(".simulation-preview .tab-content .tab-pane").each(function(i){
			if (i==s) $(this).addClass("active");
			//else $(this).hide();
		});

		$("#okLoadAllBtn").hide();
		$("#okLoadStepBtn").show();
		$("#loadAllDialog .modal-title").text("Load parameters for step "+(s+1));
		
	},
	//**************************************************************
	// Function: addLayer
	// @note: add a new layer
	//**************************************************************
	addLayer:function(){		
		//update the number of layers
		var numLayers = parseInt($("#Num_Layer").val());	
		numLayers++;
		$("#Num_Layer").val(numLayers);
		//add layer item
		var list = $(this.el).find("#list-layers");
		var layer = {};
		var content ="";
      	layer['LayerID'] = numLayers;    	
      	list.append(layerTemplate(layer));
      	$(".layer-content").last().find('input,select').val('');
      	$(".layer-content").last().find('#Crystal_Type').val(0);
		$(".layer-content").last().addClass("in");
		$(".layer-content").last().find("select.readonly option").attr("disabled","disabled");
		//update material options
		var options = $(".layer-content select[name='Material_Type']").first().html();
		$(".layer-content select[name='Material_Type']").last().append(options);
		$(".layer-content").last().find('#Material_Type').change();
		//update layer position options
		$("select[name='LayerPos']").html("");
   		for (var i=1;i<numLayers+1;++i)
   			$("select[name='LayerPos']").append("<option value='"+i+"'>"+i+"</option>");
   		
   		//-------Add parameter description----------
	  	addParamDescription();
		
	},
	//**************************************************************
	// Function: rmLayer
	// @note: remove the last layer
	//**************************************************************
	rmLayer:function(){
		var numLayers = parseInt($("#Num_Layer").val());
		if (numLayers > 1) {
			//update the number of layers
			numLayers--;
			$("#Num_Layer").val(numLayers);
			
			//remove layer item
			$("#list-layers li").last().remove();
			
			//update layer position options
			$("select[name='LayerPos']").html("");
   			for (var i=1;i<numLayers+1;++i)
   				$("select[name='LayerPos']").append("<option value='"+i+"'>"+i+"</option>");
		}
	},
	//**************************************************************
	// Function: addDefect
	// @note: add a new defect
	//**************************************************************
	addDefect:function(){
		
		//update the number of layers and store slip system values for existing defects
		var numDefects = parseInt($("#Num_Defect").val());	
		var tempData = [];
		for (var i=0;i<numDefects;++i){
			var container = $(".defect-content")[i];
			tempData.push($(container).find("#System_Name").val());
		}
		
		numDefects++;
		$("#Num_Defect").val(numDefects);
		
		//add defect item
		var list = $(this.el).find("#list-defects");
		var defect = {};
		var content ="";
      	defect['DefectID'] = numDefects;    	
      	list.append(defectTemplate(defect));
      	$(".defect-content").last().find('input,select').val('');
      	$(".defect-content").last().find('#LayerPos').val(1);
		$(".defect-content").last().addClass("in");
		
		//update slip system
		var self = this;
		updateSlipSysOptions(self.slipsys);
		//give back the slip system values
		for (var i=0;i<tempData.length;++i){
			var container = $(".defect-content")[i];
			$(container).find("#System_Name").val(tempData[i]);
		}
		
		//add node item
		var div =$(".defect-content").last();
		data={};
		$(div).find("#nNode").val(2);
		for(var i=0;i<2;++i){
			data["NodeID"] = i+1;
			$(div).find('.node-contents').append(nodeTemplate(data));
			$(div).find('.node-contents').find(".node-content").last().find('input,select').val('');
				
		}
		//update layer position options
		var numLayers = parseInt($("#Num_Layer").val());
		$("select[name='LayerPos']").html("");
   		for (var i=1;i<numLayers+1;++i)
   			$("select[name='LayerPos']").append("<option value='"+i+"'>"+i+"</option>");
   			
   		//-------Add parameter description----------
	  	addParamDescription();
				
	},
	//**************************************************************
	// Function: rmDefect
	// @note: remove the last defect 
	//**************************************************************
	rmDefect:function(){
		var numDefects = parseInt($("#Num_Defect").val());
		if (numDefects > 1) {
			//update the number of layers
			numDefects--;
			$("#Num_Defect").val(numDefects);
			//remove layer item
			$("#list-defects li").last().remove();
		
		}
	},
   	//**************************************************************
	// Function: addNode
	// @note: add a new defect node
	//**************************************************************
   	addNode:function(e){
   		var data = {};
   		var div = $(e.target).parents('.defect-content');
   		var numNodes = parseInt($(div).find('#nNode').val());
   		numNodes++;
   		data["NodeID"] = numNodes;
   		$(div).find('#nNode').val(numNodes);
   		$(div).find('.node-contents').append(nodeTemplate(data));
   		$(div).find('.node-contents').find(".node-content").last().find('input,select').val('');
   		//console.log($(e.target).parents('.defect-content'));
   		//-------Add parameter description----------
	  	addParamDescription();
		
   	},
   	//**************************************************************
	// Function: rmNode
	// @note: remove the last defect node
	//**************************************************************
   	rmNode: function(e){
   		var div = $(e.target).parents('.defect-content');
   		var numNodes = parseInt($(div).find('#nNode').val());
   		if (numNodes > 2){
   			numNodes--;
   			$(div).find('#nNode').val(numNodes);
   			$(div).find('.node-contents').find('.node-content').last().remove();
   		}
   		
   	},
   	//**************************************************************
	// Function: addSlip
	// @note: add a new Slip
	//**************************************************************
   	addSlip:function(e){
   		var container = $(e.target).parents('.modal-body,.layer-content');
   		var numSlip = parseInt($(container).find('#nSlip').val());
   		//console.log(numSlip);
   		numSlip++;
   		$(container).find('#nSlip').val(numSlip);
   		var div = $(container).find('.slip-content').last();		
   		$(div).clone().insertAfter($(div)).find("input,select").val('');
   		
   	},
   	//**************************************************************
	// Function: rmSlip
	// @note: remove the last Slip
	//**************************************************************
   	rmSlip:function(e){
   		var container = $(e.target).parents('.modal-body,.layer-content');
   		var numSlip = parseInt($(container).find('#nSlip').val());
   		if(numSlip>1){
   			numSlip--;
   			$(container).find('#nSlip').val(numSlip);
   			$(container).find('.slip-content').last().remove();
   		}

   	},
   	
   	selectSrcMaterial:function(e){
   		//console.log($(e.target).val());
   		var isUserDefined = parseInt($(e.target).val());
   		if (isUserDefined == 0){
   			var select_field = "<select id='Material_Type' name='Material_Type' value='<%=Material_Type%>'></select>";
   			$(e.target).parents(".layer-content").find("#Material_Type").replaceWith(select_field);
   			$(e.target).parents(".layer-content").find("input")
   			.not("input[name='Material_UserDefined'],#Material_Type,#Thickness,#Euler_Angles_x,#Euler_Angles_y,#Euler_Angles_z")
   			.attr("readonly","readonly");
   			$(e.target).parents(".layer-content").find("select").not("#Material_Type").not($(e.target)).addClass("readonly");
   			$(e.target).parents(".layer-content").find("select").not("#Material_Type").not($(e.target)).find("option").attr("disabled",true);
   			$(e.target).parents(".layer-content").find("#addSlipBtn,#rmSlipBtn").attr("disabled",true);
   			
   			// update Material option list without erasing the values for each layer
   			var material_type =[];
	   		$.each($("#config-form select[name='Material_Type'],#config-form input[name='Material_Type']"),function(){
	   			material_type.push($(this).val());
	   		});
	   		initMaterialMgrList();
	 		$.each($("#config-form select[name='Material_Type']"),function(i){
	   			$(this).val(material_type[i]);
	   		});	
	   		$(e.target).parents(".layer-content").find("#Material_Type").val("Default");
	   		$(e.target).parents(".layer-content").find("#Material_Type").change();
   			
   		}
   		else if(isUserDefined == 1){
   			var input_field = "<input type='text' name='Material_Type' id='Material_Type'/>";
   			$(e.target).parents(".layer-content").find("#Material_Type").replaceWith(input_field);
   			$(e.target).parents(".layer-content").find("input").not("#nSlip").removeAttr("readonly");
   			$(e.target).parents(".layer-content").find("select").removeClass("readonly");
   			$(e.target).parents(".layer-content").find("select option").attr("disabled",false);
   			$(e.target).parents(".layer-content").find("#addSlipBtn,#rmSlipBtn").attr("disabled",false);
   		}
   		
   	},
   	//**************************************************************
	// Function: selectMaterial
	// @note: select material type for each single layer
	//**************************************************************
   	selectMaterial:function(e){
   		var matName = $(e.target).val();
		//remove previous data
		$(e.target).parents(".layer-content").find(".slip-contents").html("");
		var self = this;
		//update new data
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetMaterial&Material_Name=' + matName,
			success : function(response) {
				self.material=response;
			}
		});
		
		var material = JSON.parse(this.material);
		var div = $(e.target).parents('.layer-content').find(".slip-contents");
		$(e.target).parents('.layer-content').find("input, select").each(function(i) {
			$(this).val(material[0][$(this).attr('id')]);
		});
		$.each(material[0]['Slips'], function(i, data) {
			$(div).append(slipTemplate(data));
		});
		

		//update mode index
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetCrystal&crystalName='+material[0]["Crystal_Type"],
			success : function(response) {
				self.crystal = response;
			}
		});
		var crystal = JSON.parse(this.crystal);
		var numMode=crystal["nMode"];
		$(div).find("select[name='mode']").html("");
		for (var i=1; i<=numMode;++i){
			$(div).find("select[name='mode']").append("<option value='"+i+"'>"+i+"</option>");
		}
		
		//put mode index for each slip
		$.each($(div).find("select[name='mode']"), function(i) {
			$(this).val(material[0]['Slips'][i]["mode"]);
		});

		//update slip system options
		updateSlipSysOptions(self.slipsys);
		//update field description
		addParamDescription(); 
   	},
   	
   	selectSrcControl:function(e){
   		//console.log($(e.target).val());
   		var isUserDefined = parseInt($(e.target).val());
   		if (isUserDefined == 0){
   			var select_field = "<select id='Control_Name' name='Control_Name'></select>";
   			$(e.target).parents("fieldset").find("#Control_Name").replaceWith(select_field);
   			initControlMgrList();
   			$(e.target).parents("fieldset").find("select#Control_Name").change();
   			$(e.target).parents("fieldset").find("input").not("input[name='Control_UserDefined'],#Control_Name,#ILoop_Time,#DTIME_MAX,#DTIME_MIN")
   			.attr("readonly","readonly");
   			$(e.target).parents("fieldset").find("select").not("#Control_Name").not($(e.target)).addClass("readonly");
   			$(e.target).parents("fieldset").find("select").not("#Control_Name").not($(e.target)).find("option").attr("disabled",true);
   			
   		}
   		else if(isUserDefined == 1){
   			var input_field = "<input type='text' name='Control_Name' id='Control_Name'/>";
   			$(e.target).parents("fieldset").find("#Control_Name").replaceWith(input_field);
   			$(e.target).parents("fieldset").find("input").removeAttr("readonly");
   			$(e.target).parents("fieldset").find("select").removeClass("readonly");
   			$(e.target).parents("fieldset").find("select option").attr("disabled",false);

   		}
   		
   	},
   	//**************************************************************
	// Function: selectControl
	// @note: select simulation control
	//**************************************************************
   	selectControl:function(e){
   		var ctrlName = $(e.target).val();
   		console.log(ctrlName);
		var self = this;
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSimControl&Control_Name=' + ctrlName,
			success : function(response) {
				var control = JSON.parse(response);
				var load = control[0]["Load"];
				$(e.target).parents('fieldset').find("input,select").each(function(i) {
					$(this).val(control[0][$(this).attr('id')]);
					//console.log($(this).val());
				});
				if (load == "1") {
					$(e.target).parents('fieldset').find('.applied_force label').html("Applied Strain Rate(s<sup>-1</sup>)");
				} else if (load == "2") {
					$(e.target).parents('fieldset').find('.applied_force label').html("Applied Stress Rate(MPa)");
				}	
			}
		}); 
		
   	},
   	//**************************************************************
	// Function: selectLoad
	// @note: select load type (stress/strain) 
	//**************************************************************
   	selectLoad:function(e){
   		var load = $(e.target).val();	
		if (load == "1") {
			$(e.target).parents('.modal-body,fieldset').find('.applied_force label').html("Applied Strain Rate(s<sup>-1</sup>)");
		} else if (load == "2") {
			$(e.target).parents('.modal-body,fieldset').find('.applied_force label').html("Applied Stress Rate(MPa)");
		}
   	},
   	//**************************************************************
	// Function: selectForm
	// @note: select form type of dislocation
	//**************************************************************
   	selectForm:function(e){
   		var type = $(e.target).val();
   		if(type == '0'){
   			$(e.target).parents('.defect-content').find('.form-content').show();
   			$(e.target).parents('.defect-content').find('.nNode-content,.node-contents').hide();
 			$(e.target).parents('.defect-content').find('.node-content').find('input').val(0.0);
   		}
   		else if (type == '1'){
   			$(e.target).parents('.defect-content').find('.form-content').hide();
   			$(e.target).parents('.defect-content').find('#LoopRad,#CenterLoop_01,#CenterLoop_02').val(0.0);
   			$(e.target).parents('.defect-content').find('.nNode-content,.node-contents').show();
   		}
   	},
   	
   	//**************************************************************
	// Function: selectLayerPosition
	// @note: select layer position for each dislocation
	//**************************************************************
   	selectLayerPosition:function(e){
   		updateSlipSysOptions(this.slipsys);
   	},
   	
   	//**************************************************************
	// Function: selectStructure
	// @note: select structure type and modify the number of index 
	//**************************************************************
   	selectStructure:function(e){
   		var structure = $(e.target).val();
   		if(structure.toLowerCase()=="hexagonal" || structure.toLowerCase()=="trigonal"){
   			$(e.target).parents(".modal-body").find("#nInd").val("4");
   		}
   		else{
   			$(e.target).parents(".modal-body").find("#nInd").val("3");
   		}
   	},
   	
   	//**************************************************************
	// Function: selectCrystal
	// @note: select crystal type
	//**************************************************************
   	selectCrystal:function(e){
   		var crystalName = $(e.target).val();
   		var self=this;
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetCrystal&crystalName='+crystalName,
			success : function(response) {
				self.crystal = response;
			}
		});
		
		var crystal = JSON.parse(this.crystal);
		var numMode=parseInt(crystal["nMode"]);
		var container = $(e.target).parents('.modal-body,.layer-content');
		$(container).find("#nSlip").val(numMode);
		var slip_html=$("<div>").append($(container).find(".slip-contents").children().last().clone()).html();
		$(container).find(".slip-contents").html('');
		//console.log(crystalName);
		for (var i=1;i<=numMode;++i){
			$(container).find(".slip-contents").append(slip_html);
			
		}
   		$(container).find(".slip-contents").find("input,select").val("");
   		$(container).find(".slip-contents").find("select[name='mode']").html("");
   		//update mode index
   		for (var i=1;i<=numMode;++i){
			$(container).find("select[name='mode']").append("<option value='"+i+"'>"+i+"</option>");
			
		}
   		
		//update slip system options
		updateSlipSysOptions(self.slipsys);

   	},
   	
   	//**************************************************************
	// Function: createSlipSys
	// @note: create a new slip system 
	//**************************************************************
   	createSlipSys:function(e){
   		var json_data = $('#crystalDialog .modal-body .input-slipSys').find('input,select').serializeFormJSON();
   		if(json_data["Crystal"]!=undefined && json_data["Crystal"]!=''){
   			
			if (json_data["System_Name"] != undefined && json_data["System_Name"] != '') {
				var data = JSON.stringify(json_data);
				//console.log(json_data);
				$.ajax({
					async : false,
					type : 'POST',
					url : 'action.php',
					data : 'FunctionName=CreateSlipSystem&data=' + data,
					success : function(response) {
						alert("Create a new slip system");
						//window.location.reload();
					}
				});
			} else {
				alert("Please enter a valid slip system name");
			}
   		}else {
   			alert("Please choose a crystal structure");
   		}
   			
		//back to preview / update the list 
   		initCrystalMgrList();
   		$('.crystalMgrMode1').click();
   	},
   	
   	
   	
   	//**************************************************************
	// Function: modifySlipSys
	// @note: modify a selected slip system
	//**************************************************************
   	modifySlipSys:function(e){
   		//switch buttons
   		$(e.target).parents("td").find(".modifySlipSys").hide();
   		$(e.target).parents("td").find(".saveSlipSys").show();
   		
   		var nInputs = $(e.target).parents("tr").find("td").length;
   		$.each($(e.target).parents("tr").find("td"),function(i,td){
   			if(i<nInputs-1){ // Keep actions 
	   			var text = $(this).text();
	   			var id = $(this).attr("id");
	   			var width= $(this).width();
	   			$(this).text('').html("<input id='"+id+"' name='"+id+"' value='"+text+"' style='width:100%'></input>");
	   		} 			
   		});
   		
   	},
   	
   	//**************************************************************
	// Function: saveSlipSys
	// @note: save the changes for a selected slip system
	//**************************************************************
   	saveSlipSys:function(e){
   		//switch buttons
   		$(e.target).parents("td").find(".saveSlipSys").hide();
   		$(e.target).parents("td").find(".modifySlipSys").show();
   		
   		var crystalName = $(e.target).parents(".modal-dialog").find(".list-crystal .list-group-item.active").text();
   		var json_data = $(e.target).parents("tr").find('input').serializeFormJSON();		
		json_data["Crystal"]=crystalName;
		if (json_data["System_Name"] != undefined && json_data["System_Name"] != '') {
			var data = JSON.stringify(json_data);
			//console.log(json_data);
			$.ajax({
				async : false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=SaveSlipSystem&data=' + data,
				success : function(response) {
					alert("Save slip system");
					//window.location.reload();
				}
			});
		} else {
			alert("Please enter a valid slip system name");
		}
		
		//back to preview / update the list /preserve the info (Crystal Type) in config form
   		var crystal_type =[];
  		$("#config-form select[name='Crystal_Type'] option").removeAttr("disabled");
   		$.each($("#config-form select[name='Crystal_Type']"),function(){
   			
   			crystal_type.push($(this).val());
   		});
   		initCrystalMgrList();
 		$.each($("#config-form select[name='Crystal_Type']"),function(i){
   			$(this).val(crystal_type[i]);
   		});	
   		$("#config-form select[name='Crystal_Type'] option").attr("disabled","disabled");
   		
   		$('.crystalMgrMode1').click();
   	
   	},
   	
   	//**************************************************************
	// Function: delSlipSys
	// @note: delete a selected slip system
	//**************************************************************
   	delSlipSys:function(e){
   		
   		var crystalName = $(e.target).parents(".modal-dialog").find(".list-crystal .list-group-item.active").text();
   		var td = $(e.target).parents("tr").find("td")[2]; //0:ID, 1:Structure
   		var slipSysName = $(td).html();
   		//console.log(slipSysName);
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=DeleteSlipSystem&slipSysName=' + slipSysName+'&crystalName='+crystalName,
			success : function(response) {
				alert("Delete Slip System");
				//window.location.reload();
			}
		}); 
		
		//back to preview / update the list /
   		initCrystalMgrList();
   		$('.crystalMgrMode1').click();
   	},
   	
   	//**************************************************************
	// Function: loadslipSysMgr
	// @note: load the preview of slip system manager
	//**************************************************************
   	loadCrystalMgr:function(e){
   		
   		//back to preview / update the list /preserve the info (Crystal Type) in config form
   		var crystal_type =[];
  		$("#config-form select[name='Crystal_Type'] option").removeAttr("disabled");
   		$.each($("#config-form select[name='Crystal_Type']"),function(){
   			
   			crystal_type.push($(this).val());
   		});
   		initCrystalMgrList();
 		$.each($("#config-form select[name='Crystal_Type']"),function(i){
   			$(this).val(crystal_type[i]);
   		});	
   		$("#config-form select[name='Crystal_Type'] option").attr("disabled","disabled");
   		
		$(".crystalMgrMode1").click();
   		$("#crystalDialog").modal("show");
   	},
   	
   	//**************************************************************
	// Function: createCrystal
	// @note: create a new crystal structure
	//**************************************************************
   	createCrystal:function(e){
   		
   		var json_data = $('#crystalDialog #crystalBody2').find('input,select').serializeFormJSON();
   		if (json_data["Crystal"]!=undefined &&json_data["Crystal"]!=''){
   			var data = JSON.stringify(json_data);
	   		//console.log(json_data);
			$.ajax({
				async: false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=CreateCrystal&data='+data,
				success : function(response) {
					alert("Create a new crystal");
					//window.location.reload();
				}
		 	});
		}
		else{
			alert("Please enter a valid crystal structure name");
		}	
		
		//back to preview / update the list /preserve the info (Crystal Type) in config form
  		var crystal_type =[];
  		$("#config-form select[name='Crystal_Type'] option").removeAttr("disabled");
   		$.each($("#config-form select[name='Crystal_Type']"),function(){
   			
   			crystal_type.push($(this).val());
   		});
   		initCrystalMgrList();
 		$.each($("#config-form select[name='Crystal_Type']"),function(i){
   			$(this).val(crystal_type[i]);
   		});	
   		$("#config-form select[name='Crystal_Type'] option").attr("disabled","disabled");
   		
   		$('.crystalMgrMode1').click();
   	},
   	
   	//**************************************************************
	// Function: delCrystal
	// @note: delete a selected crystal structure
	//**************************************************************
   	delCrystal:function(e){
   		
   		var crystalName = $(e.target).parents(".modal-dialog").find(".list-crystal .list-group-item.active").text();
		var arrayCrystals=["Default","FCC","HCP","HCP1","HCP2","BCC"];
		if (arrayCrystals.indexOf(crystalName)==-1){
			$.ajax({
				async : false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=DeleteCrystal&crystalName=' + crystalName,
				success : function(response) {
					alert("Delete Crystal");
					//window.location.reload();
				}
			}); 
		}
		else{
			alert(crystalName+" cannot be deleted!");
		}
		
		//back to preview / update the list /preserve the info (Crystal Type) in config form
   		var crystal_type =[];
  		$("#config-form select[name='Crystal_Type'] option").removeAttr("disabled");
   		$.each($("#config-form select[name='Crystal_Type']"),function(){
   			
   			crystal_type.push($(this).val());
   		});
   		initCrystalMgrList();
 		$.each($("#config-form select[name='Crystal_Type']"),function(i){
   			$(this).val(crystal_type[i]);
   		});	
   		$("#config-form select[name='Crystal_Type'] option").attr("disabled","disabled");
   		
   		$('.crystalMgrMode1').click();
   	},
   	
   	//**************************************************************
	// Function: loadMaterialMgr
	// @note: load the preview of material manager
	//**************************************************************
   	loadMaterialMgr:function(e){
   		
   		//back to preview / update the list /preserve the info (Material Type) in config form
   		var material_type =[];
   		$.each($("#config-form select[name='Material_Type']"),function(){
   			material_type.push($(this).val());
   		});
   		initMaterialMgrList();
 		$.each($("#config-form select[name='Material_Type']"),function(i){
   			$(this).val(material_type[i]);
   		});	
   		
   		var container = $("#matBody2");
		$(container).find("input[name='nSlip']").val(1);
		var slip_html=$("<div>").append($(container).find(".slip-contents").children().last().clone()).html();
		$(container).find(".slip-contents").html('');
		$(container).find(".slip-contents").append(slip_html);
		$(container).find(".slip-contents").find("input,select").val("");
		
		$(".matMgrMode1").click();
		$("#materialDialog").find("#saveMaterialBtn").hide();
		$("#materialDialog").modal("show");
	},
	//**************************************************************
	// Function: loadControlMgr
	// @note: load the preview of control manager
	//**************************************************************
	loadControlMgr:function(){
		
		//back to preview / update the list /preserve the info (Control Type) in config form
		var ctrlName = $("#config-form select[name='Control_Name']").val();
   		initControlMgrList();
   		$("#config-form select[name='Control_Name']").val(ctrlName);
		
		
		$(".ctrlMgrMode1").click();
		$("#controlDialog").find("#saveControlBtn").hide();
		$("#controlDialog").modal("show");
	},
   	
   	//**************************************************************
	// Function: createMaterial
	// @note: create a new material
	//**************************************************************
   	createMaterial:function(e){
   		
   		var json_data = $('#materialDialog .modal-body').children( "div" ).not('.slip-contents').
   						find('input,select').serializeFormJSON();
   		
		var o1 = [];
		var slips = $('#materialDialog #matBody2 .slip-content');
		var name = "Slips";
		$.each(slips, function(i,slip) {
			var d1={};
			var a1 = $(slip).find("select,input").serializeArray();
			$.each(a1, function() {
				d1[this.name] = this.value || '';
			});
			o1.push(d1);
		});
		json_data[name]	= o1;	
		
		if (json_data["Material_Type"]!=undefined &&json_data["Material_Type"]!=''){
			var data = JSON.stringify(json_data);
   			//console.log(json_data);
	   		$.ajax({
				async: false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=CreateMaterial&data='+data,
				success : function(response) {
					alert("Create a new material");
					//$('#materialDialog').modal('hide');
					//window.location.reload();
				}
		 	});
		}
		else{
			alert("Please enter the valid material name");
		}
		
		//back to preview / update the list / preserve the info (Material Type) in config form
   		var material_type =[];
   		$.each($("#config-form select[name='Material_Type']"),function(){
   			material_type.push($(this).val());
   		});
   		initMaterialMgrList();
 		$.each($("#config-form select[name='Material_Type']"),function(i){
   			$(this).val(material_type[i]);
   		});	
   		$('.matMgrMode1').click();
		
   	},
   	
   	//**************************************************************
	// Function: deleteMaterial
	// @note: delete existing material
	//**************************************************************
   	delMaterial:function(e){
   		
   		var matName = $(e.target).parents(".modal-dialog").find(".list-material .list-group-item.active").text();
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=DeleteMaterial&matName=' + matName,
			success : function(response) {
				alert("Delete material");
				//$('#materialDialog').modal('hide');
				//window.location.reload();
			}
		}); 
		
		//back to preview / update the list / preserve the info (Material Type) in config form
   		var material_type =[];
   		$.each($("#config-form select[name='Material_Type']"),function(){
   			material_type.push($(this).val());
   		});
   		initMaterialMgrList();
 		$.each($("#config-form select[name='Material_Type']"),function(i){
   			$(this).val(material_type[i]);
   		});	
   		$('.matMgrMode1').click();

   		
   	},
   	
   	modifyMaterial:function(e){
   		
   		//switch buttons
   		$(e.target).parents(".modal-footer").find("#modifyMaterialBtn").hide();
   		$(e.target).parents(".modal-footer").find("#saveMaterialBtn").show();
   		
   		var self = this;
   		var matName = $(e.target).parents(".modal-dialog").find(".list-material .list-group-item.active").text();
   		var mat2Template = _.template($('#material2').html());
   		var slip3Template = _.template($('#slip3').html());
   		
   		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetMaterial&Material_Name='+matName,
			success : function(response) {
				self.material = response;
				
			}
		}); 
		
		//show contents
		var material = JSON.parse(self.material);
    	var container = $(".material-preview");
    	$(container).html('').append(mat2Template(material[0]));
    	$(container).find("#Anisotropy").val(material[0]["Anisotropy"]);
    	var div = $(".material-preview .slip-contents");
    	$.each(material[0]['Slips'], function(i, data) {
			$(div).append(slip3Template(data));
		});
		$(container).find("input,select").css("width","80%");
		
		
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetCrystal&crystalName='+material[0]["Crystal_Type"],
			success : function(response) {
				self.crystal = response;
			}
		});
		
		//show mode index
		var crystal = JSON.parse(this.crystal);
		var numMode=crystal["nMode"];
		$(".material-preview .slip-contents select[name='mode']").html("");
		for (var i=1; i<=numMode;++i){
			$(".material-preview .slip-contents select[name='mode']").append("<option value='"+i+"'>"+i+"</option>");
		}
		
		//update the crystal list /preserve the info (Crystal Type)
   		var crystal_type =[]; //config-form
  		$("#config-form select[name='Crystal_Type'] option").removeAttr("disabled");
   		$.each($("#config-form select[name='Crystal_Type']"),function(){
   			
   			crystal_type.push($(this).val());
   		});
   		initCrystalMgrList();
   		$(".material-preview").find("#Crystal_Type").val(material[0]["Crystal_Type"]); //matbody
 		$.each($("#config-form select[name='Crystal_Type']"),function(i){
   			$(this).val(crystal_type[i]);
   		});	
   		$("#config-form select[name='Crystal_Type'] option").attr("disabled","disabled");
   		
   		
   	},
   	
   	saveMaterial:function(e){
   		
   		//switch buttons
   		$(e.target).parents(".modal-footer").find("#modifyMaterialBtn").show();
   		$(e.target).parents(".modal-footer").find("#saveMaterialBtn").hide();
   		
   		//save new change
   		var json_data = $('#materialDialog .material-preview').children( "div" ).not('.slip-contents').
   						find('input,select').serializeFormJSON();
   		
		var o1 = [];
		var slips = $('#materialDialog .material-preview .slip-content');
		var name = "Slips";
		$.each(slips, function(i,slip) {
			var d1={};
			var a1 = $(slip).find("select,input").serializeArray();
			$.each(a1, function() {
				d1[this.name] = this.value || '';
			});
			o1.push(d1);
		});
		json_data[name]	= o1;
   		
   		if (json_data["Material_Type"]!=undefined &&json_data["Material_Type"]!=''){
   			var data = JSON.stringify(json_data);
   			console.log(json_data);
	   		$.ajax({
				async: false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=SaveMaterial&data='+data,
				success : function(response) {
					
				}
		 	});
   		}
   		else{
			alert("Please enter the valid material name");
		}
   		
   		//back to preview / update the list / preserve the info (Material Type) in config form
   		var material_type =[];
   		$.each($("#config-form select[name='Material_Type']"),function(){
   			material_type.push($(this).val());
   		});
   		initMaterialMgrList();
 		$.each($("#config-form select[name='Material_Type']"),function(i){
   			$(this).val(material_type[i]);
   		});		
 
   	},
   	
   	//**************************************************************
	// Function: createControl
	// @note: create a new control
	//**************************************************************
   	createControl:function(){
  
   		var json_data = $('#controlDialog .modal-body').find('input,select').serializeFormJSON();
   		if (json_data["Control_Name"]!=undefined &&json_data["Control_Name"]!=''){
   			var data = JSON.stringify(json_data);
	   		//console.log(json_data);
			$.ajax({
				async: false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=CreateControl&data='+data,
				success : function(response) {
					alert("Create a new control");
					//$('#controlDialog').modal('hide');
					//window.location.reload();
				}
		 	});
		}
		else{
			alert("Please enter the valid control name");
		}
		
		//back to preview / update the list / preserve the info (Control Name) in config form
   		var ctrlName = $("#config-form select[name='Control_Name']").val();
   		initControlMgrList();
   		$("#config-form select[name='Control_Name']").val(ctrlName);
   		$('.ctrlMgrMode1').click();
   	},
   	
   	//**************************************************************
	// Function: deleteControl
	// @note: delete existing simulation control
	//**************************************************************
   	delControl:function(e){
   		
   		var ctrlName = $(e.target).parents(".modal-dialog").find(".list-control .list-group-item.active").text();
		
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=DeleteControl&ctrlName=' + ctrlName,
			success : function(response) {
				alert("Delete control");
				//$('#controlDialog').modal('hide');
				//window.location.reload();
			}
		}); 
		
		//back to preview / update the list / preserve the info (Control Name) in config form
   		var ctrlName = $("#config-form select[name='Control_Name']").val();
   		initControlMgrList();
   		$("#config-form select[name='Control_Name']").val(ctrlName);
   		$('.ctrlMgrMode1').click();

   		
   	},
   	
   	modifyControl:function(e){
   		
   		//switch buttons
   		$(e.target).parents(".modal-footer").find("#modifyControlBtn").hide();
   		$(e.target).parents(".modal-footer").find("#saveControlBtn").show();
   		
   		var self = this;
   		var ctrlName = $(e.target).parents(".modal-dialog").find(".list-control .list-group-item.active").text();
   		var ctrl2Template = _.template($('#simctrl2').html());
		
   		
   		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSimControl&Control_Name=' + ctrlName,
			success : function(response) {
				self.simctrls = response;
			}
		});
		
		//show contents
    	var simctrls = JSON.parse(self.simctrls);
    	var container = $(".control-preview");
    	var load = simctrls[0]["Load"];
    	$(container).html('').append(ctrl2Template(simctrls[0]));
  		$(container).find("input,select").css("width","80%");
  	
		if (load == "1") {
			$(e.target).parents('.modal-body').find('.applied_force label').html("Applied Strain Rate(s<sup>-1</sup>)");
		} 
		else if (load == "2") {
			$(e.target).parents('.modal-body').find('.applied_force label').html("Applied Stress Rate(MPa)");
		}
   		
   	},
   	
   	saveControl:function(e){
   		//switch buttons
   		$(e.target).parents(".modal-footer").find("#modifyControlBtn").show();
   		$(e.target).parents(".modal-footer").find("#saveControlBtn").hide();
   		
   		//save new changes
   		var json_data = $('#controlDialog .control-preview').find('input,select').serializeFormJSON();
   		console.log(json_data);
   		if (json_data["Control_Name"]!=undefined &&json_data["Control_Name"]!=''){
   			var data = JSON.stringify(json_data);
	   		$.ajax({
				async : false,
				type : 'POST',
				url : 'action.php',
				data : 'FunctionName=SaveSimControl&data=' + data,
				success : function(response) {
				}
			});
		}
		else{
			alert("Please enter the valid control name");
		}
		
   		//back to preview / update the list / preserve the info (Control Name) in config form
   		var ctrlName = $("#config-form select[name='Control_Name']").val();
   		initControlMgrList();
   		$("#config-form select[name='Control_Name']").val(ctrlName);
   		
   		
   	},
   	
   	selectTargetFunction:function(e){
   		$(".target-function").not($(e.target)).removeAttr('checked');
   		if($(e.target).val()=='0' || !(e.target.checked)){
   			$('.processor-content').hide();
   		}
   		else if($(e.target).val()=='1' && e.target.checked){
   			$('.processor-content').show();
   		}
   		
   	},
   	importDislocation:function(){
   		document.getElementById('dislocation_file').addEventListener('change', readDislocationFile, false);
   		$("#dislocation_file").click();
   	
   },
   	importMaterial:function(){
   		alert("This function is not available yet!");
   		

   },
   
   	generateDislocation:function(){
   		$("#addDefectDialog").modal("show");
   		//alert("This function is not available yet!");
   },
   
   	visualizeStructure:function(){
   		$("#visualizeDialog").modal("show");
   		
   		var volume_z = 0;
   		$.each($(".layer-content"),function(i,div){
   			volume_z += parseInt($(div).find("#Thickness").val());
   		});
   		var boxVolume = [parseInt($("#Volume_Size_x").val()),parseInt($("#Volume_Size_y").val()),volume_z];
   		var min = Math.min.apply(Math, boxVolume);
   		var newBoxVolume = [boxVolume[0]/min,boxVolume[1]/min,boxVolume[2]/min];
   		var ratio = [newBoxVolume[0]/boxVolume[0],newBoxVolume[1]/boxVolume[1],newBoxVolume[2]/boxVolume[2]];
   		var dislocations = getDislocationPosition();
   		
   		
   		for (var i=0;i<dislocations.length;++i){
   			for (var j=0; j<dislocations[i].length-1;++j){ //last element is color info
   				dislocations[i][j][0] = dislocations[i][j][0]*ratio[0];
   				dislocations[i][j][1] = dislocations[i][j][1]*ratio[1];
   				dislocations[i][j][2] = dislocations[i][j][2]*ratio[2];
   			}
   		}
   		
   		//console.log(newBoxVolume);
   		//console.log(dislocations);
   		
   		renderMicroStructure(newBoxVolume,dislocations);
   },
   

   	
 });
 

//**************************************************************
// Function: initSimulationList
// @note: initialize the simulation list for load functionality
//**************************************************************
function initSimulationList(){
	//get simulation list
	var self=this;
	$.ajax({
		async: false,
		type : 'POST',
		url : 'action.php',
		data : 'FunctionName=GetAllSimulations',
		success : function(response) {			
			self.simulations = response;
		}
	}); 
	
	//render load list and preview
	var simulations = JSON.parse(this.simulations);
	var loadTemplate = _.template($('#loadsteps').html());
	var layer2Template = _.template($('#layer2').html());
	var defect2Template = _.template($('#defect2').html());
	var slip2Template = _.template($('#slip2').html());
	var node2Template = _.template($('#node2').html());
	$.each(simulations,function(i,data){ 		
  		$(".list-simulation").append("<a class='list-group-item'>"+data["Experiment_Name"]+"</a>");
    }); 
	
	//click simulation items
	 $('.list-simulation .list-group-item').on('click', function (e) {    	
    	$('.list-simulation .list-group-item').removeClass('active');
    	$(this).addClass('active');		
    	
    	beginTab = $('.simulation-preview .nav-tabs li')[0];
    	$('.simulation-preview .nav-tabs li').removeClass('active');
    	$(beginTab).addClass('active');
    	
		var self = this;
		var simName = $(this).text();
		//get configuration
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=LoadExperiment&Experiment_Name=' + simName,
			success : function(response) {
				self.configuration = response;
			}
		}); 
	
	  	//show content
	  	var configuration = JSON.parse(self.configuration);
	  	var container = $(".simulation-preview .tab-content");
	  	configuration['Num_Defect'] = configuration['Defects'].length;
    	console.log(configuration);
    	//step3,4,5
    	$(container).html('').append(loadTemplate(configuration));
    	//step1
    	container = $(".tab-content #step0");
      	$.each(configuration["Layers"],function(i,data){
      		data["Layer_Name"] = "Layer "+(i+1);
	      	$(container).append(layer2Template(data));
	
      	}); 
	  	$.each(configuration["Layers"],function(i,d){
	  		var div = $("#step0 .slip-content")[i];
	  		$.each(d['Slips'],function(j,data){
	  			$(div).append(slip2Template(data));
	  		});
	  	});
	  	//step2
	  	container = $(".tab-content #step1");
     	 $.each(configuration["Defects"],function(i,data){
      		data['Defect_Name'] = "Defect "+(i+1);
      		$(container).append(defect2Template(data));     	
     	});  
     	$.each(configuration["Defects"],function(i,d){	  	
			var div = $("#step1 .node-content")[i];
			$.each(d['Nodes'], function(j, data) {
				$(div).append(node2Template(data));
			}); 
	 	 });   	
	});
	
}

//**************************************************************
// Function: initCrystalMgrList
// @note: initialize the Crystal Manager
//**************************************************************
function initCrystalMgrList(){
	//get slip systems
	var self = this;
      $.ajax({
			async: false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSlipSystem',
			success : function(response) {
				self.slipsys = response;
			}
	 });
	
	//clear list and preview
	$(".list-crystal").html('');
	var table_header = $(".slipSys-preview .table-slipSys tr")[0];
    var table = $(".slipSys-preview .table-slipSys tbody");
    $(table).html('').append(table_header);
    $(".input-slipSys").find("input").val('');
    $(".crystal-info").html('');
    $("#crystalBody2").find("input,select").val("");
    

	var slipsys = JSON.parse(this.slipsys);
	//console.log(slipsys);
	
	//render crystal manager
	var slipsysTemplate = _.template($('#slipsys').html());
	crystals=[];
	$.each(slipsys,function(i,data){ 		
  		crystals.push(data["Crystal"]);
    });
    crystals = _.uniq(crystals);
    $("select[name='Crystal_Type']").html('');
    $.each(crystals,function(i,item){ 		
  		$(".list-crystal").append("<a class='list-group-item'>"+item+"</a>");
  		$("select[name='Crystal_Type']").append("<option value='"+item+"'>"+item+"</option>");
    });
    
	
	//click mgr buttons
    $('.crystalMgrMode1').on('click', function (e) {
    	$(this).removeClass("btn-default").addClass("btn-primary");
    	$('.crystalMgrMode2').removeClass("btn-primary").addClass("btn-default");
    	$(this).attr("disabled",true);
    	$('.crystalMgrMode2').attr("disabled",false);
    	
    	$("#crystalBody2").hide();
    	$("#crystalBody1").show();
    	$("#okCrystalBtn").hide();
    	$("#deleteCrystalBtn").show();
    	
    });
    
    $('.crystalMgrMode2').on('click', function (e) {
    	$(this).removeClass("btn-default").addClass("btn-primary");
    	$('.crystalMgrMode1').removeClass("btn-primary").addClass("btn-default");
    	$(this).attr("disabled",true);
    	$('.crystalMgrMode1').attr("disabled",false); 
    	
    	$("#crystalBody1").hide();
    	$("#crystalBody2").show();
    	$("#okCrystalBtn").show();
    	$("#deleteCrystalBtn").hide();
    	
    });
    
    //click mgr items
    $('.list-crystal .list-group-item').on('click', function (e) {    	
    	$('.list-crystal .list-group-item').removeClass('active');
    	$(this).addClass('active');		
    	
    	var self = this;
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSlipSystem',
			success : function(response) {
				self.slipsys = response;
			}
		}); 
		
	  	//show content
	  	var crystal = $(this).text();	
	  	var slipsys = JSON.parse(this.slipsys);
    	var table_header = $(".slipSys-preview .table-slipSys tr")[0];
    	var table = $(".table-slipSys tbody");
    	var structure='';
    	var nInd='';
    	$(table).html('').append(table_header);
    	$.each(slipsys, function(i, data) {
			if(data["Crystal"]==crystal){
				//console.log(data);
				$(table).append(slipsysTemplate(data));
				structure=data["Structure"];
				nInd=data["nInd"];
			}
			
		});
		var crystal_info = "<p><b>Structure: </b>"+ structure+"</p>"
						+"<p><b>Number of index: </b>"+ nInd+"</p>";
		$(".slipSys-preview .crystal-info").html(crystal_info);
		
		
		//Hide column "Actions" for Default,FCC,HCP,HCP1,HCP2,BCC
		var arrayCrystals=["Default","FCC","HCP","HCP1","HCP2","BCC"];
		if (arrayCrystals.indexOf(crystal)!=-1){
			$(".slipsys-actions").hide();
		}
		else{
			$(".slipsys-actions").show();
		}
		
		//modify the values for hidden fields: crystal type and structure type 
		$(".input-slipSys").find("#Crystal").val(crystal);
		$(".input-slipSys").find("#Structure").val(structure);
		$(".input-slipSys").find("#nInd").val(nInd);
		
		//check if M_04 and B_04 are going to be hidden
		if (nInd=="3"){
			$(".M_04, .B_04").hide();
			$(table_header).find(".MillerIdx, .BurgerIdx").attr('colspan',3);
			$(".input-slipSys").find("#M_04, #B_04").val('0');
		}
		else{
			$(".M_04, .B_04").show();
			$(table_header).find(".MillerIdx, .BurgerIdx").attr('colspan',4);
			$(".input-slipSys").find("#M_04, #B_04").val('');
		}
		
		
	});
    
   	
}

//**************************************************************
// Function: initMaterialMgrList
// @note: initialize the Material Manager
//**************************************************************
function initMaterialMgrList(){
	//get material list
	var self = this;
	$.ajax({		 
		async: false,
		type : 'POST',
		url : 'action.php',
		data : 'FunctionName=GetMaterial',
		success : function(response) {
			self.materials = response;
		}
	});
	

	//clear list and preview
	$(".list-material").html('');
	$(".material-preview").html('');
	$("#matBody2").find("input,select").not("input[readonly='readonly']").val("");
	
	//render material manager and update select field for 'material type'
	var matTemplate = _.template($('#material').html());
	var slip2Template = _.template($('#slip2').html());
	var content = $(".material-preview");
	var materials = JSON.parse(this.materials);
	$("select[name='Material_Type']").html('');
    $.each(materials,function(i,data){ 		
  		$(".list-material").append("<a class='list-group-item'>"+data["Material_Type"]+"</a>");
  		$("select[name='Material_Type']").append("<option value='"+data["Material_Type"]+"'>"+data["Material_Type"]+"</option>");

    });  
    
    //click mgr buttons
    $('.matMgrMode1').on('click', function (e) {
    	$(this).removeClass("btn-default").addClass("btn-primary");
    	$('.matMgrMode2').removeClass("btn-primary").addClass("btn-default");
    	$(this).attr("disabled",true);
    	$('.matMgrMode2').attr("disabled",false);
    	
    	$("#matBody2").hide();
    	$("#matBody1").show();
    	$("#okMaterialBtn").hide();
    	$("#deleteMaterialBtn").show();
    	$("#modifyMaterialBtn").show();
    	
    });
    
    $('.matMgrMode2').on('click', function (e) {
    	$(this).removeClass("btn-default").addClass("btn-primary");
    	$('.matMgrMode1').removeClass("btn-primary").addClass("btn-default");
    	$(this).attr("disabled",true);
    	$('.matMgrMode1').attr("disabled",false); 
    	
    	$("#matBody1").hide();
    	$("#matBody2").show();
    	$("#okMaterialBtn").show();
    	$("#deleteMaterialBtn").hide();
    	$("#modifyMaterialBtn").hide();
    	
    });
    
    
    //click mgr items
    $('.list-material .list-group-item').on('click', function (e) {    	
    	$('.list-material .list-group-item').removeClass('active');
    	$(this).addClass('active');		
    	
    	$("#modifyMaterialBtn").show();
    	$("#saveMaterialBtn").hide();
    	
    	var self = this;
    	var matName = $(this).text();
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetMaterial&Material_Name='+matName,
			success : function(response) {
				self.material = response;
				
			}
		}); 		
	  	//show content
	  	var material = JSON.parse(self.material);
    	var container = $(".material-preview");
    	$(container).html('').append(matTemplate(material[0]));
    	var div = $(".material-preview .slip-content");
    	$.each(material[0]['Slips'], function(i, data) {
			$(div).append(slip2Template(data));
		});
	});
	
	
	
}
//**************************************************************
// Function: initControlMgrList
// @note: initialize the Control Manager
//**************************************************************
function initControlMgrList(){
	//get simulation control list
	$.ajax({		
		async: false,
		type : 'POST',
		url : 'action.php',
		data : 'FunctionName=GetSimControl',
		success : function(response) {
			self.simctrls = response;
		}
	});
	
	//clear list and preview
	$(".list-control").html('');
	$(".control-preview").html('');
	$("#ctrlBody2").find("input,select").not("input[readonly='readonly']").val("");
	
    //render control manager update select field for 'control name'
    var ctrlTemplate = _.template($('#simctrl').html());
    var content = $(".control-preview");
    var simctrls = JSON.parse(this.simctrls);
    $("select[name='Control_Name']").html('');
    $.each(simctrls,function(i,data){ 		
  		$(".list-control").append("<a class='list-group-item'>"+data["Control_Name"]+"</a>");
  		$("select[name='Control_Name']").append("<option value='"+data["Control_Name"]+"'>"+data["Control_Name"]+"</option>");
    }); 
    
    $('.ctrlMgrMode1').on('click', function (e) {
    	$(this).removeClass("btn-default").addClass("btn-primary");
    	$('.ctrlMgrMode2').removeClass("btn-primary").addClass("btn-default");
    	$(this).attr("disabled",true);
    	$('.ctrlMgrMode2').attr("disabled",false); 
    	
    	$("#ctrlBody2").hide();
    	$("#ctrlBody1").show();
    	$("#okControlBtn").hide();
    	$("#deleteControlBtn").show();
    	$("#modifyControlBtn").show();
    });
    $('.ctrlMgrMode2').on('click', function (e) {
    	$(this).removeClass("btn-default").addClass("btn-primary");
    	$('.ctrlMgrMode1').removeClass("btn-primary").addClass("btn-default");
    	$(this).attr("disabled",true);
    	$('.ctrlMgrMode1').attr("disabled",false); 
    	
    	$("#ctrlBody1").hide();
    	$("#ctrlBody2").show();
    	$("#okControlBtn").show();
    	$("#deleteControlBtn").hide();
    	$("#modifyControlBtn").hide();
    });
    
    
	$('.list-control .list-group-item').on('click', function (e) {    	
    	$('.list-control .list-group-item').removeClass('active');
    	$(this).addClass('active');		
    	
    	$("#modifyControlBtn").show();
    	$("#saveControlBtn").hide();
    	
    	var self = this;
    	var ctrlName = $(this).text();
		$.ajax({
			async : false,
			type : 'POST',
			url : 'action.php',
			data : 'FunctionName=GetSimControl&Control_Name=' + ctrlName,
			success : function(response) {
				self.simctrls = response;
			}
		}); 

    	//show content
    	var simctrls = JSON.parse(self.simctrls);
    	var container = $(".control-preview");
    	var load = simctrls[0]["Load"];
    	$(container).html('').append(ctrlTemplate(simctrls[0]));
  
		if (load == "1") {
			$(e.target).parents('.modal-body').find('.applied_force label').html("Applied Strain Rate(s<sup>-1</sup>)");
		} 
		else if (load == "2") {
			$(e.target).parents('.modal-body').find('.applied_force label').html("Applied Stress Rate(MPa)");
		}

	});
}

//**************************************************************
// Function: updateSlipSysOptions
// @note: update slip system options according to the selected crystal type
//**************************************************************
function updateSlipSysOptions(data){
	var slipsys = JSON.parse(data);
	//console.log(slipsys);
	var numDefects = parseInt($("#Num_Defect").val());
	var crystal ='';
	for (var i=0;i<numDefects;++i){
		var defect = $(".defect-content")[i];
		var pos = $(defect).find('#LayerPos').val();
		var layer = $(".layer-content")[pos-1];
		var options ='';
		crystal = $(layer).find("#Crystal_Type option:selected").text();
   		//update the slip system list
		var systemList = slipsys.filter(getSlipSysOption);
		//console.log(systemList);
		$.each(systemList, function(i, sys) {
			options = options + "<option value='"+sys['System_Name']+"'>"+sys['System_Name']+"</option>";
		});
   		$(defect).find('#System_Name').html("").html(options);
	}
	
	
	function getSlipSysOption(system) {
		return system["Crystal"] == crystal;
	}
	
}

//**************************************************************
// Function: submitConfiguration
// @note: execute different functionalities with different mode
//**************************************************************
function submitConfiguration(simName, mode,router){
	
	//console.log('simName: '+simName+', mode: '+mode);
	//Save the configuration
	$("#okSaveAllBtn").click();
	$(".inputfile-content").hide();
	
	if(mode=='0'){ //Generate Input Files
		
		$.ajax({
			async : false,
			type : "POST",
			url : "action.php",
			data : 'FunctionName=ExportConfiguration&Simulation_Name=' + simName,
			success : function(response) {
				URLs = JSON.parse(response);
				material_url = URLs["input_material"].substring(URLs["input_material"].indexOf('temp'));
				interaction_url = URLs["input_interaction"].substring(URLs["input_interaction"].indexOf('temp'));
   				
   				$(".inputfile-content").show();
   				$("#material-download").attr('href',material_url);
   				$("#interaction-download").attr('href',interaction_url);
     		
			}
		}); 

	}
	else if(mode=='1'){ //Run Simulation
		//console.log($('#nProcessor').val());
		var nProcessor = $('#nProcessor').val();
		if (nProcessor != undefined && nProcessor != '' && parseInt(nProcessor)>=2) {
			$.ajax({
				async : false,
				type : "POST",
				url : "action.php",
				data : 'FunctionName=RunSimulation&Simulation_Name=' + simName + '&nProcessors=' + nProcessor,
				success : function(response) {
					router.navigate('/simulation',true);
					//window.location.reload();
				}
			});
		} else {
			alert("Please use at least 2 processors");
		}
	}
	else{
		alert("Please select one functionality");
	}
	
	
}
//**************************************************************
// Function: renderMicroStructure
// @note: render 3D Micro Structure 
//**************************************************************
function renderMicroStructure(boxVolume, dislocations){
	
	var mesh, renderer, scene, camera, controls;
	
	init();
	animate();
	
	function init() {

		// renderer
		if (true) {
			console.log("WebGLRender");
			renderer = new THREE.WebGLRenderer();
		} else {
			console.log("CanvasRender");
			renderer = new THREE.CanvasRenderer();
		}
		var container = document.getElementById("crystal-container");
		var width = $(container).offsetParent().width()*($(container).parents(".modal-dialog").width()/100);
		var height = $(container).height();
		renderer.setSize(width, height);
		$(container).html('');
		container.appendChild(renderer.domElement);

		// scene
		scene = new THREE.Scene();

		// camera
		camera = new THREE.PerspectiveCamera(40, width / height, 1, 10000);
		camera.position.set(5, 5, 5);
		camera.up.set( 0, 0, 1 );
		
		// controls
		controls = new THREE.OrbitControls(camera,container);

		// ambient
		scene.add(new THREE.AmbientLight(0x222222));

		// light
		var light = new THREE.PointLight(0xffffff, 1);
		light.position = camera.position;
		scene.add(light);

		// axes
		scene.add(new THREE.AxisHelper(20));
		
		// geometry
		var geometry = new THREE.BoxGeometry(boxVolume[0], boxVolume[1], boxVolume[2]);
		
		// material, affected by light
		var material = new THREE.MeshLambertMaterial({
			color : 0xff0000,
			polygonOffset : true,
			polygonOffsetFactor : 1,
			polygonOffsetUnits : 1,
			shading : THREE.FlatShading,
			transparent : true,
			opacity : 0.0,
			overdraw : 0.1// for canvasRenderer only
		});

		// mesh
		mesh = new THREE.Mesh(geometry, material);
		mesh.position.x = boxVolume[0]/2;
		mesh.position.y = boxVolume[1]/2;
		mesh.position.z = boxVolume[2]/2;
		scene.add(mesh);
		

		var helper = new THREE.BoxHelper(mesh);
		helper.material.color.set(0xffffff);
		helper.material.linewidth = 3;
		scene.add(helper);
		
		//default color options for dislocation
		defaultColor = [0xffff00,0x762a83,0x7bccc4,0x74c476,0x8c510a,0xdd1c77,0x9e9ac8,0xbd0026,0xf03b20,0xfcc5c0,0xe6ab02,0xfc8d62];
		//lines   		
		for (var i=0;i<dislocations.length;++i){
			
			var geometry = new THREE.Geometry();
   			for (var j=0; j<dislocations[i].length-1;++j){
   				//console.log(dislocations[i][j]);	
   				var x = dislocations[i][j][0];
   				var y = dislocations[i][j][1];
   				var z = dislocations[i][j][2];
				geometry.vertices.push(new THREE.Vector3(x, y, z));
   			}
   			var lineMaterial = new THREE.LineBasicMaterial({
				color : defaultColor[dislocations[i][dislocations[i].length-1]]
			});
   			var line = new THREE.Line(geometry, lineMaterial);
   			scene.add(line);
   		}
		

	}
	

	function animate() {

		requestAnimationFrame(animate);

		controls.update();

		renderer.render(scene, camera);

	}
  
}


//**************************************************************
// Function: readDislocationFile
// @note: import existing dislocation input files
//**************************************************************
function readDislocationFile(evt){// to be completed..................
	
	var f = evt.target.files[0];
	var defectTemplate = _.template($('#defect').html());
    var nodeTemplate = _.template($('#node').html());
	var data=[];
	var self = this;
	
	// parse the dislocation file
	if (f) {
		var r = new FileReader();
		r.onload = function(e) {
			var contents = e.target.result;
			var lines = contents.split('\n');
			for (var i =0; i<lines.length; ++i){
				//alert(lines[i]);
				var texts = lines[i].split(/\s*[\s*]\s*/);
				//alert(texts);
				
				if (texts[1]=="frs") {
					var d = {};
					var t1 = lines[i+1].split(/\s*[\s*]\s*/);
					var t2 = lines[i+2].split(/\s*[\s*]\s*/);
					var t3 = lines[i+3].split(/\s*[\s*]\s*/);
					var t4 = lines[i+4].split(/\s*[\s*]\s*/);
					var t5 = lines[i+5].split(/\s*[\s*]\s*/);
					console.log(parseInt(t1[1])-1);
					var layer_content = $(".layer-content")[parseInt(t1[1])-1];
				    var crystal = $(layer_content).find("#Crystal_Type option:selected").text();
					console.log(crystal);
					if (t3[4]==""|| t3[4]==undefined) t3[4] = 0.0;
					if (t4[4]==""|| t4[4]==undefined) t4[4] = 0.0;
					condition = JSON.stringify({"Crystal":crystal,"M_01":t3[1],"M_02":t3[2],"M_03":t3[3],"M_04":t3[4],"B_01":t4[1],"B_02":t4[2],"B_03":t4[3],"B_04":t4[4]});
					//alert(milleridx[0] + " "+ milleridx[1]+" "+ milleridx[2]+" "+milleridx[3]);		
					var slip_system = verifySlipSystem(condition);
					d["LayerPos"] = t1[1];d["Form_Type"] =t2[1];d["nNode"] =t2[2];
					d["CenterPos_01"] = t5[1]; d["CenterPos_02"] = t5[2];d["CenterPos_03"] = t5[3];
					d["LoopRad"]=t5[4];d["CenterLoop_01"]=t5[5];d["CenterLoop_02"]=t5[6];
					d["System_Name"] = slip_system;
					
					var isEnd = false;
					var j =0;
					var o1=[];
					while(!isEnd){
						++j;
						var tt= lines[i+j+5].split(/\s*[\s*]\s*/);
						if(tt[1]!=undefined && tt[1]!=""){
							d1={"Coordinate_01":tt[2],"Coordinate_02":tt[3]};
							o1.push(d1);
						}
						else
							isEnd = true;
						
					}
					d["Nodes"] = o1;
					
					//add valid defects in data
					if (d["System_Name"]!="None") data.push(d);					
					i = i+j+5;
				}	
			}	
			if(data.length>=1){
				
				//update # of dislocations
				$("#Num_Defect").val(data.length);
				
				//render defect list
				var list = $("#list-defects");
				$(list).html("");
				$.each(data, function(i, d) {
					//console.log(d);
					var defect = {};
					defect = d;
					defect['DefectID'] = i + 1;
					$(list).append(defectTemplate(defect));
					//fill select fields
					var container = $(".defect-content")[i];
					var type = d["Form_Type"];
					$(container).find("#LayerPos").val(d["LayerPos"]);
					$(container).find("#Form_Type").val(d["Form_Type"]);	
					if (type == '0') {
						$(container).find('.form-content').show();
					} else if (type == '1') {
						$(container).find('.form-content').hide();
					}
				}); 
				
				//get slip systems
				$.ajax({
					async : false,
					type : 'POST',
					url : 'action.php',
					data : 'FunctionName=GetSlipSystem',
					success : function(response) {
						self.slipsys = response;
					}
				});
	
				//fill value for slip system
				updateSlipSysOptions(self.slipsys);
				$.each(data, function(i, d) {
					var container = $(".defect-content")[i];
					$(container).find("#System_Name").val(d["System_Name"]);
				});
				$(".defect-content").last().addClass("in"); 
				
				//-------Render node list--------------------
				$.each(data, function(i, d) {
					var div = $(".defect-content .node-contents")[i];
					$.each(d['Nodes'], function(j, d1) {
						d1["NodeID"] = j+1;
						//console.log(data);
						$(div).append(nodeTemplate(d1));
					});
				}); 	
					
				//-------Add parameter description----------
		  		addParamDescription();
			}
			else {
				alert("Warning: Some dislocations do not match the crystal type defined in layer properties.");
			}
			
		
		};
		r.readAsText(f);
	} 
	else {
		alert("Failed to load file");
	}	
}

function verifySlipSystem(condition){
		var self = this;
		$.ajax({
			async : false,
			type : "POST",
			url : "action.php",
			data : 'FunctionName=GetSlipSystem&data=' + condition ,
			success : function(response) {
				self.data = JSON.parse(response);
			}
		});
		var data = this.data;
		if (data["result"] == "success")
			return data["System_Name"];
		else
			return "None"; 
	}

//**************************************************************
// Function: addParamDescription
// @note: add description for fields
//**************************************************************
function addParamDescription(){
	var self = this;
	$.ajax({
		async : false,
		type : "POST",
		url : "action.php",
		data : 'FunctionName=GetParamDescription',
		success : function(response) {	
			self.data = JSON.parse(response);		
		}
	}); 
	
	
	var data = this.data;
	//console.log(data);
	for (field in data){
		var name = field;
		var title = data[name]['title'];
		var description = data[name]['description'];
		$("#config-form").find("input[name='"+name+"'],select[name='"+name+"']").popover({
			trigger : 'hover',
			placement:'right',
			container: 'body',
			html : true,
			title : title,
			content : description

		})
		.css('z-index',1055);	
	}	
}


function getDislocationPosition(){
	
	var dislocations = [];
	$.each($(".defect-content"),function(i,div){
		var dislocation = [];
		
		//----- Get necessary data
		var systemName = $(div).find("#System_Name").val();
		var self = this;
		$.ajax({
			async : false,
			type : "POST",
			url : "action.php",
			data : 'FunctionName=GetSlipSystem&slipSystem=' +encodeURIComponent(String(systemName)),
			success : function(response) {
				self.data = JSON.parse(response);
			}
		});
		//slip system index for color use
		var slipSysIdx = 0;
		$.each($(div).find("#System_Name option"),function(i,option){
			if($(option).val()==systemName)
				slipSysIdx = i;
		});
		
		//---- Prepare parameters
		var data = this.data[0];
		//console.log(this.slipSystems[0]);
		var param = {};
		var slipSys ={};
		param["layerPos"] =  parseInt($(div).find("#LayerPos").val());
		var layer_content = $(".layer-content")[param["layerPos"]-1];
		param["latticeRatios"] = [parseFloat($(layer_content).find('#Lattice_Ratios_x').val()),parseFloat($(layer_content).find('#Lattice_Ratios_y').val()),parseFloat($(layer_content).find('#Lattice_Ratios_z').val())];
		param["euler"] =[parseFloat($(layer_content).find('#Euler_Angles_x').val()),parseFloat($(layer_content).find('#Euler_Angles_y').val()),parseFloat($(layer_content).find('#Euler_Angles_z').val())];
		zOffset = 0;
		for (var j=0;j<param["layerPos"]-1;++j){
			var layerDiv = $(".layer-content")[j];
			zOffset += parseInt($(layerDiv).find("#Thickness").val());
		}
		param["zOffset"]=zOffset;
		
		param["formType"] = parseInt($(div).find("#Form_Type").val());
		param["nNode"] = parseInt($(div).find("#nNode").val());
		param["dislocationPos"] = [parseFloat($(div).find("#CenterPos_01").val()),parseFloat($(div).find("#CenterPos_02").val()),parseFloat($(div).find("#CenterPos_03").val())];
		param["radius"] = parseFloat($(div).find("#LoopRad").val());
		slipSys["nInd"] = parseInt(data["nInd"]);
		slipSys["burgerIdx"] = [parseFloat(data["B_01"]),parseFloat(data["B_02"]),parseFloat(data["B_03"]),parseFloat(data["B_04"])];
		slipSys["millerIdx"] = [parseFloat(data["M_01"]),parseFloat(data["M_02"]),parseFloat(data["M_03"]),parseFloat(data["M_04"])];
		param["slipSys"] = slipSys;
		//console.log(param);
		
		
		//------Transform current node positions into the global ones
		var nodeCoords = [];
		var newNodeCoords = [];
		$.each($(div).find(".node-content"),function(j,nodeDiv){
			nodeCoords.push([parseFloat($(nodeDiv).find("#Coordinate_01").val()), parseFloat($(nodeDiv).find("#Coordinate_02").val())]);
		});	
		//console.log(nodeCoords);
		newNodeCoords = setGlobalDislocation(nodeCoords, param);
		newNodeCoords.push(slipSysIdx);
		//console.log(newNodeCoords);
		
		dislocations.push(newNodeCoords);
	});
	
	return dislocations;
	
	//*********************************************************************
	//  setGlobalDislocation:
	//		Returns global nodes position
	//	OUTPUT: globalCoordinates : global position of dislocation nodes
	//		* size(globalCoordinates) = numberOfNodes for open loops
	//		* size(globalCoordinates) = numberOfPoints for close loops
	//*********************************************************************
	function setGlobalDislocation(nodesCoordinates,parameters) {
		
		numberOfNodes = parameters["nNode"];
		dislocPosition = parameters["dislocationPos"];
		radius = parameters["radius"];
		formType = parameters["formType"];
		
		// Get Euler angles associated with the crystal/layer
		euler = parameters["euler"];
		
		// Get Burgers and Miller index associated with the crystal/layer
		nind = parameters["slipSys"]["nInd"];
		burInd =  parameters["slipSys"]["burgerIdx"];
		milInd =  parameters["slipSys"]["millerIdx"];
		
		// Get lattice ratios associated with the crystal/layer
		laticeRatios = parameters["latticeRatios"];
		
		// Get zOffset position for multilayers
		//zOffset = layer[layerPosition].zOffset
		zOffset = parameters["zOffset"];
		
		// Get rotated Burgers and Miller vectors
		burVec = rotateIndex(nind,burInd,1,euler,laticeRatios);
		milVec = rotateIndex(nind,milInd,2,euler,laticeRatios);
		
		burVec = normalizeVec(burVec);
		milVec = normalizeVec(milVec);
		
		
		if(formType==1) {
			globalCoordinates = setNewInitDis(numberOfNodes,nodesCoordinates,dislocPosition,burVec,milVec,zOffset);
		} else {
			globalCoordinates = setNewInitLoop(radius,dislocPosition,burVec,milVec,zOffset);
		}
		
		return globalCoordinates;
	}
	
	//*********************************************************************
	//  setNewInitDis:
	// 		Init dislocation segment
	//		In input local, X-axis corresponds to Burgers direction, i.e.:
	//		 - pure screw: dislocation along x-axis
	//		 - pure edge: dislocation along y-axis
	//*********************************************************************
	function setNewInitDis(numberOfNodes,nodesCoordinates,dislocPosition,burVec,milVec,zOffset) {
		
		// Center dislocation in (0,0)
		Xc = 0.5 * ( nodesCoordinates[0][0] + nodesCoordinates[numberOfNodes-1][0] );
		Yc = 0.5 * ( nodesCoordinates[0][1] + nodesCoordinates[numberOfNodes-1][1] );
		
		X = new Array(numberOfNodes);
		Y = new Array(numberOfNodes);
		for (var i=0; i<numberOfNodes; i++) {
			X[i] = nodesCoordinates[i][0] - Xc;
			Y[i] = nodesCoordinates[i][1] - Yc;
		}
		//console.log(X);
		//console.log(Y);
		
		// Slip planes directions
		xDir = burVec;
		yDir = crossVec(milVec,burVec);
		yDir = normalizeVec(yDir);
		//console.log(xDir);
		//console.log(yDir);
			
		// Compute nodes global coordinates
		var globalCoordinates = new Array(numberOfNodes);
		for(i = 0; i < globalCoordinates.length; i++)
	    	globalCoordinates[i] = new Array(3);
		for (var i=0; i<numberOfNodes; i++) {
			for (var j=0; j<3; j++) {
				globalCoordinates[i][j] = X[i] * xDir[j] + Y[i] * yDir[j] + dislocPosition[j];
			}
		}
		
		// Add layer offset
		for (var i=0; i<numberOfNodes; i++) {
			globalCoordinates[i][2] += zOffset;
		}
		//console.log(globalCoordinates);
		return globalCoordinates;
	}
	
	//*********************************************************************
	//  setNewInitLoop:
	// 		Init dislocation as a loop
	//*********************************************************************
	function setNewInitLoop(radius,dislocPosition,burVec,milVec,zOffset) {
		
		// Number of points to draw the loop
		numberOfPoints = 8;
		ptheta = 2. * Math.PI / numberOfPoints;
		theta = Math.PI / 2.;
	
		// Center of the initial loop
		Xc = 0.;
		Yc = 0.;
		
		X = new Array(numberOfPoints);
		Y = new Array(numberOfPoints);
		// Local nodes coordinates
		for (var i=0; i<numberOfPoints; i++) {
			X[i] = radius * Math.cos( i * ptheta - theta ) + Xc;
			Y[i] = radius * Math.sin( i * ptheta - theta ) + Yc;
		}
		
		// Slip planes directions
		xDir = burVec;
		yDir = crossVec(milVec,burVec);
		yDir = normalizeVec(yDir);
		
		
		// Compute nodes global coordinates
		var globalCoordinates = new Array(numberOfPoints+1);
		for(i = 0; i < globalCoordinates.length; i++)
	    	globalCoordinates[i] = new Array(3);
		
		for (var i=0; i<numberOfPoints; i++) {
			for (var j=0; j<3; j++) {
				globalCoordinates[i][j] = X[i] * xDir[j] + Y[i] * yDir[j] + dislocPosition[j];
			}
		}
		for (var j=0; j<3; j++) {
				globalCoordinates[numberOfPoints][j] = globalCoordinates[0][j];
		}
		
		// Add layer offset
		for (var i=0; i<numberOfPoints+1; i++) {
			globalCoordinates[i][2] += zOffset;
		}
		
		return globalCoordinates;
	}
	
	//*********************************************************************
	//  rotateIndex
	//*********************************************************************
	function rotateIndex(nind,vecInd,vecType,euler,laticeRatios) {
	
		// Compute rotation matrix
		rotEuler = rotMatEuler(euler);
		
		// Compute cartesian vectors for HCP
		if(nind==4) {
			vec = hcpConvert(vecInd,vecType,laticeRatios);
		} else {
			vec = [vecInd[0], vecInd[1], vecInd[2]];
		}
		
		// Rotate vector
		rotVec = new Array(3);
		for (var i=0; i<3; i++) {
			rotVec[i] = 0.;
			for (var j=0; j<3; j++) {
				rotVec[i] = rotVec[i] + rotEuler[i][j] * vec[j];
			}
		}
		
		return rotVec;
	}
	
	//*********************************************************************
	//  rotMatEuler
	//*********************************************************************
	function rotMatEuler(euler) {
		
		var rotMat = new Array(3);
		for(i = 0; i < rotMat.length; i++)
	    	rotMat[i] = new Array(3);
	    
		c1 = Math.cos(euler[0]);
		s1 = Math.sin(euler[0]);
		c2 = Math.cos(euler[1]);
		s2 = Math.sin(euler[1]);
		c3 = Math.cos(euler[2]);
		s3 = Math.sin(euler[2]);
		
		rotMat[0][0] = c1*c3-c2*s1*s3;
		rotMat[1][0] = c3*s1+c1*c2*s3;
		rotMat[2][0] = s2*s3;
		rotMat[0][1] = -c1*s3-s1*c2*c3;
		rotMat[1][1] = -s1*s3+c1*c2*c3;
		rotMat[2][1] = c3*s2;
		rotMat[0][2] = s1*s2;
		rotMat[1][2] = -c1*s2;
		rotMat[2][2] = c2;
		
		return rotMat;
	}
	
	//*********************************************************************
	//	hcpConvert
	//		Convert 4-axes index into cartesian vectors for HCP
	//		iOption=1: convert Burgers vector
	//		iOption=2: convert Miller index
	//*********************************************************************
	function hcpConvert(vecInd,vecType,laticeRatios) {
		
		a = laticeRatios[0];
		c = laticeRatios[2];
		a1 = new Array(3);a2 = new Array(3);cv = new Array(3);
		
		a1[0] = a*Math.cos(Math.PI/6.);
		a1[1] = -a*Math.sin(Math.PI/6.);
		a1[2] = 0.;
		
		a2[0] = 0.;
		a2[1] = a;
		a2[2] = 0.;
		
		cv[0] = 0.;
		cv[1] = 0.;
		cv[2] = c;
		
		var vec = new Array(3);
		if(vecType==1) {
			// Burgers index
			for (var i=0; i<3; i++) {
				vec[i] = (vecInd[0]-vecInd[2])*a1[i]+(vecInd[1]-vecInd[2])*a2[i]+vecInd[3]*cv[i];
			}
		} else {
			// Miller index
			vec[2] = vecInd[3]/cv[2];
			vec[1] = (vecInd[1]-a2[2]*vec[2])/a2[1];
			vec[0] = (vecInd[0]-a1[1]*vec[1]-a1[2]*vec[2])/a1[0];
		}
		
		return vec;
	}
	
	//*********************************************************************
	//	normalizeVec
	//*********************************************************************
	function normalizeVec(vec) {
		var normVec = new Array(3);
		norm = Math.sqrt(vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]);
		normVec[0] = vec[0]/norm;
		normVec[1] = vec[1]/norm;
		normVec[2] = vec[2]/norm;
		
		return normVec;
	}
	
	//*********************************************************************
	//	crossVec
	//*********************************************************************
	function crossVec(a,b) {
		var cross = new Array(3);
		cross[0] = a[1]*b[2]-a[2]*b[1];
		cross[1] = a[2]*b[0]-a[0]*b[2];
		cross[2] = a[0]*b[1]-a[1]*b[0];
		
		return cross;
	}	
}





//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ to be completed ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

// Validate all input values on the configuration form 
function validateInput(){
	var isValid = false;
	var numSteps = 7;
	var err_msg = "";
	
	for (var step_idx = 0; step_idx< numSteps; ++step_idx){
		if(step_idx==1){}
		else if(step_idx==6){}
		else{
			$("#step" + step_idx + " .inputset").each(function(i) {
				var input = $(this).find("input,select");
				$(input).each(function(idx) {
					var id = $(this).attr("id");
					if ($(this).val()=="") err_msg += id + " is not valid\n";
				});
			});

		}
			
	}
	alert(err_msg);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//***********************************
//Convert DDD Form to JSON
//@note: Based on the DDD input structure
//*************************************
(function($) {
	$.fn.DDDFormToJSON = function() {

		var fieldsets = this.find("fieldset");
		var d = {};
		
		
		fieldsets.each(function(i) {
			// step3,4,5
			if (i != 0 && i != 1) {
				var a = $(this).children( "div" ).find("select,input").serializeArray();
				$.each(a, function() {
					d[this.name] = this.value || '';
					//console.log(this.name);
				});
			}
			
			else if(i==0){
				var a = $(this).children( "div" ).find("select,input").serializeArray();
				$.each(a, function() {
					d[this.name] = this.value || '';
				});
				// layers(depth=1)
				var o1 = [];
				var layers = $(this).find('.layer-content');
				var name = "Layers";
				$.each(layers, function(j,layer) {
					var d1={};
					var a1 = $(layer).find("select,input").serializeArray();
					$.each(a1, function() {
						d1[this.name] = this.value || '';
					});
					
					// slips(depth=2)
					var o2=[];
					var slips = $(this).find('.slip-content');
					var name2 = "Slips";
					$.each(slips, function(k,slip) {		
						var d2={};	
						var a2 = $(slip).find("select,input").serializeArray();
						$.each(a2, function() {
							d2[this.name] = this.value || '';
						}); 	
						o2.push(d2);
					});
					d1[name2] = o2;					
					o1.push(d1);
				});
				d[name] = o1;
			}
			
			else if(i==1){
				var a = $(this).children( "div" ).find("select,input").serializeArray();
				$.each(a, function() {
					d[this.name] = this.value || '';
				});
				// defects(depth=1)
				var o1 = [];
				var defects = $(this).find('.defect-content');
				var name = "Defects";
				$.each(defects, function(j,defect) {
					var d1={};
					var a1 = $(defect).find("select,input").serializeArray();
					$.each(a1, function() {
						d1[this.name] = this.value || '';
					});
					
					// defect nodes(depth=2)
					var o2=[];
					var nodes = $(this).find('.node-content');
					var name2 = "Nodes";
					$.each(nodes, function(k,node) {		
						var d2={};	
						var a2 = $(node).find("select,input").serializeArray();
						$.each(a2, function() {
							d2[this.name] = this.value || '';
						}); 	
						o2.push(d2);
					});
					d1[name2] = o2;
					o1.push(d1);
				});
				d[name] = o1;
			}
		});
		return d;
		//var dataJSON = JSON.stringify(o);
	};
})(jQuery);


//***********************************
//Convert Form to JSON (General case)
//@note: Any form can use this module
//*************************************

(function($) {
	$.fn.serializeFormJSON = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				// inputs share the same name
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}

				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);


