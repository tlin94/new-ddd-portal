LAYER 1: Copper								! Layer id and description
thickness:				3000				! Layer thickness (z) in lattice size
EulerAngles:								! Initial Crystal orientation, Bunge ZXZ convention (rad)
	2.6179938779914944       0.95531661812450919       0.78539816339744817   
MU:						45.d9				! Shear modulus (Pa)
NU:						0.33				! Poisson's ratio
*Elastic stiffness of single crystal (MPa)
  168400.   121400.    121400.     0.0     0.0     0.0
  121400.    168400.    121400.     0.0     0.0     0.0
  121400.    121400.   168400.      0.0     0.0     0.0
     0.0     0.0     0.0     75600.    0.0     0.0
     0.0     0.0     0.0     0.0      75600.   0.0
     0.0     0.0     0.0     0.0     0.0     75600.
*Lattice information (1 FCC; 2 BCC; 3 HCP; 4 HCP1; 5 HCP2; 0 User defined)
crystaltype:			1
Latticeparameter:		3.634D-10			! Lattice spacing (m)
Latticeratios: 			3.21 3.21 5.21		! Lattice ratios for HCP
Latticeangles: 			90. 90. 120.		! Lattice angles for HCP (deg)
* Lattice friction stresses (MPa) and Edge/Screw mobilities (Pa.s) for slip modes
nslipmodes:				1
111:					0.	5.d-6  5.d-6 	! Friction stress, edge mobility, screw mobility
MOBILITYe:				5.d-6
MOBILITYs:				5.d-6
