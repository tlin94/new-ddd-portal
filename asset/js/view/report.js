require_template('report');
myApp.ReportView = Backbone.View.extend({
    events: {
    	
      'click #dddresults-download':'downloadResults',
      'click #strain-download':'downloadStrain',
      'click #stress-download':'downloadStress',
      'click #segs-download':'downloadSegs',
      'click #material-download':'downloadMaterial',
      'click #interaction-download':'downloadDislocation',
      'click #internalfields-download':'downloadFields',
      
    },

    initialize: function(options) {
      
      pageTemplate = _.template($('#report').html());
      this.reportName = options.reportName;
      //console.log(this.reportName);
      
      var self = this;
      //get report path
	  $.ajax({
			async : false,
			type : "POST",
			url : "action.php",
			data : 'FunctionName=GetReportPath&Report_Name='+this.reportName,
			success : function(response) {
				//console.log(response);
				self.path = response.replace(/(\r\n|\n|\r|\t)/gm,"");
			}
	  });
      
      return this.render();
    },

    render: function(eventName) {

      // Render the page template
      $(this.el).html(pageTemplate());
      
      this.simFolder = this.path.split('/')[this.path.split('/').length - 1];
      this.results_file = 'ddd-results.dat';
      this.strain_file = 'InternalStrains.dat';
      this.stress_file = 'InternalStresses.dat';
      this.segs_file = 'segs.zip';
      this.fields_file = 'internalfields.zip';
      this.material_file = 'material_input.txt';
      this.dislocation_file = 'interaction_geom_input.txt';
	  this.templateURL = "http://localhost:8080/DDD-portal/download/"+this.simFolder+"/";
	  
	  //get Stats
	  var axis =[];
	  axis.push({"x":2,"y":3});axis.push({"x":2,"y":16});axis.push({"x":2,"y":19});axis.push({"x":2,"y":18});
	  for(var i =0;i <axis.length;++i)
	  	drawCurves(this.path,this.results_file,axis,i);

	  
      return this;
    },
    
    downloadResults:function(e){
    	var self = this;
    	$("#downloadForm").remove();
    	url = self.templateURL+self.results_file;
    	//console.log(self.templateURL+self.segs_file);
    	
		var form = "<form id='downloadForm' method='POST' action='download.php' style='display:none'>\
					<input name='FunctionName' value='GetSimulationResults'/>\
					<input name='URL' value='"+self.templateURL+"'/>\
					<input name='FileType' value='dat'/>\
					<input name='FileName' value='"+self.results_file+"'/>\
					</form>";
					
		$("#main").append(form);
		$("#downloadForm").submit();
    },
    
    downloadSegs:function(e){
    	var self = this;
    	$("#downloadForm").remove();
    	url = self.templateURL+self.segs_file;
    	//console.log(self.templateURL+self.segs_file);
    	
		var form = "<form id='downloadForm' method='POST' action='download.php' style='display:none'>\
					<input name='FunctionName' value='GetSimulationResults'/>\
					<input name='URL' value='"+self.templateURL+"'/>\
					<input name='FileType' value='zip'/>\
					<input name='FileName' value='"+self.segs_file+"'/>\
					</form>";
					
		$("#main").append(form);
		$("#downloadForm").submit();
    },
    
    downloadFields:function(e){
    	var self = this;
    	$("#downloadForm").remove();
    	url = self.templateURL+self.fields_file;
    	//console.log(self.templateURL+self.segs_file);
    	
		var form = "<form id='downloadForm' method='POST' action='download.php' style='display:none'>\
					<input name='FunctionName' value='GetSimulationResults'/>\
					<input name='URL' value='"+self.templateURL+"'/>\
					<input name='FileType' value='zip'/>\
					<input name='FileName' value='"+self.fields_file+"'/>\
					</form>";
					
		$("#main").append(form);
		$("#downloadForm").submit();
    },
    
    downloadMaterial:function(e){
    	var self = this;
    	$("#downloadForm").remove();
    	console.log("material");
    	url = self.templateURL+self.material_file;
    	//alert(self.templateURL+self.segs_file);
		var form = "<form id='downloadForm' method='POST' action='download.php' style='display:none'>\
					<input name='FunctionName' value='GetSimulationResults'/>\
					<input name='URL' value='"+self.templateURL+"'/>\
					<input name='FileType' value='txt'/>\
					<input name='FileName' value='"+self.material_file+"'/>\
					</form>";
					
		$("#main").append(form);
		$("#downloadForm").submit();
    	
    },
    downloadDislocation:function(e){
    	var self = this;
    	$("#downloadForm").remove();
    	url = self.templateURL+self.dislocation_file;

		var form = "<form id='downloadForm' method='POST' action='download.php' style='display:none'>\
					<input name='FunctionName' value='GetSimulationResults'/>\
					<input name='URL' value='"+self.templateURL+"'/>\
					<input name='FileType' value='txt'/>\
					<input name='FileName' value='"+self.dislocation_file+"'/>\
					</form>";
					
		$("#main").append(form);
		$("#downloadForm").submit();
    	
    },
    downloadStrain:function(e){
    	var self = this;
    	$("#downloadForm").remove();
    	url = self.templateURL+self.strain_file;
    	
		var form = "<form id='downloadForm' method='POST' action='download.php' style='display:none'>\
					<input name='FunctionName' value='GetSimulationResults'/>\
					<input name='URL' value='"+self.templateURL+"'/>\
					<input name='FileType' value='dat'/>\
					<input name='FileName' value='"+self.strain_file+"'/>\
					</form>";
					
		$("#main").append(form);
		$("#downloadForm").submit();
    	
    },
    downloadStress:function(e){
    	var self = this;
    	$("#downloadForm").remove();
    	url = self.templateURL+self.stress_file;
    	//alert(self.templateURL+self.segs_file);
		var form = "<form id='downloadForm' method='POST' action='download.php' style='display:none'>\
					<input name='FunctionName' value='GetSimulationResults'/>\
					<input name='URL' value='"+self.templateURL+"'/>\
					<input name='FileType' value='dat'/>\
					<input name='FileName' value='"+self.stress_file+"'/>\
					</form>";
					
		$("#main").append(form);
		$("#downloadForm").submit();
    	
    },  
      
 });
 
function drawCurves(filepath,filename,axis,i){
	$.post("action.php",{FunctionName:'GetStatsData',FilePath:filepath, FileName:filename, xAxis:axis[i]["x"],yAxis:axis[i]["y"]},
		function(json_data){		
				var data = JSON.parse(json_data);
				points =[]; //change json format(refer to flot examples)
				for (var j = 0; j < data["Points"].length; ++j) {
					points.push([data["Points"][j]["x"], data["Points"][j]["y"]]);
				}
				// define a series
				var curve = {
				    //label: "Stress",
				    data: points
				};
				// define options
				var options = {
				    series: {
				        lines: { show: true },
				        points: { show: false }
				    }
				};
				$.plot("#placeholder"+i, [ curve ],options);		
	});
}
 

